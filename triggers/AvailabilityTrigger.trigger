trigger AvailabilityTrigger on sked__Availability__c (before insert) {
	AvailabilityTriggerHandler handler = new AvailabilityTriggerHandler(true);

	if (trigger.isBefore) {
		if (trigger.isInsert) {
			handler.onBeforeInsert(trigger.new);
		}
	}
}