trigger ResourceRequirementTrigger on Resource_Requirement__c (before insert, before update) {
	ResourceRequirementHandler handler = new ResourceRequirementHandler(true);

	if (trigger.isBefore) {
		if (trigger.isInsert) {
			handler.onBeforeInsert(trigger.new);
		} else if (trigger.isUpdate) {
			handler.onBeforeUpdate(trigger.new, trigger.old);
		}
	}
}