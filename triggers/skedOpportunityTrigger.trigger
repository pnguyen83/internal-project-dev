trigger skedOpportunityTrigger on Opportunity (after update, before insert, before update) {

	if(trigger.isAfter){
		if(trigger.isUpdate){
			//SBE-91 Remove the auto generation of the contract creation when an opp is closed won
			//skedOpportunityTriggerHandler.createContractForClosedWonOpp(trigger.new, trigger.oldMap);
			skedOpportunityTriggerHandler.updatePurchaseNo(trigger.new, trigger.oldMap);
		}
	}

	if(trigger.isBefore){
	
		//Subscription Contract field should auto populate on save based on accounts only active contract)
		if(trigger.isInsert){
			//SBE-41 Removing 'Contract Length' fields and any associated triggers
			skedOpportunityTriggerHandler.preFillEndDateContract(trigger.new, null);
		}

		if(trigger.isUpdate){
			//SBE-112: When type on Opp equals "Add-On", populate the following fields based on the related contract
			//skedOpportunityTriggerHandler.preFillSubscriptionContract(trigger.new);
			//check if opportunity has Skedulo Opp Forecast product when close won
			skedOpportunityTriggerHandler.checkOpportunityLineBeforeCloseWon(trigger.newMap, trigger.oldMap);
			//SBE-41 Removing 'Contract Length' fields and any associated triggers
			skedOpportunityTriggerHandler.preFillEndDateContract(trigger.new, trigger.oldMap);
		}

	}
}