trigger ProjectTrigger on Project__c (after insert, after update, after delete) {
	ProjectTriggerHandler handler = new ProjectTriggerHandler(true);
    
    if (trigger.isAfter) {
        if (trigger.isInsert) {
            handler.onAfterInsert(trigger.new);
        }
        else if (trigger.isUpdate) {
            handler.onAfterUpdate(trigger.new, trigger.old);
        }
        else if (trigger.isDelete) {
            handler.onAfterDelete(trigger.old);
        }
    }
}