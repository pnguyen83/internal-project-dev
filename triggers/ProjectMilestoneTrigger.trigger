trigger ProjectMilestoneTrigger on Project_Milestone__c (after update) {
	ProjectMilestoneHandler handler = new ProjectMilestoneHandler(true);
	if (trigger.isAfter) {
		if(trigger.isUpdate) {
			handler.onAfterUpdate(trigger.new, trigger.oldMap);
		}
	}
}