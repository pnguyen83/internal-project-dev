trigger TaskAutomation on Task (before insert, before update) {
    for(Task nt : trigger.new){
        if(nt.Subject.toLowerCase().startsWith('email') && (nt.Type != 'Email' || nt.Sales_Activity_Type__c != 'Email') && nt.CreatedById != '00590000006JVs2'){
            nt.Type = 'Email';
            nt.Sales_Activity_Type__c = 'Email';
        }
    }
}