trigger skedContract on Subscription_Contract__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {

		if (Trigger.isBefore) {
			
			if (Trigger.isInsert) {
				skedContractHandler.onBeforeInsert(trigger.new);
			}

			if(Trigger.isUpdate){
				skedContractHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
			}	    	
	    
		} else if (Trigger.isAfter) {
	    	//
	    
		}
}