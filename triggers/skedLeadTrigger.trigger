trigger skedLeadTrigger on Lead (before insert) {
    //TRIGGER IS TO CHECK FOR DUPLICATES BASED ON EMAIL AND UPDATE STATUS TO "DQ" IF DUPLICATE IS FOUND

    set<string> setEmail  = new set<string>();
    for(lead l : trigger.new){
        setEmail.add(l.Email);
    }
    
    //Get Existing leads
    map<string,list<lead>> ExistingleadMap = new map<string,list<lead>>();
    for(lead l : [select id, email,status from lead where Email in :setEmail order by createdDate asc]){
        if(ExistingleadMap.containsKey(l.email)){
            ExistingleadMap.get(l.email).add(l);
        } else {
            ExistingleadMap.put(l.email,new list<lead>{l});
        }
    }
    
    map<string,list<lead>> newLeadMap = new map<string,list<lead>>();
    for(lead l : trigger.new){
        if(l.email != null){
            if(ExistingleadMap.containsKey(l.email) ){
                l.Status = 'DQ';
            }
                    if(newLeadMap.containsKey(l.email)){
                        l.Status = 'DQ';
                        newLeadMap.get(l.email).add(l);
                    } else {
                        newLeadMap.put(l.email,new list<lead>{l});
                    }
        }
    }
    
    for(string key : newLeadMap.keySet()){
        for(lead l : newLeadMap.get(key))
        system.debug(l.email + ' | ' + l.status);
    }
}