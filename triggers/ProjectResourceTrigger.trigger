trigger ProjectResourceTrigger on Project_Resource__c (before insert, after insert, after update, after delete) 
{
	ProjectResourceTriggerHandler handler = new ProjectResourceTriggerHandler(true);
    if (trigger.isBefore) {
        if (trigger.isInsert) {
            handler.onBeforeInsert(trigger.new);
        }
    }
    
    if (trigger.isAfter)
    {
        if (trigger.isInsert)
        {
            handler.onAfterInsert(trigger.new);
        }
        if (trigger.isUpdate)
        {
            handler.onAfterUpdate(trigger.new, trigger.old);
        }
        if (trigger.isDelete)
        {
            handler.onAfterDelete(trigger.old);
        }
    }
}