trigger skedOpportunityLineItem on OpportunityLineItem (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {

		boolean isBeforeInsert = Trigger.isBefore && Trigger.isInsert;		

		if(isBeforeInsert){
			skedOpportunityLineItemHandler.onBeforeInsert(Trigger.new);
		}

		/*
		if(Trigger.isAfter){
			if(trigger.isInsert){
				skedOpportunityLineItemHandler.recalculateOpportunityAmount(trigger.new, null, false);
			}

			if(trigger.isUpdate){
				skedOpportunityLineItemHandler.recalculateOpportunityAmount(trigger.new, trigger.oldMap, false);
			}

			if(trigger.isDelete){
				skedOpportunityLineItemHandler.recalculateOpportunityAmount(trigger.old, null, true);
			}

		}

		if(trigger.isBefore){

		}*/
		

}