trigger JiraTicketTrigger on Jira_Ticket__c (after insert, before update, before delete) {
	JiraTicketHandler handler = new JiraTicketHandler(true);
    
    if (trigger.isBefore) {
        if (trigger.isUpdate) {
            handler.onBeforeUpdate(trigger.new, trigger.old);
        }
        
    }
}