trigger skedInvoiceTriggers on Invoice_Draft__c (before insert, after update) {
    if(trigger.isBefore){
        if(trigger.isInsert){
            skedInvoiceTriggerHandler.updateBillingPeriodDate(trigger.new);
        }
    }
    if(trigger.isAfter){
        if(trigger.isUpdate){
            skedInvoiceTriggerHandler.calculateSentInvoices(trigger.new, trigger.oldMap);
        }
    }
}