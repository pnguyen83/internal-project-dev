trigger AccountTrigger on Account (after insert, after update) {
	AccountTriggerHandler handler = new AccountTriggerHandler(true);
	if (trigger.isAfter) {
		if (trigger.isInsert) {
			handler.onAfterInsert(trigger.new);
		}
		else if (trigger.isUpdate) {
			handler.onAfterUpdate(trigger.new, trigger.old);
		}
	}
}