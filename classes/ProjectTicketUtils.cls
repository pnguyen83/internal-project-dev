public class ProjectTicketUtils {
    public static void updateProjectTicketsFromJira(JsonTask.cls_issues iss, String prjID, Map<String, Jira_Ticket__c> mapTickets, 
                                                    Map<String, sked__Resource__c> mapResources, Map<String, String> mapResourceTickets) 
    {
        Jira_Ticket__c prjTicket = new Jira_Ticket__c();
        if (mapTickets.containsKey(iss.key)) {
            prjTicket = mapTickets.get(iss.key);
        }
        
        prjTicket.isUpdateFromJira__c = true;
        prjTicket.Jira_Issue_Key__c = iss.key;
        prjTicket.Jira_URL__c = Jira_Credential__c.getInstance().Host__c + '/browse/' + iss.key;
        if (!String.isEmpty(iss.fields.description))			prjTicket.Description__c = iss.fields.description;
        if (iss.fields.aggregatetimeoriginalestimate != null) 	prjTicket.Original_Estimate__c = iss.fields.aggregatetimeoriginalestimate/3600;
        if (prjTicket.Project__c == null)						prjTicket.Project__c = prjID;
        if (!String.isEmpty(iss.fields.status.name))			prjTicket.Status__c = iss.fields.status.name;
        if (!String.isEmpty(iss.fields.issueType.name))			prjTicket.Ticket_Type__c = iss.fields.issueType.name;
        if (iss.fields.timeestimate != null)					prjTicket.Time_Remain__c = iss.fields.timeestimate/3600;
        prjTicket.Subject__c = iss.fields.summary;
        if (iss.fields.priority != null)						prjTicket.Priority__c = iss.fields.priority.name;
        if (iss.Fields.assignee != null) {
            String username = iss.Fields.assignee.key;
            if (mapResources.containsKey(username)) {
                if (mapResources.get(username).Id != null) prjTicket.Resource__c = mapResources.get(username).Id;
            }
            else {
                sked__Resource__c a = new sked__Resource__c();
                a.Name = iss.Fields.assignee.displayName;
                a.sked__Alias__c = iss.Fields.assignee.key;
                mapResourceTickets.put(a.sked__Alias__c,prjTicket.Jira_Issue_Key__c);
                mapResources.put(a.sked__Alias__c, a);
            }
        }
        
        mapTickets.put(iss.key, prjTicket);
    }
}