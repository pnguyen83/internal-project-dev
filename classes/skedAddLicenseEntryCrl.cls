global class skedAddLicenseEntryCrl extends skedOpportunityDataModal{

    global string endDate 	{get;set;}
    global string startDate {get;set;}

	global skedAddLicenseEntryCrl(ApexPages.StandardController controller){
        Opportunity opp = [select id, Start_Date__c, End_Date__c from Opportunity where id=: controller.getId()];
        if(opp.End_Date__c != null) endDate = DateTime.newInstance(opp.End_Date__c,Time.newInstance(12, 0, 0, 0)).format('yyyy-MM-dd');
        if(opp.Start_Date__c != null) startDate = DateTime.newInstance(opp.Start_Date__c,Time.newInstance(12, 0, 0, 0)).format('yyyy-MM-dd');
	}


	@RemoteAction
    global static skedResponse loadOpportunityData(String opportunityId){
    	skedResponse response = new skedResponse(false, '', '');

    	list<Opportunity> opps = [select id, Start_Date__c, CloseDate, End_Date__c,CurrencyIsoCode    					
    						from Opportunity where id =:opportunityId];
    	if(opps.size() !=1) {response.message = 'Opportunity is not found.'; return response;}
    	Opportunity opp = opps[0];

    	//load license entries & entry items.
    	list<LicenseEntryModal> entries = new list<LicenseEntryModal>();    	
    	list<License_Entry__c> licenseEntries = [select id, name, License_Entry_Start_Date__c, License_Entry_Total_Months__c, Opportunity__r.End_Date__c,
    		(select id, Product2.name, Product2Id, Product2.Family, Description, Quantity, UnitPrice, TotalPrice, Total_ACV__c from Opportunity_Product__r order by CreatedDate)
    	  	from License_Entry__c 
    		where Opportunity__c =:opportunityId order by CreatedDate];

    	// map to modal.
    	for(License_Entry__c entry: licenseEntries){
    		LicenseEntryModal entryModal = new LicenseEntryModal(entry);
            
    		//line items.
    		for(OpportunityLineItem lineItem: entry.Opportunity_Product__r){
    			LicenseEntryItemModal lineItemModal = new LicenseEntryItemModal(lineItem);
                entryModal.lines.add(lineItemModal);
                entryModal.productFamily = lineItem.Product2.Family;
    		}
            
    		entries.add(entryModal);
    	}

        //SIS-137
        list<skedOpportunityDataModal.ProductFamily> productFamilies = loadProductFamilies(opp.CurrencyIsoCode);

    	skedOpportunityDataModal modal = new skedOpportunityDataModal();
    	modal.id = opportunityId;
    	modal.startDate = skedUtils.toDateString(opp.CloseDate);
    	modal.endDate   = skedUtils.toDateString(opp.End_Date__c);
    	modal.entries   = entries;
        modal.productFamilies = productFamilies;

        response.result = modal;
    	response.success = true;
    	return response;
    }

    private static list<skedOpportunityDataModal.ProductFamily> loadProductFamilies(String currencyIsoCode) {
        list<skedOpportunityDataModal.ProductFamily> pfs = new list<skedOpportunityDataModal.ProductFamily>();

        Set<String> families = new Set<String>();
        for (skedOpportunityDataModal.PickListItem productFamily : skedUtil.getPicklistValues('Product2', 'Family')) {
            skedOpportunityDataModal.ProductFamily pf = new skedOpportunityDataModal.ProductFamily();
            if (productFamily.label != 'None') {
                pf.productFamily = productFamily;
                pfs.add(pf);
                families.add(productFamily.id);
            }
        }

        list<PricebookEntry> productObjs = [SELECT id, Product2.Id, UnitPrice, Product2.Name, IsActive, ProductCode,Product2.Description, Product2.Family
                                            FROM PricebookEntry 
                                            WHERE Pricebook2.isStandard = true 
                                            AND Product2.Family IN: families
                                            AND CurrencyIsoCode =: currencyIsoCode
                                            AND IsActive = true];       

        list<skedOpportunityDataModal.ProductFamily> result = new list<skedOpportunityDataModal.ProductFamily>();
        for (skedOpportunityDataModal.ProductFamily pf : pfs) {
            list<ProductModal> products = pf.products;
            for(PricebookEntry entry : productObjs){
                if (entry.Product2.Family == pf.productFamily.id) {
                    products.add(new ProductModal(entry.Product2.Id, entry.Product2.Name,entry.Product2.description, entry.UnitPrice));
                }
            }
            if (!products.isEmpty()) {
                result.add(pf);
            }
        }
        return pfs;
    }

    /*
    * Save data:
    * - License_Entry__c
    * --- OpportunityLineItem
    */
    @RemoteAction
    global static skedResponse saveEntries(String entriesJson,string oppID){
    	skedResponse response = new skedResponse(false, '', '');
    	list<LicenseEntryModal> entriesModal = (list<LicenseEntryModal>)JSON.deserialize(entriesJson, list<LicenseEntryModal>.class);
        Opportunity opp = [select id, CurrencyIsoCode, recordTypeId, Subscription_Order__c from Opportunity where id=:oppId];
    	list<License_Entry__c> entries = new list<License_Entry__c>();
    	list<OpportunityLineItem> lineItems = new list<OpportunityLineItem>();
    	map<id, id> mapProd_Pricebook = new map<id, id>();
        set<string> setOppLineItemId = new set<string>(); 

		// SBE-124
		Id oppAddonRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(SkeduloConstants.OPP_RECORDTYPE_ADDON).getRecordTypeId();
		if (opp.recordTypeId == oppAddonRecordTypeId && String.isBlank(opp.Subscription_Order__c)) {
			response.success = false;
			response.message = 'Hey! You are not able to save this License Entry because there is no contract associated to this Add-On Opportunity. Please click the "Find Contract" button at the top of the opportunity page.';
			return response;
		}

    	for(LicenseEntryModal entryModal: entriesModal){
    		for(LicenseEntryItemModal lineItemModal: entryModal.lines){
    			mapProd_Pricebook.put(lineItemModal.productId, null);
                setOppLineItemId.add(lineItemModal.id);
    		}    		
    	}

    	for(PriceBookEntry pBook: [SELECT Id, Product2Id, Product2.Id, Product2.Name FROM PriceBookEntry 
    		WHERE Product2Id IN :mapProd_Pricebook.keySet() AND PriceBook2.isStandard=true And CurrencyIsoCode = :opp.CurrencyIsoCode]){
    		mapProd_Pricebook.put(pBook.Product2Id, pBook.id);
    	}

        map<string,License_Entry__c> map_id_entry = new map<string,License_Entry__c>();
    	for(LicenseEntryModal entryModal: entriesModal){

    		License_Entry__c entry = new License_Entry__c();
            if(entryModal.id.length() > 18 ){
                entry.id = null;
                entry.Opportunity__c = oppID;
            }else{
                entry.id  = entryModal.id;
            }

    		entry.License_Entry_Start_Date__c = skedUtils.toDate(entryModal.startDate);
            entry.License_Entry_End_Date__c   = skedUtils.toDate(entryModal.endDate);
            
            map_id_entry.put(entryModal.id, entry);
    		
    	}

        SavePoint sPoint = Database.setSavepoint();

        try{
            // License_Entry__c --------------------
            upsert map_id_entry.values();

            delete [select id from OpportunityLineItem where id IN:setOppLineItemId];

            for(LicenseEntryModal entryModal: entriesModal){
                License_Entry__c entry = map_id_entry.get(entryModal.id);
                List<LicenseEntryItemModal> lstMergedLineItems = new List<LicenseEntryItemModal>();
                
                for(LicenseEntryItemModal lineItemModal: entryModal.lines){
                    OpportunityLineItem lineItem = new OpportunityLineItem();
                    lineItem.License_Entry__c = entry.id;
                    lineItem.OpportunityId = oppID;
                    if(mapProd_Pricebook.get(lineItemModal.productId) != null){
                        lineItem.PricebookEntryId = mapProd_Pricebook.get(lineItemModal.productId); 
                    } 
                    lineItem.Description = lineItemModal.description;
                    lineItem.Quantity = lineItemModal.quantity;
                    lineItem.UnitPrice = lineItemModal.unitPriceMonth;
                    lineItem.Product_Family__c = entryModal.productFamily;
                    lineItems.add(lineItem);
                }

            }               

            //--- OpportunityLineItem
            insert lineItems;
            response.success = true;

        } catch(Exception ex){
            response.getErrorMessage(ex);
            Database.rollback(sPoint);
        }        

    	response.result = entriesModal;    	
    	return response;   	
    }

    /*
    * delete:
    * 
    * --- OpportunityLineItem
    */
    @RemoteAction
    global static skedResponse deleteLicenseEntryItem(string recordId){        
        skedResponse response = new skedResponse(false, '', '');
        delete [select id from OpportunityLineItem where id=:recordId];//OpportunityLineItem    
        response.success = true;
        return response;
    }

    /*
    * delete:
    * 
    * --- OpportunityLineItem
    */
    @RemoteAction
    global static skedResponse deleteLicenseEntry(string recordId){        
        skedResponse response = new skedResponse(false, '', '');

        SavePoint sPoint = Database.setSavepoint();

        try{
            //1. delete OpportunityLineItem.
            delete [select id from OpportunityLineItem where License_Entry__c =:recordId];

            //2. delete License_Entry__c
            delete [select id from License_Entry__c where id=:recordId];
            response.success = true;

        } catch(Exception ex){
            response.message =  ex.getMessage();
            Database.rollback(sPoint);

        }

        return response;
    }

    
}