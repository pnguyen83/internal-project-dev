@isTest
private class skedCreateInvoicesBatchTests {
    
    @isTest static void createInvoiceAllContracts1() {
        skedDataFactory.initCommonData();
        Opportunity opp = [select id from Opportunity limit 1];
        opp.StageName = 'Closed Won';
        //opp.Primary_Win_Loss_Reason__c = 'Pricing';
        opp.Win_Loss_Notes__c = 'Running tests';
        update opp;

        Subscription_Contract__c cont = [select id, Order_Status__c from Subscription_Contract__c limit 1];
        cont.Subscription_Contract_Start__c = Date.today();
        cont.Subscription_Contract_End__c = Date.today().addYears(1);
        cont.Order_Status__c = 'Active';
        update cont;

        Test.startTest();
        skedCreateInvoicesBatchTemp b = new skedCreateInvoicesBatchTemp();
        Database.executeBatch(b,1); 
        Test.stopTest();
    }

    @isTest static void createInvoiceAllContracts2() {
        skedDataFactory.initCommonData();
        Opportunity opp = [select id from Opportunity limit 1];
        opp.StageName = 'Closed Won';
        //opp.Primary_Win_Loss_Reason__c = 'Pricing';
        opp.Win_Loss_Notes__c = 'Running tests';
        update opp;
        
        Subscription_Contract__c cont = [select id, Order_Status__c from Subscription_Contract__c limit 1];
        cont.Subscription_Contract_Start__c = Date.today();
        cont.Subscription_Contract_End__c = Date.today().addYears(1);
        cont.Order_Status__c = 'Active';
        cont.Initial_Opportunity__c = opp.Id;
        update cont;

        Order__c order = skedDataFactory.createOrder(cont);

        Product2 prod = [select Id from product2 limit 1];

        skedDataFactory.createSalesOrderItem(order, prod);

        Test.startTest();
        skedCreateInvoicesBatchTemp b = new skedCreateInvoicesBatchTemp(cont.Id);
        Database.executeBatch(b,1); 
        Test.stopTest();
    }

	@isTest static void createUpdateInvoices() {
        skedDataFactory.initCommonData();
        Opportunity opp = [select id, AccountId, Start_Date__c, End_Date__c from Opportunity limit 1];
        Contact cont1 = [select id from Contact where AccountId =:opp.AccountId limit 1];

        Subscription_Contract__c subOrder = skedDataFactory.createSubscription_Orderr(opp, cont1);
        Invoice_Draft__c inv = skedDataFactory.createInvoice(subOrder);
        inv.Invoice_Status__c = 'Sent';
        update inv;

        Invoice_Draft__c invQuery = [select id, Invoice_Status__c from Invoice_Draft__c where id=:inv.id];
        system.debug('invQuery:::'+invQuery.Invoice_Status__c);
    }
    
    /*@isTest static void createInvoice1Contract() {
        skedDataFactory.initCommonData();
        Opportunity opp = [select id,Subscription_Order__c,Subscription_Order__r.id from Opportunity limit 1];
        opp.StageName = 'Closed Won';
        update opp; 
        opp = [select id,Subscription_Order__c,Subscription_Order__r.id from Opportunity limit 1];
        System.debug(opp.Subscription_Order__c);
        ApexPages.StandardController scon = new ApexPages.StandardController(opp.Subscription_Order__r);
        skedSimulateCreateInvoiceProcessExt Sim = new skedSimulateCreateInvoiceProcessExt(scon);

        Test.startTest();
        //skedCreateInvoicesBatchTemp b = new skedCreateInvoicesBatchTemp(opp.Subscription_Order__c);
        //Database.executeBatch(b,1);   
            Sim.createInvoice();
        Test.stopTest();
    }


    @isTest static void createUpdateInvoices() {
        skedDataFactory.initCommonData();
        Opportunity opp = [select id, AccountId, Start_Date__c, End_Date__c from Opportunity limit 1];
        Contact cont1 = [select id from Contact where AccountId =:opp.AccountId limit 1];

        //opp.StageName = 'Closed Won';
        //update opp;
        //skedCreateInvoicesBatchTemp b = new skedCreateInvoicesBatchTemp();
        //Database.executeBatch(b,1);

        Subscription_Contract__c subOrder = skedDataFactory.createSubscription_Orderr(opp, cont1);
        Invoice__c inv = skedDataFactory.createInvoice(subOrder);
        inv.Invoice_Status__c = 'Sent';
        update inv;

        Invoice__c invQuery = [select id, Invoice_Status__c from Invoice__c where id=:inv.id];
        system.debug('invQuery:::'+invQuery.Invoice_Status__c);
    }*/
}