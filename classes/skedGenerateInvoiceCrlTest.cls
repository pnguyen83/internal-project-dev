@isTest
private class skedGenerateInvoiceCrlTest {
	
	@testSetup static void setup() {
		skedDataFactory.initCommonData();
	}
	
	@isTest static void test_saveEntries_success() {
       
		Invoice_Line_Items__c invItem = skedDataFactory.createInvoiceLineItem();
		Invoice_Draft__c inv = [select id, name, Project__c from Invoice_Draft__c limit 1];
		Project__c pro = [select id from Project__c limit 1];

		ApexPages.StandardController sc = new ApexPages.standardController(pro); 
        skedGenerateInvoiceCrl ctrl = new skedGenerateInvoiceCrl(sc);
        skedResponse resp = skedGenerateInvoiceCrl.loadInvoiceData(pro.Id);
        skedGenerateInvoiceCrl.ProjectModal invModel = (skedGenerateInvoiceCrl.ProjectModal)resp.result;
        system.assertEquals(invModel.entries.size() > 0, true);

        //skedOpportunityDataModal.InvoiceEntryModal invEntryModel = new skedOpportunityDataModal.InvoiceEntryModal(inv);
        //invEntryModel.lines = invModel.entries;
        //List<skedOpportunityDataModal.InvoiceEntryModal>  entries = new List<skedOpportunityDataModal.InvoiceEntryModal>{invEntryModel};
        String entriesJson = JSON.serialize(invModel.entries);
        string projectId = [select id from Project__c limit 1][0].id;
        skedGenerateInvoiceCrl.saveEntries(entriesJson, projectId);

        skedGenerateInvoiceCrl.deleteInvoiceEntryItem(invItem.id);
        skedGenerateInvoiceCrl.deleteInvoiceEntry(inv.id);
        
	}
	
}