public with sharing class AvailabilityTriggerHandler {
	private boolean m_isExecuting = false;
    
	public AvailabilityTriggerHandler(boolean isExecuting)
    {
        m_isExecuting = isExecuting;
    }
    
    public boolean isTriggerContext
    {
        get{ return m_isExecuting;}
    }

    public void onBeforeInsert(List<sked__Availability__c> lstNew) {
    	deDupNewAvai(lstNew);
    }

    //===============================================================================//
    public void deDupNewAvai(List<sked__Availability__c> lstNew) {
    	for (sked__Availability__c avai : [SELECT Id, sked__Start__c, sked__Finish__c, sked__Type__c, sked__Resource__c 
    										FROM sked__Availability__c WHERE sked__Start__c >= TODAY]) {
    		for (sked__Availability__c newAvai : lstNew) {
    			if (avai.sked__Start__c == newAvai.sked__Start__c && avai.sked__Finish__c == newAvai.sked__Finish__c && 
    				avai.sked__Resource__c == newAvai.sked__Resource__c) {
    				newAvai.addError('Duplicated Availability record');
    			}	
    		}
    	}
    }
}