@isTest
private class ProjectMilestoneTest
{
	@isTest
	static void itShould()
	{
		Map<String, sObject> mapData = TestUtilities.createData();
		List<Project_Milestone__c> lstMilestones = new List<Project_Milestone__c>();
		Project__c prj = (Project__c)mapData.get('project 1');
		for (Project_Milestone__c prT : [SELECT Id, Actual_Date__c FROM Project_Milestone__c WHERE Project__c = :prj.id]) {
			prT.Actual_Date__c = System.today();
			lstMilestones.add(prT);
		}

		update lstMilestones;
	}
}