public class JsonProject
    {
        public String expand;   //description,lead,issueTypes,url,projectKeys
        public String self; //https://softqware.atlassian.net/rest/api/2/project/11505
        public String id;   //11505
        public String key;  //AI
        public String name; //App Ideas
        public String projectTypeKey;   //software
        public String description;
        public Category projectCategory;
        
        public class Category {
            public String name;
            public String description;
        }
    }