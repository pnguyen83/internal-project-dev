@isTest
private class ProjectTicketUtilsTest
{
	static testmethod void updateProjectTicketsFromJiraTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Jira_Ticket__c jiraTicket = (Jira_Ticket__c)mapData.get('jira ticket 1');
		sked__resource__c resource = (sked__resource__c)mapData.get('resource 1');
		

		JsonTask.cls_statusCategory cate = new JsonTask.cls_statusCategory();
		cate.self = 'self';
		cate.id = 200;
		cate.key = 'key';
		cate.colorName = 'colorName';
		cate.name = 'name';

		JsonTask.cls_status status = new JsonTask.cls_status();
		status.self = 'self';
		status.iconUrl = 'iconUrl';
		status.description = 'description';
		status.name = 'name';
		status.id = 'id';
		status.statusCategory = cate;

		JsonTask.cls_issuetype issueType = new JsonTask.cls_issuetype();
		issueType.name = 'name';

		JsonTask.cls_priority priority = new JsonTask.cls_priority();
		priority.name = 'name';

		JsonTask.cls_assignee assignee = new JsonTask.cls_assignee();
		assignee.key = 'admin';
		assignee.displayName = 'displayName';

		JsonTask.cls_assignee assignee2 = new JsonTask.cls_assignee();
		assignee.key = 'admin';
		assignee.displayName = 'displayName';

		JsonTask.cls_fields field = new JsonTask.cls_fields();
		field.summary = 'summary';
		field.status = status;
		field.description = 'description';
		field.aggregatetimeoriginalestimate = 7200;
		field.timeestimate = 3600;
		field.issuetype = issueType;
		field.priority = priority;
		field.assignee = assignee;

		JsonTask.cls_fields field2 = new JsonTask.cls_fields();
		field.summary = 'summary';
		field.status = status;
		field.description = 'description';
		field.aggregatetimeoriginalestimate = 7200;
		field.timeestimate = 3600;
		field.issuetype = issueType;
		field.priority = priority;
		field.assignee = assignee2;

		JsonTask.cls_issues iss = new JsonTask.cls_issues();
		iss.key = 'key2';
		iss.fields = field;

		JsonTask.cls_issues iss2 = new JsonTask.cls_issues();
		iss2.key = 'key';
		iss2.fields = field2;

		test.starttest();
		ProjectTicketUtils.updateProjectTicketsFromJira(iss, 'prjID', new Map<String, Jira_Ticket__c>{'key2'=>jiraTicket}, new Map<String, sked__Resource__c>{'admin'=>resource}, new Map<String, String>());
		test.stoptest();
	}
}