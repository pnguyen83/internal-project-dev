@isTest 
private class skedFindContractExtTest {

	@isTest static void findContract() {
		skedDataFactory.initCommonData();

		Opportunity opp = [select id from Opportunity limit 1];
		opp.StageName = 'Closed Won';
        opp.Win_Loss_Notes__c = 'Running tests';
		update opp;

		Test.startTest();
		skedFindContractExt.findContract(opp.Id);
        Test.stopTest();
	}

	@isTest static void findContract2() {
		skedDataFactory.initCommonData();

		Account acc = skedDataFactory.createAccount();
		Contact billingContact = skedDataFactory.createBilling_Contactr(acc);
		Contact cont1 = skedDataFactory.createContact(acc);
		acc.Billing_Contact__c = billingContact.id;
		update acc;
		Opportunity opp = skedDataFactory.createOpportunity(acc, 'Initial', 'Negotiation');
		Subscription_Contract__c con = skedDataFactory.createSubscription_Orderr(opp, cont1);
		Opportunity opp1 = skedDataFactory.createOpportunity(acc, 'Add-On', 'Closed Won');

		Test.startTest();
		skedFindContractExt.findContract(opp.Id);
        Test.stopTest();
	}

	@isTest static void findContract3() {
		skedDataFactory.initCommonData();

		Account acc = skedDataFactory.createAccount();
		Contact billingContact = skedDataFactory.createBilling_Contactr(acc);
		Contact cont1 = skedDataFactory.createContact(acc);
		acc.Billing_Contact__c = billingContact.id;
		update acc;
		Opportunity opp = skedDataFactory.createOpportunity(acc, 'Initial', 'Negotiation');
		Subscription_Contract__c con = skedDataFactory.createSubscription_Orderr(opp, cont1);
		Opportunity opp1 = skedDataFactory.createOpportunity(acc, 'Add-On', 'Closed Won');
		opp1.Start_Date__c = Date.today();
		update opp1;

		Test.startTest();
		skedFindContractExt.findContract(opp1.Id);
        Test.stopTest();
	}
}