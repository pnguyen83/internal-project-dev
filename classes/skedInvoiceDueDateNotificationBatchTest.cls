@isTest 
private class skedInvoiceDueDateNotificationBatchTest {

    public static testMethod void testschedule(){
        skedDataFactory.initCommonData();

        Subscription_Contract__c cont = [select id, Order_Status__c from Subscription_Contract__c limit 1];
        cont.Subscription_Contract_Start__c = Date.today();
        cont.Subscription_Contract_End__c = Date.today().addYears(1);
        cont.Order_Status__c = 'Active';
        update cont;

        Invoice_Draft__c invoice = skedDataFactory.createInvoice(cont);
        invoice.Invoice_Status__c = 'Draft';
        invoice.Invoice_Date__c = Date.today().addDays(1);
        invoice.Invoice_Notification_Recipient__c = 'test@skedulo.com';
        invoice.RecordTypeId = Schema.SObjectType.Invoice_Draft__c.getRecordTypeInfosByName().get('License').getRecordTypeId();
        update invoice;

        Test.startTest();
        skedInvoiceDueDateNotificationSchedule  os = new skedInvoiceDueDateNotificationSchedule();
        string sch =' 0 0 13 * * ?';
        system.schedule('Test', sch, os);

        skedInvoiceDueDateNotificationBatch b = new skedInvoiceDueDateNotificationBatch();
        database.executebatch(b, 100);
        Test.stopTest();
    }
}