@isTest 
private class skedSimulateCreateInvoiceProcessExtTest {

    @isTest static void createInvoice() {
        skedDataFactory.initCommonData();
        Subscription_Contract__c cont = [select id, Order_Status__c from Subscription_Contract__c limit 1];

        ApexPages.StandardController sc = new ApexPages.StandardController(cont);
        skedSimulateCreateInvoiceProcessExt con = new skedSimulateCreateInvoiceProcessExt(sc);
        con.createInvoice();
    }
}