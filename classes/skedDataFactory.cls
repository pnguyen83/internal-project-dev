@isTest
public class skedDataFactory {

	public static Contact createBilling_Contactr(Account acc) {
		Contact createNewBilling_Contactr = new Contact();
		createNewBilling_Contactr.AccountId = acc.Id;
		createNewBilling_Contactr.LastName = 'BillingContact';
		createNewBilling_Contactr.FirstName = 'Test';
		createNewBilling_Contactr.Salutation = 'Mr.';
		createNewBilling_Contactr.MailingStreet = '12 maynard';
		createNewBilling_Contactr.MailingCity = 'Ngunnawal';
		createNewBilling_Contactr.MailingPostalCode = '2913';
		createNewBilling_Contactr.MailingCountry = 'Australia';
		createNewBilling_Contactr.MailingState = 'Australian Capital Territory';
		createNewBilling_Contactr.HasOptedOutOfEmail = false;
		createNewBilling_Contactr.Email = 'test@email.com';
		createNewBilling_Contactr.Primary_Contact__c = true;
		createNewBilling_Contactr.Do_not_Market__c = false;
		createNewBilling_Contactr.Status__c = 'Untouched';
		createNewBilling_Contactr.Customer_Contact__c = true;
		createNewBilling_Contactr.Email_Opt_In__c = false;
		createNewBilling_Contactr.MQL_Checkbox__c = false;
		createNewBilling_Contactr.Target_Account__c = false;
		createNewBilling_Contactr.No_Longer_with_Account__c = false;
		insert createNewBilling_Contactr;
		return createNewBilling_Contactr;
	}

	public static Account createAccount() {
		Account createNewAccount = new Account();
		createNewAccount.Name = 'Manh-Test';
		createNewAccount.Type = 'Customer';
		createNewAccount.BillingStreet = '79 McLachlan St, Fortitude Valley';
		createNewAccount.BillingCity = 'Brisbane';
		createNewAccount.BillingPostalCode = '4006';
		createNewAccount.BillingCountry = 'Australia';
		createNewAccount.ShippingStreet = '79 McLachlan St, Fortitude Valley';
		createNewAccount.ShippingCity = 'Brisbane';
		createNewAccount.ShippingState = 'Queensland';
		createNewAccount.ShippingPostalCode = '4006';
		createNewAccount.ShippingCountry = 'Australia';
		createNewAccount.Phone = '99999999';
		createNewAccount.Country_Code__c = 'au';
		createNewAccount.Billing_Entity__c = 'Skedulo AU';
		createNewAccount.Employee_Range__c = '51-149';
		createNewAccount.Annual_Revenue_Range__c = '$50,000,000 to $100,000,000';
		createNewAccount.Target_Account__c = false;
		createNewAccount.Target_Type__c = 'SMB';
		createNewAccount.Target_Type_Formula__c = 'SMB';
		createNewAccount.Batch_Update__c = false;
		createNewAccount.Has_Opp__c = false;
		createNewAccount.Scheduler_Licences__c = 5;
		createNewAccount.Resource_Licences__c = 100;
		//createNewAccount.Billing_Contact__c = createBilling_Contactr().Id ;
		createNewAccount.Invoice_Days_Prior__c = 7;
		insert createNewAccount;
		return createNewAccount;
	}

	public static Contact createContact(Account acc) {
		Contact createNewContact = new Contact();
		createNewContact.AccountId = acc.Id;
		createNewContact.LastName = 'Test';
		createNewContact.FirstName = 'Manh';

		createNewContact.MailingState = 'Queensland';
		createNewContact.MailingCity = 'Brisbane';
		createNewContact.MailingPostalCode = '4006';
		createNewContact.MailingStreet = '79 McLachlan St, Fortitude Valley';
		createNewContact.MailingCountry = 'Australia';
		createNewContact.Email = 'test@email.com';

		createNewContact.ReportsToId = null;
		createNewContact.HasOptedOutOfEmail = false;
		createNewContact.Primary_Contact__c = false;
		createNewContact.Do_not_Market__c = false;
		createNewContact.Status__c = 'Customer';
		createNewContact.Customer_Contact__c = true;
		createNewContact.Email_Opt_In__c = false;
		createNewContact.MQL_Checkbox__c = false;
		createNewContact.Target_Account__c = false;
		createNewContact.No_Longer_with_Account__c = false;
		insert createNewContact;
		return createNewContact;
	}


	public static Opportunity createOpportunity(Account acc, string oppType, string StageName) {
		Opportunity createNewOpportunity = new Opportunity();
		createNewOpportunity.AccountId = acc.id;
		createNewOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('License - ' + oppType).getRecordTypeId();
		createNewOpportunity.Name = 'Skedulo Demo';
		createNewOpportunity.StageName = StageName; //'Negotiation' ;
		createNewOpportunity.Amount = 12290.00;
		createNewOpportunity.CloseDate = (Date) JSON.deserialize('"2017-04-13"', Date.Class);
		//createNewOpportunity.Type = oppType; //'Initial' ;
		createNewOpportunity.ForecastCategoryName = 'Closed';
		createNewOpportunity.CampaignId = null;
		createNewOpportunity.Pricebook2Id = Test.getStandardPricebookId();
		createNewOpportunity.ACV_Opportunity__c = true;
		createNewOpportunity.ACV_Contract_Length__c = 2;
		createNewOpportunity.Payment_Terms__c = 'Annual';
		createNewOpportunity.License_Payment_Method__c = 'Credit Card Direct Debit';
		createNewOpportunity.SalesforceAE__c = null;
		createNewOpportunity.Salesforce_com_SE__c = null;
		createNewOpportunity.Services_Opportunity__c = null;
		createNewOpportunity.Currency__c = 'AUD';
		createNewOpportunity.Country__c = 'APAC';
		createNewOpportunity.Primary_Contact__c = null;
		createNewOpportunity.Assigned_SA__c = null;
		createNewOpportunity.Assigned_PM__c = null;
		createNewOpportunity.Assigned_CSM__c = null;
		createNewOpportunity.Encore_Opportunity__c = false;
		createNewOpportunity.Original_Opportunity__c = null;
		createNewOpportunity.Contract_Length_Months__c = 36;
		createNewOpportunity.Terms_and_Conditions__c = 'entersasasas asas<br>asa<br>asas<br>asa<br>asa<br>sas<br>dsds<br>ds<br>dsd<br>sdsds<br>ds<br>ds<br>sds<br>sd<br>ds<br>fdfdf<br>gfg<br>fgfg<br>fgfg';
		createNewOpportunity.CurrencyIsoCode = 'AUD';
		insert createNewOpportunity;
		return createNewOpportunity;
	}



	public static License_Entry__c createLicenseEntry(Opportunity opp) {
		License_Entry__c createNewLicenseEntry = new License_Entry__c();
		createNewLicenseEntry.Opportunity__c = opp.Id;
		createNewLicenseEntry.License_Entry_Start_Date__c = opp.Start_Date__c;
		createNewLicenseEntry.License_Entry_End_Date__c = opp.End_Date__c;
		createNewLicenseEntry.Order__c = null;
		insert createNewLicenseEntry;
		return createNewLicenseEntry;
	}

	public static License_Entry__c createLicenseEntry3monthsLater(Opportunity opp) {
		License_Entry__c createNewLicenseEntry = new License_Entry__c();
		createNewLicenseEntry.Opportunity__c = opp.Id;
		createNewLicenseEntry.License_Entry_Start_Date__c = opp.Start_Date__c.addmonths(3);
		createNewLicenseEntry.License_Entry_End_Date__c = opp.End_Date__c;
		createNewLicenseEntry.Order__c = null;
		insert createNewLicenseEntry;
		return createNewLicenseEntry;
	}

	public static Product2 createProduct2(string prodName, string prodFamily, string RecordTypeId) {
		Product2 createNewProduct2 = new Product2();
		createNewProduct2.Name = prodName; //'Skedulo Scheduler License' ;
		createNewProduct2.ProductCode = 'SKED-SCH';
		createNewProduct2.Description = 'Annual License for Skedulo Scheduler/Admin User';
		createNewProduct2.IsActive = true;
		createNewProduct2.Family = prodFamily; //'License' ;
		createNewProduct2.RecordTypeId = RecordTypeId;
		insert createNewProduct2;
		return createNewProduct2;
	}

	public static PricebookEntry createPricebookEntry(Product2 prod) {
		PricebookEntry createNewPricebookEntry = new PricebookEntry();
		createNewPricebookEntry.Pricebook2Id = Test.getStandardPricebookId();
		createNewPricebookEntry.Product2Id = prod.Id;
		createNewPricebookEntry.UnitPrice = 59.00;
		createNewPricebookEntry.IsActive = true;
		createNewPricebookEntry.UseStandardPrice = false;
		createNewPricebookEntry.CurrencyIsoCode = 'AUD';
		insert createNewPricebookEntry;
		return createNewPricebookEntry;
	}


	public static OpportunityLineItem createOpportunityProduct(Opportunity opp, PricebookEntry priceEntry, License_Entry__c licenseEntry) {
		OpportunityLineItem createNewOpportunityProduct = new OpportunityLineItem();
		createNewOpportunityProduct.OpportunityId = opp.Id;
		createNewOpportunityProduct.PricebookEntryId = priceEntry.id;
		createNewOpportunityProduct.Quantity = 2.00;
		//createNewOpportunityProduct.TotalPrice = 118.00 ;
		createNewOpportunityProduct.UnitPrice = 59.00;
		createNewOpportunityProduct.Description = 'Annual License for Skedulo Scheduler/Admin User';
		createNewOpportunityProduct.License_Entry__c = licenseEntry.Id;
		createNewOpportunityProduct.Product_Family__c = 'License';
		insert createNewOpportunityProduct;
		return createNewOpportunityProduct;
	}

	public static Subscription_Contract__c createSubscription_Orderr(Opportunity opp, Contact cont) {
		Subscription_Contract__c createNewSubscription_Orderr = new Subscription_Contract__c();
		createNewSubscription_Orderr.Subscription_Contract_Start__c = opp.Start_Date__c;
		createNewSubscription_Orderr.Subscription_Contract_End__c = opp.End_Date__c;
		createNewSubscription_Orderr.Order_Status__c = 'Not Started';
		createNewSubscription_Orderr.Account__c = opp.AccountId;
		createNewSubscription_Orderr.Contact__c = cont.Id;
		createNewSubscription_Orderr.BillingContact__c = cont.Id;
		createNewSubscription_Orderr.Total_Invoiced__c = 15015.00;
		createNewSubscription_Orderr.Payment_Terms__c = 'Annual';
		createNewSubscription_Orderr.Special_Terms__c = 'Arvin Test on Special Terms';
		createNewSubscription_Orderr.Currency__c = 'AUD';
		insert createNewSubscription_Orderr;
		return createNewSubscription_Orderr;
	}


	public static Invoice_Draft__c createInvoice(Subscription_Contract__c cont) {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		Invoice_Draft__c createNewInvoice = new Invoice_Draft__c();
		createNewInvoice.Invoice_Date__c = cont.Subscription_Contract_Start__c;
		createNewInvoice.Invoice_Due_Date__c = cont.Subscription_Contract_End__c;
		createNewInvoice.Subscription_Order__c = cont.Id;
		createNewInvoice.Invoice_Status__c = 'Ready to Send';
		createNewInvoice.Account__c = acc.Id;
		insert createNewInvoice;
		return createNewInvoice;
	}

	public static Invoice_Draft__c createInvoicer() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		Invoice_Draft__c createNewInvoicer = new Invoice_Draft__c();
		createNewInvoicer.Invoice_Date__c = (Date) JSON.deserialize('"2017-06-15"', Date.Class);
		createNewInvoicer.Invoice_Due_Date__c = (Date) JSON.deserialize('"2017-06-22"', Date.Class);
		//createNewInvoicer.Subscription_Order__c = createSubscription_Orderr().Id ;
		createNewInvoicer.Invoice_Status__c = 'Sent';
		createNewInvoicer.Project__c = createProject().Id;
		createNewInvoicer.Type__c = 'Service';
		createNewInvoicer.Account__c = acc.Id;
		insert createNewInvoicer;
		return createNewInvoicer;
	}

	public static Order__c createOrder(Subscription_Contract__c cont) {
		Order__c order = new Order__c();
		order.Order__c = cont.id;
		order.Subscription_Entry_Start_Date__c = system.today().addDays(- 1);
		order.Subscription_Entry_End_Date__c = System.today().addDays(- 1).addMonths(24);
		insert order;
		return order;
	}

	/*
	 * Contact(BillingContact), Account, Opportunity(License), 
	 * create Product2, PricebookEntry.
	 *
	 */
	public static void initCommonData() {
		Account acc = createAccount();
		Contact billingContact = createBilling_Contactr(acc);
		Contact cont1 = createContact(acc);
		acc.Billing_Contact__c = billingContact.id;
		update acc;
		Opportunity opp1 = createOpportunity(acc, 'Initial', 'Negotiation');

		opp1.Start_Date__c = Date.newInstance(System.today().addmonths(1).year(), System.today().addmonths(1).month(), 1);
		opp1.Contract_Length_Months__c = 24;
		opp1.Primary_Contact__c = cont1.id;
		update opp1;

		License_Entry__c licenseEntry = createLicenseEntry(opp1);

		License_Entry__c licenseEntry3Months = createLicenseEntry3monthsLater(opp1);
		Product2 prod = createProduct2('Skedulo Scheduler License', 'License',
		                               Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Product New').getRecordTypeId()
		);
		PricebookEntry priceEntry = createPricebookEntry(prod);
		OpportunityLineItem oppLine = createOpportunityProduct(opp1, priceEntry, licenseEntry);
		OpportunityLineItem oppLine1 = createOpportunityProduct(opp1, priceEntry, licenseEntry3Months);
		createSubscription_Orderr(opp1, cont1);
	}

	public static sked__Resource__c createProject_Managerr() {
		sked__Resource__c createNewProject_Managerr = new sked__Resource__c();
		createNewProject_Managerr.Name = 'Hieu Le';
		createNewProject_Managerr.sked__Home_Address__c = '79 McLachlan St, Fortitude Valley QLD 4006, Australia';
		createNewProject_Managerr.sked__Notification_Type__c = 'sms';
		createNewProject_Managerr.sked__Resource_Type__c = 'Person';
		createNewProject_Managerr.sked__User__c = null;
		createNewProject_Managerr.sked__Weekly_Hours__c = 40.00;
		insert createNewProject_Managerr;
		return createNewProject_Managerr;
	}


	public static sked__Region__c createRegion2r() {
		sked__Region__c createNewRegion2r = new sked__Region__c();
		createNewRegion2r.Name = 'QLD';
		createNewRegion2r.sked__Timezone__c = 'Australia/Queensland';
		insert createNewRegion2r;
		return createNewRegion2r;
	}

	public static Project__c createProject() {
		Project__c createNewProject = new Project__c();
		createNewProject.Name = 'Project test1';
		Account acc = createAccount();
		createNewProject.Account__c = acc.Id;
		createNewProject.Contact__c = createContact(acc).Id;
		createNewProject.End__c = (Date) JSON.deserialize('"2017-05-10"', Date.Class);
		createNewProject.Invoice_Sent__c = '50% Deposit Invoice Sent';
		createNewProject.Project_Key__c = 'PRO0996';
		createNewProject.Project_Manager__c = createProject_Managerr().Id;
		createNewProject.Region2__c = createRegion2r().Id;
		createNewProject.SF_Project_Type__c = 'Implementation Project';
		createNewProject.Start__c = (Date) JSON.deserialize('"2017-05-08"', Date.Class);
		createNewProject.Status__c = 'UAT';
		createNewProject.UAT_Due_Date__c = (Date) JSON.deserialize('"2017-05-08"', Date.Class);
		insert createNewProject;
		return createNewProject;
	}

	public static Invoice_Line_Items__c createInvoiceLineItem() {
		Invoice_Line_Items__c createNewInvoiceLineItem = new Invoice_Line_Items__c();
		createNewInvoiceLineItem.Line_Item_Description__c = 'Expert services.';
		createNewInvoiceLineItem.Line_Item_Unit_Price__c = 1500.00;
		createNewInvoiceLineItem.Line_Item_Quantity__c = 1.00;
		createNewInvoiceLineItem.Invoice__c = createInvoicer().Id;

		list<Product2> prods = [select id, name from Product2];
		Product2 prod = null;
		if (prods.isEmpty()) {
			prod = createProduct2('Skedulo Scheduler License3', 'License',
			                      Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Product New').getRecordTypeId()
			);
		} else {
			prod = prods[0];
		}

		createNewInvoiceLineItem.Product__c = prod.Id;
		createNewInvoiceLineItem.Type__c = 'SOW';
		insert createNewInvoiceLineItem;
		return createNewInvoiceLineItem;
	}

	public static User createUser() {
		User u = new User();
		String uniqueName = UserInfo.getOrganizationId() +
		Datetime.now().format('yyyyMMddHHmmssSSS') + 
		Integer.valueOf(math.rint(math.random() * (2000000 - 1)));

		u.FirstName = uniqueName.subString(29, 34);
		u.LastName = uniqueName.subString(30, 35);
		u.Email = uniqueName + '@skedulo.com';
		u.UserName = uniqueName + '@skedulo.com';
		u.EmailEncodingKey = 'UTF-8';
		u.Alias = 'TestUser';
		u.TimeZoneSidKey = 'America/Los_Angeles';
		u.LocaleSidKey = 'en_US';
		u.LanguageLocaleKey = 'en_US';
		u.ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
		u.PostalCode = '123456789';
		u.Department = 'test';
		u.Phone = '123456789';
		u.Fax = '123456789';
		u.IsActive = true;

		insert u;
		return u;
	}

	public static Sales_Order_Item__c createSalesOrderItem(Order__c order, Product2 prod) {
		Sales_Order_Item__c item = new Sales_Order_Item__c();
		item.Sales_Order__c = order.Id;
		item.Related_Product__c = prod.Id;
		item.Line_Item_Unit_Price__c = 100;
		item.Line_Item_Quantity__c = 10;

		insert item;
		return item;
	}
}