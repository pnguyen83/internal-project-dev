global class skedInvoiceLineItemsCrl extends skedOpportunityDataModal{

    public decimal taxPercent {get;set;}//percentage.
    //Months__c
    public integer months   {get;set;}

	/*
    * Inner Modal classes
    */
    public class InvoiceModal {
        public string id {get;set;}
        public list<InvoiceEntryItemModal>  lineItems         {get;set;} //
        public list<ProductModal>       products        {get;set;} //
        public list<PicklistItem> invoiceTypes {get;set;}
        
        public InvoiceModal(string id, list<InvoiceEntryItemModal> lineItems, list<ProductModal> products, list<PicklistItem> invoiceTypes){
            this.id =  id;
            this.lineItems = lineItems;
            this.products = products;
            this.invoiceTypes = invoiceTypes;
        }
    }

	global skedInvoiceLineItemsCrl(ApexPages.StandardController stdController){
        Invoice_Draft__c inv = (Invoice_Draft__c)stdController.getRecord();
        inv = [select id, AccountId__c, Months__c,Region__c from Invoice_Draft__c where id=:inv.id limit 1][0];
        taxPercent = 0;
        months = 1;
        list<Account> accounts = [select id,Country_Code__c from Account where id=:inv.AccountId__c limit 1];

        if(String.isNotBlank(inv.Region__c)){
            CountryTax__c countryTax = CountryTax__c.getvalues(inv.Region__c.tolowerCase());
            if(countryTax != null) taxPercent = countryTax.Tax__c/100;
        }
        else
        if(!accounts.isEmpty()){
            CountryTax__c countryTax = CountryTax__c.getvalues(accounts[0].Country_Code__c);
            if(countryTax != null) taxPercent = countryTax.Tax__c/100;
        } 

        if(inv.Months__c != null && inv.Months__c > 0){
            months = Integer.valueOf(inv.Months__c);
        }
    }

    public static list<ProductModal> loadProducts(string countryCode){
		list<PricebookEntry> productObjs = [SELECT id, Product2.Id, UnitPrice, Product2.Name, IsActive, ProductCode,Product2.Description FROM PricebookEntry 
    		WHERE Pricebook2.isStandard = true AND IsActive = true AND CurrencyIsoCode =:countryCode
            //and Product2.Family !=:'License'
            ];	

	    list<ProductModal> products = new list<ProductModal>();
	    for(PricebookEntry entry:productObjs){
	    		products.add(new ProductModal(entry.Product2.Id, entry.Product2.Name,entry.Product2.description, entry.UnitPrice) );
	    }
	   return products;
	}

	@RemoteAction
    global static skedResponse loadInvoiceData(String invoiceId){
    	skedResponse response = new skedResponse(true, '', '');
    	list<InvoiceEntryItemModal>  lineItems = new list<InvoiceEntryItemModal>();

    	try{	    	  	
	    	list<Invoice_Draft__c> invoiceEntries = [select id, name, CurrencyIsoCode,
	    		Currency__c, Invoice_Date__c,Invoice_Due_Date__c, Invoice_Status__c, Invoice_Tax__c, Invoice_Total__c, Invoice_Total_Ex_Tax__c,
	    		Project__c,Subscription_Order__c, Total_Outstanding__c, Total_Paid__c,    		
	    			(select id, name, Invoice__c, Type__c, Line_Item_Description__c,Line_Item_Name__c, Line_Item_Quantity__c, Line_Item_Tax__c, Line_Item_Total__c,
	    				Line_Item_Unit_Price__c, Product__c, Sales_Order_Item__c, Discount__c, Line_Item_Add_on__c
	    			from Invoice_Line_Items__r order by CreatedDate)
	    	from Invoice_Draft__c 
	    	where id =:invoiceId order by CreatedDate];
	    	if(invoiceEntries.size() == 0){
	    		response.success = false;
	    		response.message = 'Invoice is not found, please contact your Salesforce Administrator.';
	    		return response;
	    	}

            Map<String, List<InvoiceEntryItemModal>> mapLineItems = new Map<String, List<InvoiceEntryItemModal>>();
	    	
	    	for(Invoice_Line_Items__c lineItem: invoiceEntries[0].Invoice_Line_Items__r){
                System.debug('aaa lineItem = ' + lineItem);
	    		InvoiceEntryItemModal lineItemModal = new InvoiceEntryItemModal(lineItem);
	    		lineItems.add(lineItemModal);
	    	}	 

            response.result = new InvoiceModal(invoiceId, lineItems, loadProducts(invoiceEntries[0].CurrencyIsoCode), skedUtils.getPicklistValues('Invoice_Line_Items__c','Type__c'));

    	}  catch(Exception ex){
    		response.getErrorMessage(ex);
    		response.success = false;
    	}

    	return response;

    }

    /*
    * Save data:
    * - Invoice_Draft__c
    * --- Invoice_Line_Items__c
    */
    @RemoteAction
    global static skedResponse saveEntries(String entriesJson,string projectId){
    	skedResponse response = new skedResponse(false, '', '');
    	list<InvoiceEntryModal> entriesModal = (list<InvoiceEntryModal>)JSON.deserialize(entriesJson, list<InvoiceEntryModal>.class);
        system.debug('entriesModal::' + entriesModal );
        if(entriesModal == null || entriesModal.isEmpty()) return response;   
        InvoiceEntryModal entryModal = entriesModal[0];
        system.debug(entryModal);

        Invoice_Draft__c invoice = [select id, CurrencyIsoCode from Invoice_Draft__c where id =:entryModal.id];
        list<Invoice_Line_Items__c> lineItems = new list<Invoice_Line_Items__c>();  
            
        SavePoint sPoint = Database.setSavepoint();
        try{            
            // insert/update Invoice Line Items.            
            for(InvoiceEntryItemModal lineItemModal: entryModal.lines){
                System.debug('aaa lineItemModal = ' + lineItemModal);
                Decimal addon = lineItemModal.addon != null ? lineItemModal.addon : 0;
                Decimal discount = lineItemModal.discount != null ? lineItemModal.discount : 0;
                //lineItemModal.invoiceId = entry.Id;
                lineItemModal.totalPrice = lineItemModal.unitPrice*(lineItemModal.quantity + addon) - discount;
                lineItemModal.currencyCountryCode = invoice.CurrencyIsoCode;
                //lineItems.add(lineItemModal.toInvoiceItemRecord());
            }   

            List<InvoiceEntryItemModal> lstInvoiceLineItems =  mergeInvoiceLineItem(entryModal.lines);

            for (InvoiceEntryItemModal lineData : lstInvoiceLineItems) {
                lineItems.add(lineData.toInvoiceItemRecord());
            }    

            //--- Invoice_Line_Items__c
            upsert lineItems;
            response.result = lineItems;
            response.success = true;
        } catch(Exception ex){     
            System.debug('Error: ' + ex.getMessage() + ', stack: ' + ex.getStackTraceString());                
            response.getErrorMessage(ex); Database.rollback(sPoint);
        }          
        return response;
    }

    /*
    * delete:
    * 
    * --- Invoice_Line_Items__c
    */
    @RemoteAction
    global static skedResponse deleteInvoiceEntryItem(string recordId){        
       skedResponse response = new skedResponse(true, '', '');
    	try{
            delete [select id from Invoice_Line_Items__c where id =:recordId];
    	}  catch(Exception ex){
    		response.getErrorMessage(ex);response.success = false;    		
    	}

    	return response;
    }

    /*
    * delete:
    * 
    * --- Invoice_Line_Items__c
    */
    @RemoteAction
    global static skedResponse deleteInvoiceEntry(string recordId){        
       skedResponse response = new skedResponse(true, '', '');
    	try{
            delete [select id from Invoice_Draft__c where id =:recordId];
    	}  catch(Exception ex){
    		response.getErrorMessage(ex);response.success = false;    		
    	}

    	return response;
    }

    private static List<InvoiceEntryItemModal> mergeInvoiceLineItem(List<InvoiceEntryItemModal> lstLineItem) {
        List<InvoiceEntryItemModal> lstResult = new List<InvoiceEntryItemModal>();
        Map<String, List<InvoiceEntryItemModal>> mapInvoiceLineModel = new Map<String, List<InvoiceEntryItemModal>>(); 

        //merge similar line items into one
        for (InvoiceEntryItemModal lineItemModal : lstLineItem) {
            List<InvoiceEntryItemModal> lstLineData = mapInvoiceLineModel.get(lineItemModal.productId);
            if (lstLineData == null) {
                lstLineData = new List<InvoiceEntryItemModal>();
                lstLineData.add(lineItemModal);
            }
            else {
                boolean hasSimilar = false;
                for (InvoiceEntryItemModal lineData : lstLineData) {
                    if (lineData.unitPrice == lineItemModal.unitPrice && 
                        (lineItemModal.id == null || lineItemModal.id != lineData.id)) {
                        lineData.description = lineItemModal.description;
                        lineData.quantity += lineItemModal.quantity;
                        lineData.totalPrice += lineItemModal.totalPrice;
                        lineData.addon = lineItemModal.addon;
                        hasSimilar = true;
                        break;
                    }
                }

                if (hasSimilar == false) {
                    lstLineData.add(lineItemModal);
                }
                
            }
            mapInvoiceLineModel.put(lineItemModal.productId, lstLineData);
        }

        for (List<InvoiceEntryItemModal> lstLineData : mapInvoiceLineModel.values()) {
            for (InvoiceEntryItemModal lineData : lstLineData) {
                lstResult.add(lineData);
            }
        }
        
        return lstResult;
    }

}