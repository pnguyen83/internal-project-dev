global class skedCreateInvoicesBatchTemp implements Database.Batchable<sObject> {

	String query;

	//Process All Contracts in the system if IsActive = true and today < End Dazte
	global skedCreateInvoicesBatchTemp() {
		Datetime dt = Datetime.newInstance(system.today(), Time.newInstance(12, 0, 0, 0));
		string fdt = dt.format('yyyy-MM-dd');
		query = 'select id ,CurrencyIsoCode,Payment_Method__c,Currency__c,Account__c,Account__r.BillingCountry,Account__r.Country_Code__c,Account__r.Invoice_Days_Prior__c,Account__r.Entity__c,BillingContact__c,Subscription_Contract_End__c,Payment_Terms__c,Contact__c,Special_Terms__c,Subscription_Contract_Start__c, Purchase_Order_No__c, Initial_Opportunity__c, Initial_Opportunity__r.Start_Date__c, Initial_Opportunity__r.StageName, Initial_Opportunity__r.Billing_Subscription_Created__c from Subscription_Contract__c where Order_Status__c = \'Active\' and Subscription_Contract_End__c > ' + fdt;

		system.debug(query);

	}

	//Process All Contracts in the system if IsActive = true and today < End Dazte
	global skedCreateInvoicesBatchTemp(string contractID) {
		Datetime dt = Datetime.newInstance(system.today(), Time.newInstance(12, 0, 0, 0));
		string fdt = dt.format('yyyy-MM-dd');
		query = 'select id,CurrencyIsoCode,Payment_Method__c,Currency__c ,Subscription_Contract_End__c,Account__r.BillingCountry,Account__c,Account__r.Billing_Contact__c,Account__r.Country_Code__c,Account__r.Invoice_Days_Prior__c,Account__r.Entity__c,BillingContact__c,Payment_Terms__c,Contact__c,Special_Terms__c,Subscription_Contract_Start__c, Purchase_Order_No__c, Initial_Opportunity__r.Start_Date__c, Initial_Opportunity__r.StageName, Initial_Opportunity__r.Billing_Subscription_Created__c from Subscription_Contract__c where id = \'' + contractID + '\'';

		system.debug(query);

	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}


	//Process all Orders of each Contracts to create Invoice based on Start Order Date
	//Order should have next Invoice Date = today or Invoice Date = null

	global void execute(Database.BatchableContext BC, List<Subscription_Contract__c> scope) {
		for (Subscription_Contract__c SC : scope) {
			Map<Date, map<id, map<Decimal, list<Sales_Order_Item__c>>>> date_map_product_map_price = new Map<Date, map<id, map<Decimal, list<Sales_Order_Item__c>>>> ();
			list<Order__c> orderNeedToCreateInvoices = [select id, Subscription_Term__c, Subscription_Entry_Start_Date__c, Subscription_Entry_End_Date__c, Invoices_Processed__c,
															(select id, Related_Product__c, Related_Product__r.Family, Related_Product__r.Name, Line_Item_Unit_Price__c, Line_Item_Total_ACV__c, Line_Item_Total__c, 
																Line_Item_Quantity__c, Line_Item_Name__c, Line_Item_Description__c 
															 from Sales_Order_Items__r)
			                                            from Order__c 
			                                            where Order__c = :SC.id
														order by Subscription_Entry_Start_Date__c asc];
			//process invoices not sent
			processDatemapProductMapPrice(SC, date_map_product_map_price, orderNeedToCreateInvoices);
			map<Date, Invoice_Draft__c> map_date_invoice = processCurrentInvoices(SC.id);

            List<Date> lstProductDate = new List<Date>();
            lstProductDate.addAll(date_map_product_map_price.keyset());
            lstProductDate.sort();
            
            Date currentBillingEndDate = null; 
			Integer index = 0;
			System.debug('lstProductDate: ' + lstProductDate);
			for (Date d : lstProductDate) {
				integer numberOfMonths = getNumberOfMonths(SC.Payment_Terms__c);
				Date contractDate = SC.Subscription_Contract_Start__c;
				while (contractDate<d) {
					contractDate = contractDate.addMonths(getNumberOfMonths(SC.Payment_Terms__c));
				}

				Date invoiceDueDate = d;
				integer dayPrior = SC.Account__r.Invoice_Days_Prior__c == null ? 7 : (integer) SC.Account__r.Invoice_Days_Prior__c;
				Date invoiceDate = d.addDays(- dayPrior);

				if (d != contractDate) {
					numberOfMonths = d.MonthsBetween(contractDate);
				}

				// SBE-97
				if (SC.Initial_Opportunity__r.StageName == 'Closed Won' && !SC.Initial_Opportunity__r.Billing_Subscription_Created__c && index == 0) {
					if (SC.Subscription_Contract_Start__c >= Date.today() && SC.Subscription_Contract_Start__c <= Date.today().addDays(14)) {
						invoiceDate = Date.today();
						invoiceDueDate = Date.today().addDays(14);
					} else {
						invoiceDate = SC.Subscription_Contract_Start__c.addDays(-14);
						invoiceDueDate = SC.Subscription_Contract_Start__c;
					}
				}

				map<id, map<Decimal, list<Sales_Order_Item__c>>> map_product_map_price = new map<id, map<Decimal, list<Sales_Order_Item__c>>>();

				if (!date_map_product_map_price.get(d).isEmpty()) {
					map_product_map_price = date_map_product_map_price.get(d);
					Invoice_Draft__c invoice = null;
					if (!map_date_invoice.containsKey(d)) {
						invoice = createInvoice(SC, invoiceDueDate, invoiceDate);
						invoice.Months__c = numberOfMonths;
						invoice.Billing_Period_Start_Date__c = d;
						invoice.Billing_Period_End_Date__c = d.addMonths(numberOfMonths).addDays(-1);
						// SIS-35
						if (d <= currentBillingEndDate) {
							invoice.Billing_Period_End_Date__c = currentBillingEndDate;
						}
						insert invoice;
					} else {
						invoice = map_date_invoice.get(d);
					}
					List<Invoice_Line_Items__c> lines = upsertInvoiceLineItems(map_product_map_price, invoice, SC, numberOfMonths);
					upsert lines;

					currentBillingEndDate = invoice.Billing_Period_End_Date__c;
				}

				if (currentBillingEndDate == null || d > currentBillingEndDate) {
					currentBillingEndDate = d.addMonths(numberOfMonths).addDays(-1);
				}

				system.debug('#currentBillingEndDate#' + d + ' - ' + currentBillingEndDate);
				index++;
			}

			//update order
			List<Order__c> processedOrders = new List<Order__c>();
			for (Order__c o : orderNeedToCreateInvoices) {
				if (o.Invoices_Processed__c == false) {
					o.Invoices_Processed__c = true;
					processedOrders.add(o);
				}
			}
			update processedOrders;

			if (String.isBlank(SC.Initial_Opportunity__c)) continue;

			Opportunity opp = [select Id, Billing_Subscription_Created__c from Opportunity where id =: SC.Initial_Opportunity__c LIMIT 1];
			if (opp != null) {
				opp.Billing_Subscription_Created__c = true;
				update opp;
			}
		}
	}

	global void finish(Database.BatchableContext BC) {

	}

	private void processDatemapProductMapPrice(Subscription_Contract__c SC, Map<Date, map<id, map<Decimal, list<Sales_Order_Item__c>>>> date_map_product_map_price, list<Order__c> orderNeedToCreateInvoices) {
		for (Order__c order : orderNeedToCreateInvoices) {
			Date contractDate = SC.Subscription_Contract_Start__c;
			Date oDate = order.Subscription_Entry_Start_Date__c;

			while (true) {
				if (contractDate < oDate) {
					contractDate = contractDate.addMonths(getNumberOfMonths(SC.Payment_Terms__c));
					continue;
				}
				date currentEnd = contractDate;

				system.debug('currentEnd' + currentEnd);
				system.debug('oDate' + oDate);
				if (oDate < currentEnd) {
					map<id, map<Decimal, list<Sales_Order_Item__c>>> map_product_map_price = new map<id, map<Decimal, list<Sales_Order_Item__c>>> ();
					if (order.Invoices_Processed__c == false) {
						if (date_map_product_map_price.containsKey(oDate)) {
							map_product_map_price = date_map_product_map_price.get(oDate);
						}
						for (Sales_Order_Item__c SOI : order.Sales_Order_Items__r) {
							// SIS-137
							if (SOI.Related_Product__r.Family != 'License' && SOI.Related_Product__r.Family != 'Premier Support') {
								continue;
							}
							// SIS-137
							map<Decimal, list<Sales_Order_Item__c>> map_price_product = new map<Decimal, list<Sales_Order_Item__c>> ();
							if (map_product_map_price.containsKey(SOI.Related_Product__c)) {
								map_price_product = map_product_map_price.get(SOI.Related_Product__c);
								list<Sales_Order_Item__c> lstSOI = new list<Sales_Order_Item__c> ();
								if (map_price_product.containsKey(SOI.Line_Item_Unit_Price__c)) {
									lstSOI = map_price_product.get(SOI.Line_Item_Unit_Price__c);
								}
								lstSoi.add(SOI);
								map_price_product.put(SOI.Line_Item_Unit_Price__c, lstSOI);
							} else {
								list<Sales_Order_Item__c> lstSOI = new list<Sales_Order_Item__c> { SOI };
								map_price_product.put(SOI.Line_Item_Unit_Price__c, lstSOI);
							}
							map_product_map_price.put(SOI.Related_Product__c, map_price_product);
						}
					}
					
					//Mapping The Days with Product and Lines
					system.debug('oDate in Loop' + oDate);
					system.debug('map_product_map_price' + map_product_map_price);
					date_map_product_map_price.put(oDate, map_product_map_price);
					system.debug('date_map_product_map_price' + date_map_product_map_price);
					oDate = currentEnd;
				}

				system.debug('oDate' + oDate);
				contractDate = contractDate.addMonths(getNumberOfMonths(SC.Payment_Terms__c));

				system.debug('contractDate' + contractDate);
				if (contractDate> SC.Subscription_Contract_End__c.addDays(1))
				break;
			}


		}
	}

	private integer getNumberOfMonths(string freQuency) {
		if (freQuency == 'Monthly') return 1;
		if (freQuency == 'Quarterly') return 3;
		if (freQuency == 'Bi-annual') return 6;
		return 12;
	}

	private map<Date, Invoice_Draft__c> processCurrentInvoices(id contractID) {
		map<Date, Invoice_Draft__c> map_date_invoice = new map<date, Invoice_Draft__c> ();

		for (Invoice_Draft__c invoice :[select id, CurrencyIsoCode, Invoice_Due_Date__c, Billing_Period_End_Date__c,
											(select id, Product__c, Line_Item_Description__c, Line_Item_Name__c, Line_Item_Quantity__c, Line_Item_Total__c, Line_Item_Tax__c, Line_Item_Unit_Price__c 
											from Invoice_Line_Items__r) 
										from Invoice_Draft__c 
										where Subscription_Order__c = :contractID and Invoice_Status__c in('Ready to Send', 'Draft', 'New')]) {
			map_date_invoice.put(invoice.Invoice_Due_Date__c, invoice);
		}

		return map_date_invoice;
	}

	private list<Invoice_Line_Items__c> upsertInvoiceLineItems(map<id, map<Decimal, list<Sales_Order_Item__c>>> map_product_map_price, Invoice_Draft__c invoice, Subscription_Contract__c SC, integer numberOfMonths) {
		CountryTax__c countryTax = CountryTax__c.getvalues(SC.Account__r.Country_Code__c);
		List<Invoice_Line_Items__c> upsertlines = new List<Invoice_Line_Items__c> ();
		for (id pID : map_product_map_price.keyset()) {
			for (decimal price : map_product_map_price.get(pID).keyset()) {
				List<Invoice_Line_Items__c> lines = invoice.Invoice_Line_Items__r;
				Invoice_Line_Items__c currItem = null;
				for (Invoice_Line_Items__c item : lines) {
					if (item.Product__c == pID && item.Line_Item_Unit_Price__c == price) {
						currItem = item;
						break;
					}
				}

				if (currItem == null) {
					currItem = new Invoice_Line_Items__c(
					                                     Invoice__c = invoice.id,
					                                     Line_Item_Unit_Price__c = price * numberOfMonths,
					                                     Line_Item_Quantity__c = 0,
					                                     Line_Item_Tax__c = 0,
					                                     Line_Item_Total__c = 0,
					                                     CurrencyIsoCode = invoice.CurrencyIsoCode
					);
				}

				for (Sales_Order_Item__c SOI : map_product_map_price.get(pID).get(price)) {
					currItem.Line_Item_Quantity__c += SOI.Line_Item_Quantity__c;
					currItem.Line_Item_Name__c = SOI.Related_Product__r.Name;
					currItem.Product__c = SOI.Related_Product__c;
					currItem.Line_Item_Description__c = SOI.Line_Item_Description__c;

				}
				currItem.Line_Item_Total__c = currItem.Line_Item_Unit_Price__c * currItem.Line_Item_Quantity__c;


				if (countryTax != null) {
					currItem.Line_Item_Tax__c = currItem.Line_Item_Total__c * (countryTax.tax__c / 100); // 10 % tax
				}

				upsertlines.add(currItem);
			}
		}

		//merge invoice line item based on product and unit price
		upsertlines = mergeInvoiceLineItem(upsertlines);

		return upsertlines;
	}

	private Invoice_Draft__c createInvoice(Subscription_Contract__c SC, Date InvoiceDueDate, Date invoiceDate) {
		Invoice_Draft__c invoice = new Invoice_Draft__c(
		                                    Invoice_Date__c = invoiceDate,
		                                    Invoice_Due_Date__c = InvoiceDueDate,
		                                    Invoice_Status__c = 'Draft',
		                                    Subscription_Order__c = SC.id,
		                                    Automate_Invoice__c = true,
		                                    Type__c = 'License',
		                                    Payment_Method__c = SC.Payment_Method__c,
		                                    Purchase_Order_No__c = SC.Purchase_Order_No__c,
		                                    RecordTypeId = Schema.SObjectType.Invoice_Draft__c.getRecordTypeInfosByName().get('License').getRecordTypeId(),
		                                    CurrencyIsoCode = SC.CurrencyIsoCode,
		                                    Account__c = SC.Account__c,
		                                    Billing_Contact__c = SC.Account__r.Billing_Contact__c
		);

		return invoice;
	}


	private static List<Invoice_Line_Items__c> mergeInvoiceLineItem(List<Invoice_Line_Items__c> lstLineItem) {
        List<Invoice_Line_Items__c> lstResult = new List<Invoice_Line_Items__c>();
        Map<String, List<Invoice_Line_Items__c>> mapInvoiceLineModel = new Map<String, List<Invoice_Line_Items__c>>(); 

        //merge similar line items into one
        for (Invoice_Line_Items__c lineItemModal : lstLineItem) {
            List<Invoice_Line_Items__c> lstLineData = mapInvoiceLineModel.get(lineItemModal.Product__c);
            if (lstLineData == null) {
                lstLineData = new List<Invoice_Line_Items__c>();
                lstLineData.add(lineItemModal);
            }
            else {
                boolean hasSimilar = false;
                for (Invoice_Line_Items__c lineData : lstLineData) {
                    if (lineData.Line_Item_Unit_Price__c == lineItemModal.Line_Item_Unit_Price__c && 
                        (lineItemModal.id == null || lineItemModal.id != lineData.id)) {
                        //lineData.description = lineItemModal.description;
                        lineData.Line_Item_Quantity__c += lineItemModal.Line_Item_Quantity__c;
                        lineData.Line_Item_Total__c += lineItemModal.Line_Item_Total__c;
                        hasSimilar = true;
                        break;
                    }
                }

                if (hasSimilar == false) {
                    lstLineData.add(lineItemModal);
                }
                
            }
            mapInvoiceLineModel.put(lineItemModal.Product__c, lstLineData);
        }

        for (List<Invoice_Line_Items__c> lstLineData : mapInvoiceLineModel.values()) {
            for (Invoice_Line_Items__c lineData : lstLineData) {
                lstResult.add(lineData);
            }
        }
        
        return lstResult;
    }
}