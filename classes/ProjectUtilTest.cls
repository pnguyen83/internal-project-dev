@isTest
private class ProjectUtilTest
{
	static testmethod void getAllTaskTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Project__c prj3 = (Project__c)mapData.get('project 3');

		test.starttest();
		ProjectUtil.getAllTask('prjKey', prj3.id);
		test.stoptest();
	}

	static testmethod void mapResourceTicketsTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Map<String, String> mapResTicket = new Map<String, String>();
		mapResTicket.put('resource2', 'JIRA1');

		test.starttest();
		ProjectUtil.connectJiraTicketWithResource(mapResTicket);
		test.stoptest();
	}

	static testmethod void updateProjectResourceTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Project__c prj1 = (Project__c)mapData.get('project 1');
		List<String> lstPrjIds = new List<String>{prj1.Id};

		test.starttest();
		ProjectUtil.updateProjectResource(lstPrjIds);
		test.stoptest();
	}

	static testmethod void generateJiraProjectTest() {
		Jira_Credential__c credential = new Jira_Credential__c();
		credential.Host__c = 'host';
		credential.Password__c = 'password';
		credential.Username__c = 'username';
		insert credential;

		Map<String, sObject> mapData = TestUtilities.createData();
		Project__c prj = (Project__c)mapData.get('project 2');

		test.starttest();
		ProjectUtil.generateJiraProject(prj.Id, 'prjKey', 'name', 'description', 'type', 'resourceName');
		test.stoptest();
	}

	
}