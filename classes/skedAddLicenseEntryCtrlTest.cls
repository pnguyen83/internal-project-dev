@isTest
private class skedAddLicenseEntryCtrlTest {
	
	/*
		Init test data.
	*/
	@testSetup static void setup() {
		skedDataFactory.initCommonData();
	}	
	
	@isTest static void test_saveEntries_success() {
		Opportunity opp = [select id from Opportunity limit 1][0];	
		ApexPages.StandardController sc = new ApexPages.standardController(opp); 
        skedAddLicenseEntryCrl ctrl = new skedAddLicenseEntryCrl(sc);
        skedResponse res = skedAddLicenseEntryCrl.loadOpportunityData(opp.id);
        system.assertEquals(res.success, true);
        //skedOpportunityDataModal dataModal = (skedOpportunityDataModal)JSON.deserialize(res.result, 
		//	skedOpportunityDataModal.class);

		skedOpportunityDataModal dataModal = (skedOpportunityDataModal)res.result;
        skedAddLicenseEntryCrl.saveEntries(JSON.serialize(dataModal.entries), opp.id);

	}

	//@isTest static void test_saveEntries_failed() {
		// Implement test code
	//}

	@isTest static void test_deleteLicenseEntryItem() {
		Opportunity opp = [select id from Opportunity limit 1][0];	
		ApexPages.StandardController sc = new ApexPages.standardController(opp); 
        skedAddLicenseEntryCrl ctrl = new skedAddLicenseEntryCrl(sc);
        skedResponse res = skedAddLicenseEntryCrl.loadOpportunityData(opp.id);

		//skedOpportunityDataModal dataModal = (skedOpportunityDataModal)JSON.deserialize(res.result, 
		//	skedOpportunityDataModal.class);
		skedOpportunityDataModal dataModal = (skedOpportunityDataModal)res.result;
		system.assertEquals(dataModal.entries.size() > 0, true);
		skedAddLicenseEntryCrl.deleteLicenseEntryItem(dataModal.entries[0].lines[0].id);
		
	}

	@isTest static void test_deleteLicenseEntry_success() {
		Opportunity opp = [select id from Opportunity limit 1][0];	
		ApexPages.StandardController sc = new ApexPages.standardController(opp); 
        skedAddLicenseEntryCrl ctrl = new skedAddLicenseEntryCrl(sc);
        skedResponse res = skedAddLicenseEntryCrl.loadOpportunityData(opp.id);  

		//skedOpportunityDataModal dataModal = (skedOpportunityDataModal)JSON.deserialize(res.result, 
		//	skedOpportunityDataModal.class);
		skedOpportunityDataModal dataModal = (skedOpportunityDataModal)res.result;

		system.assertEquals(dataModal.entries.size() > 0, true);
		skedAddLicenseEntryCrl.deleteLicenseEntry(dataModal.entries[0].id);
	}

	@isTest static void test_deleteLicenseEntry_failed() {
		Opportunity opp = [select id from Opportunity limit 1][0];	
		ApexPages.StandardController sc = new ApexPages.standardController(opp); 
        skedAddLicenseEntryCrl ctrl = new skedAddLicenseEntryCrl(sc);
        skedResponse res = skedAddLicenseEntryCrl.loadOpportunityData(opp.id);        
		//skedOpportunityDataModal dataModal = (skedOpportunityDataModal)JSON.deserialize(res.result, 
		//	skedOpportunityDataModal.class);
		skedOpportunityDataModal dataModal = (skedOpportunityDataModal)res.result;
		system.assertEquals(dataModal.entries.size() > 0, true);
		skedAddLicenseEntryCrl.deleteLicenseEntry(dataModal.entries[0].id + 'failed');
	}
	
}