public with sharing class skedConvertCurrencyToAUD {

	private final sObject mysObject;
    private final id oppID;

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public skedConvertCurrencyToAUD(ApexPages.StandardController stdController) {
        this.oppID = stdController.getId();
    }

    public PageReference convertCurrency() {
        skedCorrectOpportunityCurrencyBatch b = new skedCorrectOpportunityCurrencyBatch(this.oppID);
        Database.executeBatch(b,1);
        PageReference p = new PageReference('/'+this.oppID);
        return p;
    }
}