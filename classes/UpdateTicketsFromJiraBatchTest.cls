@isTest
private class UpdateTicketsFromJiraBatchTest
{
	static testmethod void doTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Project__c prj1 = (Project__c)mapData.get('project 1');
		Project__c prj2 = (Project__c)mapData.get('project 2');
		List<Project__c> lstPrjs = new List<Project__c>{prj1, prj2};
		delete lstPrjs;

		Jira_Credential__c credential = new Jira_Credential__c();
		credential.Host__c = 'host';
		credential.Password__c = 'password';
		credential.Username__c = 'username';
		insert credential;

		test.starttest();
		UpdateTicketsFromJiraBatch mybatch = new UpdateTicketsFromJiraBatch();
		DataBase.executeBatch(mybatch);
		test.stoptest();
	}
}