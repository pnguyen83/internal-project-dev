global class GetAllProjectBatch implements Schedulable , Database.AllowsCallOuts 
{
    global void execute(SchedulableContext SC) {
        //'/rest/api/2/project' is jira api used to get all project information
        //list of jira api: https://docs.atlassian.com/jira/REST/cloud/#api/2/project-getAllProjects
        HttpResponse response = JiraUtil.connectToJira('/rest/api/2/project');
        Integer statusCode = response.getStatusCode();
        
        if (statusCode == 200) {
            Map<String, Project__c> mapProjects = new Map<String, Project__c>();
            Map<String, String> mapPrjType = new Map<String, String>();
            
            for (Project_Type__c prt : [SELECT Id, Jira_Name__c, SF_Value__c FROM Project_Type__c]) {
                mapPrjType.put(prt.Jira_Name__c, prt.SF_Value__c);
            }
            
            for (Project__c prj : [SELECT Id, Name, Project_Key__c FROM Project__c]) {
                mapProjects.put(prj.Project_Key__c, prj);
            }
            
            List<JsonProject> lstProject = (List<JsonProject>) System.JSON.deserialize(response.getBody(), List<JsonProject>.class);
            
            for (JsonProject json : lstProject) {
                if (mapProjects.containsKey(json.key)) {
                    ProjectUtil.updateProject(mapProjects.get(json.key),json,mapPrjType);
                }
                else {
                    Project__c prj = new Project__c();
                    ProjectUtil.updateProject(prj,json,mapPrjType);
                    mapProjects.put(prj.Project_Key__c, prj);                        
                }
            }
            SavePoint sp = Database.setSavepoint();
            try {
                upsert mapProjects.values();
                UpdateTicketsFromJiraBatch prjBatch = new UpdateTicketsFromJiraBatch();
                Database.executeBatch(prjBatch, 10); 
            } catch(Exception e) {
                Database.rollback(sp);
                LogUtil.generateLog('Error happens: ' + e.getMessage() + ', at line: ' + e.getLineNumber() + ', with stack trace setting: ' + e.getStackTraceString() + ' in GetAllProjectBatch class');
            }
        }
    }
}