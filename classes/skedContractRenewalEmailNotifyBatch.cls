global class skedContractRenewalEmailNotifyBatch implements Database.Batchable<sObject> {
    
    global skedContractRenewalEmailNotifyBatch() { }
    
    global Database.QueryLocator start(Database.BatchableContext context) {
        Datetime dt = Datetime.newInstance(system.today(), Time.newInstance(12, 0, 0, 0));
        Datetime dt60 = dt.addDays(60);
        Datetime dt90 = dt.addDays(90);
        string fdt60 = dt60.format('yyyy-MM-dd');
        string fdt90 = dt90.format('yyyy-MM-dd');

        String strQuery = ''
        + 'SELECT Id, Name, CSM__r.Email, CSM__r.Name, Current_Contract_Start_Date__c, Current_Contract_End_Date__c, '
        + '(SELECT Id, Name, Order_Status__c, Subscription_Contract_Start__c, Subscription_Contract_End__c FROM Orders__r) '
        + 'FROM Account '
        + 'WHERE (Current_Contract_End_Date__c = ' + fdt60 + ' OR Current_Contract_End_Date__c = ' + fdt90 + ') '
        + 'AND CSM__r.Email != null';

        System.debug(strQuery);

        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext context, List<Account> scope) {
        try {

            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();

            OrgWideEmailAddress owa = [SELECT id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName = 'Skedulo Accounts' LIMIT 1];
            List<EmailTemplate> templates = [SELECT id, Subject, HtmlValue, Body FROM EmailTemplate 
                                             WHERE Name = '60 Days Before Contract Renewal' OR Name = '90 Days Before Contract Renewal'
                                             ORDER BY Name ASC LIMIT 2];

            for (Account acc : scope) {
                
                if (acc.Orders__r.isEmpty()) {
                    continue;
                }

                Date activeEndDate = null; 
                for (Subscription_Contract__c cont : acc.Orders__r) {
                    if (cont.Order_Status__c == 'Active') { 
                        activeEndDate = cont.Subscription_Contract_End__c;
                        break;
                    }
                }
                Boolean isStopSendMail = false;

                if (activeEndDate != null) {
                    for (Subscription_Contract__c cont : acc.Orders__r) {
                        if (cont.Order_Status__c == 'Active') { continue; }
                        if (cont.Subscription_Contract_Start__c >= activeEndDate 
                            && cont.Subscription_Contract_Start__c <= activeEndDate.addDays(30)) {
                            isStopSendMail = true;
                            break;
                        }
                    }
                } else {
                    continue;
                }

                if (isStopSendMail) { continue; }

                Messaging.SingleEmailMessage m = new Messaging.SingleEmailMessage();

                if (acc.Current_Contract_End_Date__c == Date.today().addDays(60)) {
                    m = getMailNotification(acc, templates.get(0), owa.Id);
                } else if (acc.Current_Contract_End_Date__c == Date.today().addDays(90)) {
                    m = getMailNotification(acc, templates.get(1), owa.Id);
                }
                mails.add(m);
            }
            Messaging.sendEmail(mails);
        } catch (Exception ex) {
            System.debug('Error: ' + ex.getMessage());
        }

    }

    private Messaging.SingleEmailMessage getMailNotification(Account acc, EmailTemplate template, String fromAddress) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        mail.setToAddresses(new List<String> { acc.CSM__r.Email });
        //mail.setToAddresses(new List<String> { 'thuan.nguyen@skedulo.com' });
        mail.saveAsActivity = false;
        mail.setOrgWideEmailAddressId(fromAddress);

        // process the merge fields
        String subject = template.Subject;  
        subject = subject.replace('{!Account.CSM__c}', acc.CSM__r.Name);
        subject = subject.replace('{!Account.Name}', acc.Name);

        String plainBody = template.Body;
        plainBody = plainBody.replace('{!Account.CSM__c}', acc.CSM__r.Name);
        plainBody = plainBody.replace('{!Account.Name}', acc.Name);
        plainBody = plainBody.replace('{!Account.Current_Contract_End_Date__c}', String.valueOf(acc.Current_Contract_End_Date__c));

        mail.setSubject(subject);
        mail.setPlainTextBody(plainBody);

        return mail;
    }
    
    global void finish(Database.BatchableContext context) {
        
    }
    
}