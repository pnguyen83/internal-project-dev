public class LogUtil 
{
	public static void generateLog(String errorLog)
    {
        Custom_Log__c log = new Custom_Log__c();
        log.Log_Description__c = errorLog;
        insert log;
    }
}