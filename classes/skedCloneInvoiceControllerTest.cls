@isTest
private class skedCloneInvoiceControllerTest
{
	@testSetup static void setup() {
    	skedDataFactory.initCommonData();
  	}

  	@isTest static void testCloneInvoiceController() {
  		 Invoice_Line_Items__c invItem = skedDataFactory.createInvoiceLineItem();
  		 Invoice_Draft__c inv = [select id, name, Project__c,Invoice_Date__c,Invoice_Due_Date__c,Invoice_Status__c, Type__c, 
  		 					Subscription_Order__c, CurrencyIsoCode, OwnerId
  		 					from Invoice_Draft__c limit 1];
  		 System.debug('inv = ' + inv);
  		 ApexPages.StandardController sc = new ApexPages.standardController(inv); 
  		 skedCloneInvoiceController controller = new skedCloneInvoiceController(sc);
  		 Test.startTest();
  		 controller.save();
  		 controller.cancel();
  		 controller.saveAndNew();
  	}
}