@isTest
private class skedInvoiceLineItemsCrlTest {
	
	/*
		Init test data.
	*/
	@testSetup static void setup() {
		skedDataFactory.initCommonData();
	}

	@isTest static void test_saveEntries_success() {
       
		Invoice_Line_Items__c invItem = skedDataFactory.createInvoiceLineItem();
		Invoice_Draft__c inv = [select id, name, Project__c from Invoice_Draft__c limit 1];

		ApexPages.StandardController sc = new ApexPages.standardController(inv); 
        skedInvoiceLineItemsCrl ctrl = new skedInvoiceLineItemsCrl(sc);
        skedResponse resp = skedInvoiceLineItemsCrl.loadInvoiceData(inv.Id);
        skedInvoiceLineItemsCrl.InvoiceModal invModel = (skedInvoiceLineItemsCrl.InvoiceModal)resp.result;
        system.assertEquals(invModel.lineItems.size() > 0, true);
        //
        skedOpportunityDataModal.InvoiceEntryModal invEntryModel = new skedOpportunityDataModal.InvoiceEntryModal(inv);
        invEntryModel.lines = invModel.lineItems;
        List<skedOpportunityDataModal.InvoiceEntryModal>  entries = new List<skedOpportunityDataModal.InvoiceEntryModal>{invEntryModel};
        String entriesJson = JSON.serialize(entries);
        string projectId = [select id from Project__c limit 1][0].id;
        skedInvoiceLineItemsCrl.saveEntries(entriesJson, projectId);
	}

	@isTest static void test_deleteInvoiceEntryItem() {
		Invoice_Line_Items__c invItem = skedDataFactory.createInvoiceLineItem();
		Invoice_Draft__c inv = [select id, name, Project__c from Invoice_Draft__c];

		ApexPages.StandardController sc = new ApexPages.standardController(inv); 
        skedInvoiceLineItemsCrl ctrl = new skedInvoiceLineItemsCrl(sc);
        skedResponse resp = skedInvoiceLineItemsCrl.loadInvoiceData(inv.Id);
        skedInvoiceLineItemsCrl.InvoiceModal invModel = (skedInvoiceLineItemsCrl.InvoiceModal)resp.result;
        system.assertEquals(invModel.lineItems.size() > 0, true);

        skedInvoiceLineItemsCrl.deleteInvoiceEntryItem(invItem.id);
        skedInvoiceLineItemsCrl.deleteInvoiceEntry(inv.id);

	}



	
	
}