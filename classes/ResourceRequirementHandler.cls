public with sharing class ResourceRequirementHandler {
	private boolean m_isExecuting = false;
    
	public ResourceRequirementHandler(boolean isExecuting)
    {
        m_isExecuting = isExecuting;
    }
    
    public boolean isTriggerContext
    {
        get{ return m_isExecuting;}
    }

    public void onBeforeInsert(List<Resource_Requirement__c> lstNew) {
    	upsertProjectManagerRequirement(lstNew);
    }

    public void onBeforeUpdate(List<Resource_Requirement__c> lstNew, List<Resource_Requirement__c> lstOld) {
    	upsertProjectManagerRequirement(lstNew);
    }

    //====================================================================================//
    /**
	Only allow add 1 resource requirement record for Project Manager and the required number is 1
    */
    public static void upsertProjectManagerRequirement(List<Resource_Requirement__c> lstNew) {
    	Map<String, List<Resource_Requirement__c>> mapNew = new Map<String, List<Resource_Requirement__c>>();
    	Map<String, Resource_Requirement__c> mapNewRR = new Map<String, Resource_Requirement__c>();

    	for (Resource_Requirement__c rr : lstNew) {
    		if (rr.Resource_Category_Name__c == HardcodeUtil.PROJECT_MANAGER) {
    			if (rr.Required_Number__c > 1) {
    				rr.Required_Number__c.addError('Each project can have 1 Project Manager only');
    				continue;
    			}
    			List<Resource_Requirement__c> lstRR = mapNew.get(rr.Project__c);
    			if (lstRR == null) {
    				lstRR = new List<Resource_Requirement__c>();
    			}
    			lstRR.add(rr);
    			mapNew.put(rr.Project__c, lstRR);
    			mapNewRR.put(rr.Id, rr);
    		}
    	}

    	if (!mapNew.isEmpty()) {
    		for (Resource_Requirement__c rr : [SELECT Id, Project__c, Required_Number__c 
    											FROM Resource_Requirement__c 
    												WHERE Resource_Category_Name__c = :HardcodeUtil.PROJECT_MANAGER]) {
    			if (mapNew.containsKey(rr.Project__c)) {
    				List<Resource_Requirement__c> lstRR = mapNew.get(rr.Project__c);
    				if (!lstRR.isEmpty()) {
    					for (Resource_Requirement__c newRR : lstRR) {
    						if (newRR.Id != rr.Id) {
    							newRR.addError('There is a record with resource category is Project Manager already');	
    						}
    					}
    				}
    			}
    		}
    	}
    }
}