public class skedUtil {

	public static string CLOSED_WON = 'Closed Won';
	public static String DATE_FORMAT = 'yyyy-MM-dd';

	/**
	 * 
	 */
	public static string toDateString(Date inputDate) {
		if (inputDate == null) return '';
		Datetime dtime = Datetime.newInstance(inputDate, Time.newInstance(0, 0, 0, 0));
		return dtime.format(DATE_FORMAT);
	}

	/**
	 * 
	 */
	public static Date toDate(String ds) {
		list<String> dString = ds.split('-');
		if (dString.size() < 3) return null;
		return Date.newInstance(Integer.valueOf(dString[0]), Integer.valueOf(dString[1]), Integer.valueOf(dString[2].substring(0, 2)));

	}

	public static list<skedOpportunityDataModal.PickListItem> getPicklistValues(string objName, String fld) {

		list<skedOpportunityDataModal.PickListItem> options = new list<skedOpportunityDataModal.PickListItem> ();
		Schema.DescribeSObjectResult objDescribe = Schema.getGlobalDescribe().get(objName).getDescribe();
		// Get a map of fields for the SObject
		map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
		// Get the list of picklist values for this field.
		list<Schema.PicklistEntry> values = fieldMap.get(fld).getDescribe().getPickListValues();
		// Add these values to the selectoption list.
		for (Schema.PicklistEntry a : values)
		{
			options.add(new skedOpportunityDataModal.PickListItem(a.getValue(), a.getLabel()));
		}
		return options;
	}

	// Returns a dynamic SOQL statement for the whole object, includes only creatable fields since we will be inserting a cloned result of this query
	public static string getCreatableFieldsSOQL(String objectName) {

		String selects = '';

		// Get a map of field name and field token
		Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
		list<string> selectFields = new list<string> ();

		if (fMap != null) {
			for (Schema.SObjectField ft : fMap.values()) { // loop through all field tokens (ft)
				Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
				if (fd.isCreateable()) { // field is creatable
					selectFields.add(fd.getName());
				}
			}
		}

		if (!selectFields.isEmpty()) {
			for (string s : selectFields) {
				selects += s + ',';
			}
			if (selects.endsWith(',')) { selects = selects.substring(0, selects.lastIndexOf(',')); }

		}

		return 'SELECT ' + selects + ' FROM ' + objectName + ' WHERE ';

	}
}