public class JiraUtil implements Database.AllowsCallOuts
{
    //Use to send request to jira based on the jira api url passing
    public static HttpResponse connectToJira(String jiraURL)
    {
        Jira_Credential__c jiraLogin = Jira_Credential__c.getOrgDefaults();
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http host = new Http();
        String endpoint = jiraLogin.Host__c + jiraURL;
        request.setEndpoint(jiraLogin.Host__c + jiraURL);
        request.setMethod('GET');
        request.setHeader('Content-Type', 'application/json');
        
        Blob headerValue = Blob.valueOf(jiraLogin.Username__c + ':' + jiraLogin.Password__c);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue); 
        request.setHeader('Authorization', authorizationHeader);
        request.setTimeout(12000);
        if (!test.isRunningTest()) {
            response = host.send(request);    
        } else {
            response.setStatusCode(200);
            JsonProject.Category category = new JsonProject.Category();
            category.name = 'category name';
            category.description = 'category description';
            JsonProject jsonTest = new JsonProject();
            jsonTest.expand = 'description,lead,issueTypes,url,projectKeys';
            jsonTest.self = 'https://internaltesting.atlassian.net/rest/api/2/project/10400';
            jsonTest.id = '10400';
            jsonTest.key = 'FIJ0025';
            jsonTest.name = 'Fiji Project';
            jsonTest.projectTypeKey = 'business';
            jsonTest.description = 'this is description';
            jsonTest.projectCategory = category;
            List<JsonProject> lstJsonProject = new List<JsonProject>{jsonTest};
            String strJSON = JSON.serialize(lstJsonProject);
            response.setBody(strJSON);
        }
        
        
        return response;
    }
    
    //===========================================================================================================================//
    public static HttpResponse createProject(String prjKey, String name, String description, String type, String contactName, Jira_Credential__c jiraLogin)
    {
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http host = new Http();
        
        JiraProject prj = new JiraProject();
        prj.key = prjKey.toUpperCase();
        prj.name = name;
        prj.projectTypeKey = type.toLowerCase();
        prj.description = description;
        prj.lead = contactName;
        prj.url = 'http://atlassian.com';
        prj.assigneeType = 'PROJECT_LEAD';
        
        String body = JSON.serialize(prj);
        request.setBody(body);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        request.setEndpoint(jiraLogin.Host__c + '/rest/api/2/project');
        
        Blob headerValue = Blob.valueOf(jiraLogin.Username__c + ':' + jiraLogin.Password__c);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue); 
        request.setHeader('Authorization', authorizationHeader);
        request.setTimeout(120000);
        if (!test.isRunningTest()) {
            response = host.send(request);    
        }
        
        return response;
    }
    
    //=================================================================================================================//
    public static HttpResponse updateJiraProject(String prjKey, String name, String description, String type, String contactName,Jira_Credential__c jiraLogin) {
        HttpRequest request = new HttpRequest();
        HttpResponse response = new HttpResponse();
        Http host = new Http();
        
        JiraProject prj = new JiraProject();
        prj.key = prjKey.toUpperCase();
        prj.name = name;
        prj.projectTypeKey = type.toLowerCase();
        prj.description = description;
        prj.lead = contactName;
        prj.url = 'http://atlassian.com';
        prj.assigneeType = 'PROJECT_LEAD';
        
        String body = JSON.serialize(prj);
        request.setBody(body);
        request.setMethod('PUT');
        request.setHeader('Content-Type', 'application/json');
        request.setEndpoint(jiraLogin.Host__c + '/rest/api/2/project/' + prjKey.toUpperCase());
        
        Blob headerValue = Blob.valueOf(jiraLogin.Username__c + ':' + jiraLogin.Password__c);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue); 
        request.setHeader('Authorization', authorizationHeader);
        request.setTimeout(120000);
        if (!test.isRunningTest()) {
            response = host.send(request);    
        }
        
        return response;
    }
    
    //=================================================================================================================//
    public static void getAllJiraTickets(String prjKey, String prjID, Map<String, Jira_Ticket__c> mapJiraTicket, 
                                         Map<String, sked__Resource__c> mapResources, Map<String, String> mapResourceTickets) {
        HttpResponse response = connectToJira('/rest/api/2/search?jql=project="' + prjKey + '"&maxResults=-1');
        Integer statusCode = response.getStatusCode();

        if (test.isRunningTest()) {
            JsonTask.cls_statusCategory cate = new JsonTask.cls_statusCategory();
            cate.self = 'self';
            cate.id = 200;
            cate.key = 'key';
            cate.colorName = 'colorName';
            cate.name = 'name';

            JsonTask.cls_status status = new JsonTask.cls_status();
            status.self = 'self';
            status.iconUrl = 'iconUrl';
            status.description = 'description';
            status.name = 'name';
            status.id = 'id';
            status.statusCategory = cate;

            JsonTask.cls_issuetype issueType = new JsonTask.cls_issuetype();
            issueType.name = 'name';

            JsonTask.cls_priority priority = new JsonTask.cls_priority();
            priority.name = 'name';

            JsonTask.cls_assignee assignee = new JsonTask.cls_assignee();
            assignee.key = 'admin';
            assignee.displayName = 'displayName';

            JsonTask.cls_fields field = new JsonTask.cls_fields();
            field.summary = 'summary';
            field.status = status;
            field.description = 'description';
            field.aggregatetimeoriginalestimate = 7200;
            field.timeestimate = 3600;
            field.issuetype = issueType;
            field.priority = priority;
            field.assignee = assignee;

            JsonTask.cls_issues iss = new JsonTask.cls_issues();
            iss.key = 'key2';
            iss.fields = field;

            JsonTask jTask = new JsonTask();
            jTask.issues = new List<JsonTask.cls_issues>{iss};
            String strJSON = JSON.serialize(jTask);
            response.setBody(strJSON);
        }
        
        if (statusCode == 200) {
            JsonTask task = JsonTask.parse(response.getBody());
            //update jira ticket on salesforce
            for (JsonTask.cls_issues iss : task.issues) {
                ProjectTicketUtils.updateProjectTicketsFromJira(iss,prjID,mapJiraTicket,mapResources, mapResourceTickets);
            }
        }
    }  
    
    //=============================================================Custom Classes=======================================//
    public class JiraProject
    {
        public String key;
        public String name;
        public String description;
        public String assigneeType;
        public String projectTypeKey;
        public String lead;
        public String url;
    }
    
    //use to catch response message from Jira
    public class JsonMessage 
    {
        public Map<String,String> errors;
    }
    
    public static JsonMessage parse(String json){
        return (JsonMessage) System.JSON.deserialize(json, JsonMessage.class);
    }
}