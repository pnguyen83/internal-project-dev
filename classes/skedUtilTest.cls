@isTest
private class skedUtilTest
{
	static testmethod void GetDifferentDaysTest() {
		test.starttest();
		skedUtils.GetDifferentDays(String.valueOf(Date.today()), String.valueOf(Date.today().addDays(1)));
		skedUtils.GetStartOfDate(String.valueOf(Date.today()), 'Australia/Brisbane');
		skedUtils.GetStartOfDate(System.now(), 'Australia/Brisbane');
		skedUtils.GetEndOfDate(System.now(), 'Australia/Brisbane');
		skedUtils.ConvertDateToIsoString(Date.today());
		skedUtils.convertToTimezone(System.now(), 'Australia/Brisbane');
		skedUtils.ConvertToDateValue('2017-4-4');
		skedUtils.ConvertBetweenTimezones(System.now(), 'Australia/Brisbane', 'Asia/Ho_Chi_Minh');
		skedUtils.ConvertTimeNumberToMinutes(800);
		skedUtil.getPicklistValues('Invoice_Line_Items__c', 'Type__c');
		skedUtil.toDate(skedUtils.toDateString(Date.today()));
		test.stoptest();
	}
}