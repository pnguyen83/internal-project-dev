@isTest
private class ObjectTest
{
	static testmethod void doTest() {
		String JSONContent = '{"isActive":true}';
		JSONParser parser = JSON.createParser(JSONContent);
		String JSONContent2 = '{"firstName":"John"}';
		JSONParser parser2 = JSON.createParser(JSONContent2);

		test.starttest();
		JiraProject.consumeObject(parser);
		JiraProject.consumeObject(parser2);
		JiraProject.Components comp = new JiraProject.Components();
		JiraProject.IssueTypes issue = new JiraProject.IssueTypes();
		JiraProject.AvatarUrls ava = new JiraProject.AvatarUrls();
		ava.a48x48 = '';
		ava.a24x24 = '';
		ava.a16x16 = '';
		ava.a32x32 = '';

		JiraProject.Lead lead = new JiraProject.Lead();
		JiraProject.Roles role = new JiraProject.Roles();
		JiraProject.Project prj = new JiraProject.Project();
		prj.self = '';
		prj.Id = '';
		prj.key = '';
		prj.name = '';
		prj.projectTypeKey = '';
		prj.description = '';
		prj.lead = lead;
		prj.assigneeType = '';
		prj.avatarUrls = ava;
		
		JiraProject jiraPrj = new JiraProject();
		String strJSON = JSON.serialize(jiraPrj);
		JiraProject.parse(strJSON);

		JsonTask jTask = new JsonTask();
		String strJSON2 = JSON.serialize(jTask);
		JsonTask.parse(strJSON2);
		test.stoptest();
	}
}