public class ResourceTicketExtension {
    public List<DevWrapper> lstDevTicket {get;set;}
    public List<SelectOption> lstDevOptions { get;set; }
    public string selectedValue { get;set; }
    public Integer index = 4;
    public Integer start = 0;
    public boolean next {get;set;}
    public boolean previous {get;set;}
    public Integer count {get;set;}
    public List<DevWrapper> lstDev {get;set;}
    public List<DevWrapper> lstSearch {get;set;}
    
    public ResourceTicketExtension(ApexPages.StandardController stdController) {
        if (!test.isRunningTest()) {
            stdController.addFields(new List<String>{'Name'});    
        }
        
        Project__c prj = (Project__c)stdController.getRecord();
        if (String.isNotBlank(prj.Name)) {
            lstDev = new List<DevWrapper>();
            lstDevTicket = new List<DevWrapper>();
            lstSearch = new List<DevWrapper>();
            List<Project__c> lstProject = [SELECT Id, (SELECT Id, Resource__r.Name FROM Project_Resources__r WHERE Project__c = :prj.Id),
                                  (SELECT Id,Name, Subject__c, Resource__r.Name, Status__c, Original_Estimate__c, Priority__c FROM Jira_Tickets__r ORDER BY Name ASC)
                                  FROM Project__c WHERE Id = :prj.Id
                                  ];
            
            if (lstProject != null && !lstProject.isEmpty()) {
                Project__c project = lstProject.get(0);
                
                if (!project.Project_Resources__r.isEmpty()) {
                    lstDevOptions = new List<SelectOption>();
                    lstDevOptions.add(new SelectOption('All','All'));
                    for (Project_Resource__c prjR : project.Project_Resources__r) {
                        lstDevOptions.add(new SelectOption(prjR.Resource__r.Name,prjR.Resource__r.Name));
                    }
                    
                    if (!project.Jira_Tickets__r.isEmpty()) {
                        for (Jira_Ticket__c jira : project.Jira_Tickets__r) {
                            lstDevTicket.add(new DevWrapper(jira.Resource__r.Name, jira));
                        }
                    }
                    count = lstDevTicket.size();
                    initListDev(false, true);
                }
            }
        } 
    }
    
    //Get picklist value and return search result 
    public void checkDeveloper() {
        start = 0;
        index = 4;
        lstSearch.clear();
        lstDev.clear();
        count = lstDevTicket.size();
        initListDev(false, true);
    }
    
    //Move to next page
    public void next() {
        index = index + 4;
        start = start + 4;
        lstDev.clear();
        if(index > count)
        {
            index = Math.Mod(count,4) + start;
            initListDev(true, false);
            index = start + 4;    
        }
        else
        {
            initListDev(next, false);
        }   
    }
    
    //move to previous page
    public void previous()
    {
        lstDev.clear();
        if(start > 4) {    
            index = index - 4;
            start = start - 4;
            initListDev(false, false);
        }    
        else {
            index = index - 4;
            start = start - 4;
            initListDev(false, true);
        }   
    }
    
    //Get list of data to display
    public void initListDev(Boolean isNext, Boolean isPrev) {
        if (!lstDevTicket.isEmpty()) {
            if (count <= index) {
                index = count;
                isNext = true;
            }  
            if (selectedValue != null && !selectedValue.equals('All')) {
                for (Integer i = 0; i < count; i++) {
                    DevWrapper temp = lstDevTicket.get(i);
                    if (temp.devName.equals(selectedValue)) {
                        lstSearch.add(lstDevTicket.get(i));
                    }
                }
                count = lstSearch.size();
                if (count > 4) {
                    for (Integer i = 0; i < 4; i++) {
                        lstDev.add(lstSearch.get(i));
                    }
                    isNext = false;
                }
                else {
                    lstDev = lstSearch;
                    isNext = true;
                }
            }
            else {
                for (Integer i = start; i < index; i++) {
                    lstDev.add(lstDevTicket.get(i));
                }
                count = lstDevTicket.size();
            }
            previous = isPrev;
            next = isNext;
        }
    }
    
    public class DevWrapper {
        public String devName {get;set;}
        public Jira_Ticket__c ticket {get;set;}
        
        public DevWrapper(String name, Jira_Ticket__c jira) {
            devName = name;
            if (jira != null) { 
                ticket = jira;
            }
            else {
                ticket = new Jira_ticket__c(Subject__c = '',Status__c = '',Priority__c='',Original_Estimate__c = 0);
            }
        }
    }
}