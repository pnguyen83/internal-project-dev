//This class contains all Hardcode string that can be used in another class
public class HardcodeUtil 
{
    public static final Integer	DEFAULT_DAY_NUMBER = 7;
	public static final String CLOSED_WON_STATUS = 'Closed Won';
    public static final String DUPLICATED_OPPORTUNITY_NAME_MESSAGE = 'There is an existing opportunity with this name in the database';
    public static final String UPDATE_CLOSED_WON_ERROE_MESSAGE   = 'Cannot update a Closed Won Opportunity';
    public static final String DUPLICATED_PROJECT_NAME_MESSAGE   = 'There is an existing project with the name ';
    public static final String DUPLICATED_PROJECT_KEY_MESSAGE    = 'There is an existing project with that project key, please choose another one';
    public static final String JIRA_USERNAME_NOT_EXIST_MESSAGE 	 = 'This username is not existing in Jira: ';
    public static final String JIRA_USER_NOT_EXISTING_MESSAGE    = 'There is not any user for this Project Manager in Jira: ';
    public static final String CREATE_JIRA_PROJECT_ERROR_MESSAGE = 'Cannot create project in Jira';
    public static final String CLOSED_WON_OPPORTUNITY_MESSAGE    = 'Please Closed Won this Opportunity before generating Project';
    public static final String OPPORTUNITY_HAS_PROJECT_MESSAGE   = 'There is a project associated with this opportunity, cannot create a new one';
    public static final String EDIT_JIRA_TICKET_ERROR 			 = 'Cannot update/delete a Jira Ticket on Salesforce';	

    public static final String PROJECT_MANAGER = 'Project Manager';
    public static final String PROJECT_MANAGER_KEY = 'PM';
}