public with sharing class skedOpportunityLineItemHandler {

    public static void onBeforeInsert(list<OpportunityLineItem> newItems){
        // SIS-137
        // validation(newItems);
    }

    /*public static void validation(list<OpportunityLineItem> newItems){

        Set<id> setOppo     = new Set<Id>();
        Set<id> setProduct  = new Set<Id>();

        for(OpportunityLineItem item: newItems){
            setOppo.add(item.OpportunityId);
            setProduct.add(item.Product2Id);
        }

        map<id,Opportunity> mapOpportunity = new map<id,Opportunity>([
            select id, RecordType.name from Opportunity where id IN:setOppo]);

        map<id,Product2> mapProduct = new map<id,Product2>([
            select id, Family from Product2 where id in:setProduct]);

        //avoid Service Product tobe add to "License" Opportunity.
        for(OpportunityLineItem item: newItems){
            Product2 pro = mapProduct.get(item.Product2Id);
            Opportunity opp = mapOpportunity.get(item.OpportunityId);
            if(pro == null || opp == null) continue;
            boolean isServiceOpp = opp.RecordType.name == 'Service'; 
            boolean isServiceProduct = pro.Family == 'Service';

            if(isServiceProduct && !isServiceOpp 
                || !isServiceProduct && isServiceOpp){
                item.addError('Product Type do not match Opportunity Type.');
            }

        }
    }*/
	/*
    public static void recalculateOpportunityAmount(list<OpportunityLineItem> newItems, map<id,OpportunityLineItem> oldMap,boolean isDeleted){

        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('License').getRecordTypeId();
        set<id> oppIds = new set<id>();
        if(oldMap == null || isDeleted){
            for(OpportunityLineItem oppLine : newItems){
                oppIds.add(oppLine.OpportunityId);
            }
        }else{
            for(OpportunityLineItem oppLine: newItems){
                OpportunityLineItem oldLine = oldMap.get(oppLine.id);
                if(oppLine.Quantity != oldLine.Quantity || oppLine.UnitPrice != oldLine.UnitPrice){
                    oppIds.add(oppLine.OpportunityId);
                }
            }
        }

        List<Opportunity> opps = [select id, Amount,(select id,TotalPrice from OpportunityLineItems ) from Opportunity where id in: oppIds and recordTypeId = :oppRecordTypeId];

        for(Opportunity opp : opps){
            opp.Amount = 0;
            for(OpportunityLineItem line: opp.OpportunityLineItems){
                opp.Amount += line.TotalPrice * 12;
            }
        }

        system.debug('opp Amount ' + opps);

        if(opps.size() > 0){
            update opps;
        }

    }
    */
}