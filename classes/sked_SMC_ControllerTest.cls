@isTest
private class sked_SMC_ControllerTest
{
	static testmethod void getConfigDataTest() {
		Map<String, sObject> mapData = TestUtilities.createData();

		test.starttest();
		sked_SMC_Controller.getConfigData();
		sked_SMC_Controller.getRegions();
		sked_SMC_Controller.getPickListValues('Project__c', 'Jira_Project_Type__c');
		sked_SMC_Controller.getShiftTypeSettings();
		test.stoptest();
	}

	static testmethod void getProviderListTest() {
		Map<String, sObject> mapData = TestUtilities.createData();

		test.starttest();
		sked_SMC_Controller.getProviderList(String.valueOf(Date.today()));
		test.stoptest();
	}

	static testmethod void deleteShiftTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		sked__Availability__c avai1 = (sked__Availability__c)mapData.get('availability 2');
		sked_SMC_Controller.saveShiftModel shiftModel = new sked_SMC_Controller.saveShiftModel();
		shiftModel.Id = avai1.Id;
		String strJSON = JSON.serialize(shiftModel);

		test.starttest();
		sked_SMC_Controller.deleteShift(strJSON);
		test.stoptest();
	}

	static testmethod void saveReplicationTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		sked__resource__c res1 = (sked__resource__c)mapData.get('resource 1');
		sked__region__c region1 = (sked__region__c)mapData.get('region 2');
		Date startDate = Date.today();
		Date endDate = startDate.addDays(8);
		String strStartDate = '';
		String strEndDate = '';
		String locale = UserInfo.getLocale().toLowerCase();
		if (locale.contains('en_us')) {
			strStartDate = startDate.month() + '/' + startDate.day() + '/' + startDate.year();
			strEndDate = endDate.month() + '/' + endDate.day() + '/' + endDate.year();
		} else {
			strStartDate = startDate.day() + '/' + startDate.month() + '/' + startDate.year();
			strEndDate = endDate.day() + '/' + endDate.month() + '/' + endDate.year();
		}

		sked_SMC_Controller.saveReplicationModel saveRepModel = new sked_SMC_Controller.saveReplicationModel();
		saveRepModel.resourceId = res1.Id;
		saveRepModel.regionTimezone = region1.sked__Timezone__c;
		saveRepModel.canOverride = true;
		saveRepModel.sourceDate = strStartDate;
		saveRepModel.startDateString = strStartDate;
		saveRepModel.endDateString = strEndDate;

		String strJSON = JSON.serialize(saveRepModel);

		test.starttest();
		sked_SMC_Controller.saveReplication(strJSON);
		test.stoptest();
	}

	static testmethod void saveShiftCreationTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		sked__region__c region1 = (sked__region__c)mapData.get('region 2');
		sked__resource__c res1 = (sked__resource__c)mapData.get('resource 2');
		String dateString = String.valueOf(Date.today());
		Integer startTime = 800;
		Integer endTime = 1200;
		
		sked_SMC_Controller.saveShiftModel model = new sked_SMC_Controller.saveShiftModel();
		model.startTime = startTime;
		model.endTime = endTime;
		model.dateString = dateString;
		model.resourceId = res1.Id;
		model.regionTimezone = region1.Id;
		model.recurringShift = true;
		model.shiftType = 'Available';
		String strJSON = JSON.serialize(model);

		test.starttest();
		sked_SMC_Controller.saveShiftCreation(strJSON);
		test.stoptest();
	}


}