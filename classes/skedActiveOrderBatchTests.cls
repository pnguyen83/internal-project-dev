@isTest
private class skedActiveOrderBatchTests
{
	@isTest
	static void testActiveORderBatch()
	{
		skedDataFactory.initCommonData();
		Opportunity opp = [select id, AccountId, Start_Date__c, End_Date__c from Opportunity limit 1];
		Contact cont1 = [select id from Contact where AccountId =:opp.AccountId limit 1];
        Subscription_Contract__c subOrder = skedDataFactory.createSubscription_Orderr(opp, cont1);
		Order__c order = skedDataFactory.createOrder(subOrder);
		Test.startTest();
			 skedUpdateOrderActiveFieldSchedule  os = new skedUpdateOrderActiveFieldSchedule();
			 string sch =' 0 0 13 * * ?';
			 system.schedule('Test',sch,os);
        Test.stopTest();

	}
}