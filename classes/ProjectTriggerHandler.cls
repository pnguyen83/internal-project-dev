public class ProjectTriggerHandler 
{
	private boolean m_isExecuting = false;
    
    /**
    Constructor
    */
	public ProjectTriggerHandler(boolean isExecuting)
    {
        m_isExecuting = isExecuting;
    }
    
    public boolean isTriggerContext
    {
        get{ return m_isExecuting;}
    }
    
    /**
    Logic after insert projects
    */
    public void onAfterInsert(List<Project__c> lstNew)
    {
        //check if Project Manager is null or not, if not, update or insert a PM Project Resource
        ProjectUtil.updatePMProjectResource(lstNew, null);
        //create Project Time Line for the new projects
        ProjectUtil.isFromTrigger = true;
        updateAccountStorage(lstNew);    
        createProjectStatusUpdate(lstNew, null); 
        createProjectMileStone(lstNew);     
    }
    
    /**
    Logic after update projects
    */
    public void onAfterUpdate(List<Project__c> lstNew, List<Project__c> lstOld)
    {
        //check if Project Manager is null or not, if not, update or insert a PM Project Resource
        ProjectUtil.updatePMProjectResource(lstNew, lstOld);
        //check if Project Manager is removed or not, if is removed, delete the corresponding PM Project Resource
        ProjectUtil.getProjectsRemovedPM(lstNew, lstOld);
    	//Update StartDate and End Date of Project Resources
    	ProjectUtil.updateAllPrjResources(lstNew);   
        //Create Project Status Update record
        createProjectStatusUpdate(lstNew, lstOld);

        List<Project__c> lstNewPrjs = new List<Project__c>();
        lstNewPrjs = getUpdateStorageProjects(lstNew, lstOld);
        if (!lstNewPrjs.isEmpty()) {
            updateAccountStorage(lstNewPrjs);
        } 
    }

    /**
    Logic after delete projects
    */
    public void onAfterDelete(List<Project__c> lstOld) {
        ProjectUtil.deleteOpportunityProjectField(lstOld);
    }

    /**
    Create Project Status Update when Current Status Note field of Project is updated    
    */
    private void createProjectStatusUpdate(List<Project__c> lstNew, List<Project__c> lstOld) {
        Map<String, Project__c> mapOldProjects = new Map<String, Project__c>();
        List<Project_Status_Update__c> lstResult = new List<Project_Status_Update__c>();

        if (lstOld != null) {
            for (Project__c prj : lstOld) {
                mapOldProjects.put(prj.id, prj);
            }    
        }
        

        for (Project__c prj : lstNew) {
            if (prj.Current_Status_Notes__c != null) {
                Project__c oldPrj = mapOldProjects.get(prj.id);
                if ((oldPrj != null && String.isNotBlank(prj.Current_Status_Notes__c) && (oldPrj.Current_Status_Notes__c == null || oldPrj.Current_Status_Notes__c != prj.Current_Status_Notes__c))||
                    lstOld == null) {
                    Project_Status_Update__c prjStatusUpdate = new Project_Status_Update__c(
                        Date__c = System.today(),
                        Project__c = prj.id,
                        Project_Status_Update__c = prj.Current_Status_Notes__c,
                        User__c = UserInfo.getName()
                    );
                    lstResult.add(prjStatusUpdate);
                }
            }
        }

        if (!lstResult.isEmpty()) {
            insert lstResult;
        }


    }

    /**
    Get projects that update Dropbox_Link__c and Google_Drive_Link__c
    */
    private List<Project__c> getUpdateStorageProjects(List<Project__c> lstNew, List<Project__c> lstOld) {
        Map<String, Project__c> mapNewPrjs = new Map<String, Project__c>();

        for (Project__c prj : lstNew) {
            if (prj.Account__c != null) {
                mapNewPrjs.put(prj.Id, prj);
            }
        }

        for (Project__c prj : lstOld) {
            if (mapNewPrjs.containsKey(prj.Id)) {
                Project__c newPrj = mapNewPrjs.get(prj.Id);
                if (newPrj.Dropbox_Link__c == prj.Dropbox_Link__c && newPrj.Google_Drive_Link__c == prj.Google_Drive_Link__c) {
                    mapNewPrjs.remove(prj.Id);
                }
            }
        }

        return mapNewPrjs.values();
    }

    /**
    Update Account related to projects
    */
    private void updateAccountStorage(List<Project__c> lstNewPrjs) {
        Set<String> setAccIDs = new Set<String>();
        List<Account> lstAccounts = new List<Account>();

        for (Project__c prj : lstNewPrjs) {
            if (prj.Account__c != null) {
                setAccIDs.add(prj.Account__c);
            }
        }

        for (Account acc : [SELECT Id, Client_Dropbox_Link__c, Client_Google_Docs_Link__c FROM Account WHERE Id IN : setAccIDs]) {
            for (Project__c prj : lstNewPrjs) {
                if (prj.Account__c == acc.Id) {
                    Integer i = 0;
                    if (prj.Dropbox_Link__c != acc.Client_Dropbox_Link__c) {
                        acc.Client_Dropbox_Link__c = prj.Dropbox_Link__c;
                        i++;
                    }
                    if (prj.Google_Drive_Link__c != acc.Client_Google_Docs_Link__c) {
                        acc.Client_Google_Docs_Link__c = prj.Google_Drive_Link__c;
                        i++;
                    }

                    if (i > 0) {
                        lstAccounts.add(acc);    
                    }
                    
                }
            }
        }

        if (!lstAccounts.isEmpty()) {
            update lstAccounts;
        }
    }

    public static void createProjectMileStone(List<Project__c> lstNews) {
        List<Project_Milestone__c> lstProjectMilestone = new List<Project_Milestone__c>();
        
        for (Project__c prj : lstNews) {
            Project_Milestone__c scopeKickOff = new Project_Milestone__c(
                Project__c = prj.id,
                Name = 'Scope Kick Off',
                Actual_Date__c = prj.Actual_Scope_Kick_Off__c
            );
            lstProjectMilestone.add(scopeKickOff);

            Project_Milestone__c devKickOff = new Project_Milestone__c(
                Project__c = prj.id,
                Name = 'Development Kick Off',
                Actual_Date__c = prj.Actual_Development_Kick_Off__c
            );
            lstProjectMilestone.add(devKickOff);

            Project_Milestone__c uatKickOff = new Project_Milestone__c(
                Project__c = prj.id,
                Name = 'UAT Kick Off',
                Actual_Date__c = prj.Actual_UAT_Kick_Off__c
            );  
            lstProjectMilestone.add(uatKickOff);

            Project_Milestone__c deployToProd = new Project_Milestone__c(
                Project__c = prj.id,
                Name = 'Deploy to Prod',
                Actual_Date__c = prj.Actual_Deploy_to_Prod__c
            ); 
            lstProjectMilestone.add(deployToProd);

            Project_Milestone__c goLiveDate = new Project_Milestone__c(
                Project__c = prj.id,
                Name = 'Go Live Date',
                Actual_Date__c = prj.Actual_Go_Live_Date__c
            );  
            lstProjectMilestone.add(goLiveDate);        

            Project_Milestone__c hypercareKickOff = new Project_Milestone__c(
                Project__c = prj.id,
                Name = 'Hypercare Kick Off',
                Actual_Date__c = prj.Actual_Hypercare_Kick_Off__c
            );
            lstProjectMilestone.add(hypercareKickOff);

            Project_Milestone__c projectCompletion = new Project_Milestone__c(
                Project__c = prj.id,
                Name = 'Project Completion',
                Actual_Date__c = prj.Actual_Project_Completion__c
            );
            lstProjectMilestone.add(projectCompletion);
            
        }

        insert lstProjectMilestone;
    }
}