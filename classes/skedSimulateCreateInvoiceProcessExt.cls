public with sharing class skedSimulateCreateInvoiceProcessExt {

	private final sObject mysObject;
    private final id contractID;

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public skedSimulateCreateInvoiceProcessExt(ApexPages.StandardController stdController) {
        this.contractID = stdController.getId();
    }

    public PageReference createInvoice() {
        skedCreateInvoicesBatchTemp b = new skedCreateInvoicesBatchTemp(this.contractID);
        Database.executeBatch(b,1);
        PageReference p = new PageReference('/'+this.contractID);
        return p;
    }
}