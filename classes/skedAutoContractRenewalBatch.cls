global class skedAutoContractRenewalBatch implements Database.Batchable<SObject> {

	global skedAutoContractRenewalBatch() { }
	
	global Database.QueryLocator start(Database.BatchableContext context) {
		// 60 days prior to a contract end date only
		Datetime dt = Datetime.newInstance(system.today(), Time.newInstance(12, 0, 0, 0));
		dt = dt.addDays(60);
		string fdt = dt.format('yyyy-MM-dd');
		String strQuery = ''
		+ 'SELECT Id, Account__c, Subscription_Contract_Start__c, Subscription_Contract_End__c, Initial_Opportunity__c, Initial_Opportunity__r.CurrencyIsoCode, (SELECT Id, Name, Start_Date__c, RecordTypeId FROM Opportunities__r) '
		+ 'FROM Subscription_Contract__c '
		+ 'WHERE Subscription_Contract_End__c = ' + fdt + ' AND Order_Status__c = \'Active\' '
		+ 'AND Initial_Opportunity__c != null ';

		System.debug(strQuery);

		return Database.getQueryLocator(strQuery);
	}

   	global void execute(Database.BatchableContext context, List<Subscription_Contract__c> scope) {
		
		Map<Id, String> mapIgnored = new Map<Id, String>();
		Map<Id, Opportunity> mapInitOpp = new Map<Id, Opportunity>();
		Set<Id> setOppIds = new Set<Id>();
		Set<String> setOppCurrency = new Set<String>();
		Map<Id, Id> mapAccountId = new Map<Id, Id>();
		
		Id oppRenewalRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(SkeduloConstants.OPP_RECORDTYPE_RENEWAL).getRecordTypeId();
		Id oppInitialRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(SkeduloConstants.OPP_RECORDTYPE_INITIAL).getRecordTypeId();
		Id oppAddOnRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(SkeduloConstants.OPP_RECORDTYPE_ADDON).getRecordTypeId();

		for (Subscription_Contract__c SC : scope) {

			mapAccountId.put(SC.Id, SC.Account__c);

			if (SC.Opportunities__r.isEmpty()) {
				continue;
			}

			for (Opportunity opp : SC.Opportunities__r) {
				setOppIds.add(opp.Id);
			}
			
			setOppCurrency.add(SC.Initial_Opportunity__r.CurrencyIsoCode);
		}

		String soql = skedUtil.getCreatableFieldsSOQL('Opportunity');
		for (Opportunity opp : (List<Opportunity>)Database.query(soql + 'id IN: setOppIds')) {
			mapInitOpp.put(opp.Id, opp);
		}

		List<License_Entry__c> lstLicenses = new List<License_Entry__c>();
		Map<Id, List<OpportunityLineItem>> mapItem = new Map<Id, List<OpportunityLineItem>>();
		Map<Id, Id> mapPriceBookUS = new Map<Id, Id>();
		Map<Id, Id> mapPriceBookAU = new Map<Id, Id>();
		Map<Id, Set<String>> mapLicenseEntry = new Map<Id, Set<String>>();

		for(PriceBookEntry pBook: [SELECT Id, Product2Id, Product2.Id, Product2.Name, CurrencyIsoCode FROM PriceBookEntry 
    		WHERE PriceBook2.isStandard = true And CurrencyIsoCode = :setOppCurrency]){
    		if (pBook.CurrencyIsoCode == 'USD') mapPriceBookUS.put(pBook.Product2Id, pBook.id);
    		if (pBook.CurrencyIsoCode == 'AUD') mapPriceBookAU.put(pBook.Product2Id, pBook.id);
    	}

		for (OpportunityLineItem oppItem : [SELECT Id, Quantity, Product2Id, Product2.Name, UnitPrice, OpportunityId, Opportunity.Subscription_Order__c, 
												Opportunity.CurrencyIsoCode, Product_Family__c
											FROM OpportunityLineItem WHERE OpportunityId IN: setOppIds
											AND (Opportunity.RecordTypeId =: oppInitialRecordTypeId OR Opportunity.RecordTypeId =: oppAddOnRecordTypeId OR Opportunity.RecordTypeId =: oppRenewalRecordTypeId)
											AND (Product_Family__c = 'License' OR Product_Family__c = 'Premier Support')]) {
			OpportunityLineItem item = new OpportunityLineItem();
			item.Product2Id = oppItem.Product2Id;
			item.UnitPrice = oppItem.UnitPrice;
			item.Quantity = oppItem.Quantity;
			item.Product_Family__c = oppItem.Product_Family__c;

			if (oppItem.Opportunity.CurrencyIsoCode == 'USD') item.PricebookEntryId = mapPriceBookUS.get(oppItem.Product2Id);
			if (oppItem.Opportunity.CurrencyIsoCode == 'AUD') item.PricebookEntryId = mapPriceBookAU.get(oppItem.Product2Id);
			
			List<OpportunityLineItem> lst = new List<OpportunityLineItem>();
			if (mapItem.containsKey(oppItem.Opportunity.Subscription_Order__c)) {
				lst = mapItem.get(oppItem.Opportunity.Subscription_Order__c);
			}

			Boolean isMerge = false;

			for (OpportunityLineItem li : lst) {
				if(item.UnitPrice == li.UnitPrice && item.Product2Id == li.Product2Id) {
					li.Quantity += item.Quantity;
					isMerge = true;
					break;
				}
			}
			if (!isMerge) {
				lst.add(item);
				Set<String> productFamilies = new Set<String>();
				if (mapLicenseEntry.containsKey(oppItem.Opportunity.Subscription_Order__c)) {
					productFamilies = mapLicenseEntry.get(oppItem.Opportunity.Subscription_Order__c);
				}
				productFamilies.add(item.Product_Family__c);
				mapLicenseEntry.put(oppItem.Opportunity.Subscription_Order__c, productFamilies);
			}
			mapItem.put(oppItem.Opportunity.Subscription_Order__c, lst);
		}

		Map<Id, Opportunity> mapNewOpp = new Map<Id, Opportunity>();
		Map<Id, License_Entry__c> mapNewLicense = new Map<Id, License_Entry__c>();
		Map<Id, License_Entry__c> mapNewPremierSupport = new Map<Id, License_Entry__c>();

		for (Opportunity opp : [SELECT Id, Start_Date__c, AccountId FROM Opportunity WHERE AccountId =: mapAccountId.values() AND RecordTypeId =: oppRenewalRecordTypeId]) {
			for (Subscription_Contract__c SC : scope) {
				if (mapAccountId.containsKey(SC.Id) && mapAccountId.get(SC.Id) == opp.AccountId) {
					if (opp.Start_Date__c >= SC.Subscription_Contract_End__c && opp.Start_Date__c <= SC.Subscription_Contract_End__c.addDays(30)) {
						mapIgnored.put(SC.Id, null);
					}
				}
			}
		}
		System.debug('mapIgnore: ' + mapIgnored);

		for (Subscription_Contract__c SC : scope) {
			// Skip if There is already a renewal opportunity with start dates within 30 days after the contract end date
			if (mapIgnored.containsKey(SC.Id)) { continue; }
			if (!mapInitOpp.containsKey(SC.Initial_Opportunity__c)) { continue; }

			// Renewal Opportunity
			Opportunity newOpp = new Opportunity();
			newOpp = mapInitOpp.get(SC.Initial_Opportunity__c).clone(false, true);
			newOpp.Name += ' Renewal';
			newOpp.RecordTypeId = oppRenewalRecordTypeId;
			newOpp.Start_Date__c = SC.Subscription_Contract_End__c.addDays(1);
			newOpp.CloseDate = Date.today();
			newOpp.Subscription_Order__c = null;
			newOpp.Valid_Until__c = null;
			newOpp.StageName = 'Negotiate and Close';
			newOpp.Primary_Win_Loss_Reason__c = null;

			// split types License and Premier Support
			for (String productFamily : mapLicenseEntry.get(SC.Id)) {
				License_Entry__c le = new License_Entry__c();
				le.License_Entry_Start_Date__c = newOpp.Start_Date__c;
				le.License_Entry_End_Date__c = newOpp.Start_Date__c.addMonths(Integer.valueOf(newOpp.Contract_Length_Months__c)).addDays(-1);

				if (productFamily == 'License') {
					mapNewLicense.put(SC.Id, le);
				} else {
					mapNewPremierSupport.put(SC.Id, le);
				}
			}
			mapNewOpp.put(SC.Id, newOpp);
		}

		Savepoint sp;

		try {

			sp = Database.setSavepoint();

			if (!mapNewOpp.isEmpty()) {
				insert mapNewOpp.values();

				List<License_Entry__c> insertEntries = new List<License_Entry__c>();
				for (String scId : mapNewOpp.keySet()) {
					if (mapNewLicense.containsKey(scId)) {
						mapNewLicense.get(scId).Opportunity__c = mapNewOpp.get(scId).Id;
					}

					if (mapNewPremierSupport.containsKey(scId)) {
						mapNewPremierSupport.get(scId).Opportunity__c = mapNewOpp.get(scId).Id;
					}
				}
				if (!mapNewLicense.isEmpty()) {
					insert mapNewLicense.values();
				}
				if (!mapNewPremierSupport.isEmpty()) {
					insert mapNewPremierSupport.values();
				}

				if (!mapItem.isEmpty()) {
					List<OpportunityLineItem> lstNewItem = new List<OpportunityLineItem>();
					for (String scId : mapNewOpp.keySet()) {
						for (OpportunityLineItem item : mapItem.get(scId)) {
							if (item.Product_Family__c == 'License' && mapNewLicense.containsKey(scId)) {
								item.License_Entry__c = mapNewLicense.get(scId).Id;
							}
							if (item.Product_Family__c == 'Premier Support' && mapNewPremierSupport.containsKey(scId)) {
								item.License_Entry__c = mapNewPremierSupport.get(scId).Id;
							}
							item.OpportunityId = mapNewOpp.get(scId).Id;
						}
						lstNewItem.addAll(mapItem.get(scId));
					}

					insert lstNewItem;
				}
			}

		} catch (Exception ex) {
			Database.rollback(sp);
			System.debug(ex.getMessage() + ex.getLineNumber());
		}

	}
	
	global void finish(Database.BatchableContext context) {
		
	}
}