public with sharing class AccountTriggerHandler {
	private boolean m_isExecuting = false;
    
    /**
	Constructor
    */
	public AccountTriggerHandler(boolean isExecuting)
    {
        m_isExecuting = isExecuting;
    }
    
    /**
	check if trigger is running or not
    */
    public boolean isTriggerContext
    {
        get{ return m_isExecuting;}
    }

    /**
	Logic of after insert accounts
    */
    public void onAfterInsert(List<Account> lstNew) {
    	Map<String, Account> mapNewAccounts = new Map<String, Account>();
    	for (Account acc : lstNew) {
    		mapNewAccounts.put(acc.Id, acc);
    	}

    	updateStorageLinkProjectAndOpportunity(mapNewAccounts);
    }

    /**
    Logic of after update accounts
    */
    public void onAfterUpdate(List<Account> lstNew, List<Account> lstOld) {
    	Map<String, Account> mapNewAccounts = new Map<String, Account>();
    	mapNewAccounts = getUpdateStorageAccounts(lstNew, lstOld);
    	if (!mapNewAccounts.isEmpty()) {
    		updateStorageLinkProjectAndOpportunity(mapNewAccounts);
    	}
    }

    /**
	Get accounts which update Client_Google_Docs_Link__c field and Client_Dropbox_Link__c fields
    */
    private Map<String, Account> getUpdateStorageAccounts(List<Account> lstNew, List<Account> lstOld) {
    	Map<String, Account> mapUpdateAccounts = new Map<String, Account>();

    	for (Account acc : lstNew) {
    		mapUpdateAccounts.put(acc.Id, acc);
    	}

    	for (Account acc : lstOld) {
    		if (mapUpdateAccounts.containsKey(acc.Id)) {
    			Account newAcc = mapUpdateAccounts.get(acc.Id);
    			if ((acc.Client_Google_Docs_Link__c == newAcc.Client_Google_Docs_Link__c && 
    				acc.Client_Dropbox_Link__c == newAcc.Client_Dropbox_Link__c)) {
    				mapUpdateAccounts.remove(acc.Id);
    			}
    		}
    	}

    	return mapUpdateAccounts;
    }

    /**
	Update storage link for project and opportunity
    */
    private void updateStorageLinkProjectAndOpportunity(Map<String, Account> mapNewAccounts) {
		List<sObject> lstSObjects = new List<sObject>();

		for (Opportunity opp : [SELECT Id, Client_Google_Docs_Link__c, Client_Dropbox_Link__c, AccountID 
									FROM Opportunity WHERE AccountID IN : mapNewAccounts.keySet()]) {
			if (mapNewAccounts.containsKey(opp.AccountID)) {
				Account acc = mapNewAccounts.get(opp.AccountID);
				if (opp.Client_Google_Docs_Link__c != acc.Client_Google_Docs_Link__c) {
					opp.Client_Google_Docs_Link__c = acc.Client_Google_Docs_Link__c;
				}
				if (opp.Client_Dropbox_Link__c != acc.Client_Dropbox_Link__c) {
					opp.Client_Dropbox_Link__c = acc.Client_Dropbox_Link__c; 
				}
				lstSObjects.add(opp);
			}
		}

		for (Project__c prj : [SELECT Id, Dropbox_Link__c, Google_Drive_Link__c, Account__c 
									FROM Project__c WHERE Account__c IN : mapNewAccounts.keySet()]) {
			if (mapNewAccounts.containsKey(prj.Account__c)) {
				Account acc = mapNewAccounts.get(prj.Account__c);
				if (prj.Dropbox_Link__c != acc.Client_Dropbox_Link__c) {
					prj.Dropbox_Link__c = acc.Client_Dropbox_Link__c; 
				}
				if (prj.Google_Drive_Link__c != acc.Client_Google_Docs_Link__c) {
					prj.Google_Drive_Link__c = acc.Client_Google_Docs_Link__c;
				}
				lstSObjects.add(prj);
			}
		}

		if (!lstSObjects.isEmpty()) {
			update lstSObjects;
		}

	}
}