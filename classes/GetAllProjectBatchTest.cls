@isTest
private class GetAllProjectBatchTest
{
	static testmethod void doTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Project_Type__c prjType = new Project_Type__c();
		prjType.Jira_Name__c = 'business';
		prjType.SF_Value__c = 'Business';
		prjType.name = 'Business';

		insert prjType;

		test.starttest();
		GetAllProjectBatch myBatch = new GetAllProjectBatch();
		myBatch.execute(null);
		test.stoptest();
	}
}