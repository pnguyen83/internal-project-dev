global class skedInvoiceDueDateNotificationBatch implements Database.Batchable<SObject> {
	
	global skedInvoiceDueDateNotificationBatch() { }
	
	global Database.QueryLocator start(Database.BatchableContext context) {
		Datetime dt = Datetime.newInstance(system.today(), Time.newInstance(12, 0, 0, 0));
		dt = dt.addDays(1);
		string fdt = dt.format('yyyy-MM-dd');
		String strQuery = ''
		+ 'SELECT Id, Name, RecordType.Name, Region__c, Invoice_Notification_Recipient__c, Billing_Contact__c '
		+ 'FROM Invoice_Draft__c '
		+ 'WHERE (RecordType.Name = \'License\' OR RecordType.Name = \'Services\') AND (Region__c = \'US\' OR Region__c = \'AU\') '
		+ 'AND Invoice_Notification_Recipient__c != null '
		+ 'AND Invoice_Status__c = \'Draft\' AND Invoice_Date__c = ' + fdt;

		System.debug(strQuery);

		return Database.getQueryLocator(strQuery);
	}

   	global void execute(Database.BatchableContext context, List<Invoice_Draft__c> scope) {
		
		List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();

		OrgWideEmailAddress owa = [SELECT id, DisplayName, Address FROM OrgWideEmailAddress WHERE DisplayName = 'Skedulo Accounts' LIMIT 1];
		EmailTemplate template = [SELECT id, Subject, HtmlValue, Body FROM EmailTemplate WHERE Name = 'Invoice Notification' LIMIT 1];

		for (Invoice_Draft__c inv : scope) {
			Messaging.SingleEmailMessage m = getMailNotification(inv, template, owa.Id);
			mails.add(m);
		}
		Messaging.sendEmail(mails);

	}

	private Messaging.SingleEmailMessage getMailNotification(Invoice_Draft__c inv, EmailTemplate template, String fromAddress) {
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        mail.setToAddresses(new List<String> { inv.Invoice_Notification_Recipient__c });
		mail.saveAsActivity = false;
		mail.setOrgWideEmailAddressId(fromAddress);

		String recordlink = URL.getSalesforceBaseUrl().toExternalForm() + '/' + inv.Id;

		// process the merge fields
    	String subject = template.Subject;	
    	subject = subject.replace('{!Invoice_Draft__c.Name}', inv.Name);

    	String htmlBody = template.HtmlValue;
	    htmlBody = htmlBody.replace('###invoiceLink###', recordlink);
	    htmlBody = htmlBody.replace('{!Invoice_Draft__c.invLink__c}', inv.Name);

	    mail.setSubject(subject);
    	mail.setHtmlBody(htmlBody);

		System.debug('' + mail);

		return mail;
	}
	
	global void finish(Database.BatchableContext context) {
		
	}
}