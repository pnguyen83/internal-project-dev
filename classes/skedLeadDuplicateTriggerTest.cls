@isTest
public class skedLeadDuplicateTriggerTest{

    static testMethod void doTestForDupe(){
        list<lead> newLeads = new list<lead>();
        for(integer i=0;i<2;i++){
            lead l = new lead();
            l.lastname = 'Skedulo';
            l.email = 'Test@skedulo.com';
            l.company = 'Skedulo Account';
            newLeads.add(l);
        }
        insert newLeads;
        
        lead l = new lead();
        l.lastname = 'Skedulo';
        l.email = 'Test@skedulo.com';
        l.company = 'Skedulo Account';
        insert l;
        
        lead l2 = new lead();
        l2.lastname = 'Skedulo';
        l2.email = 'Test123@skedulo.com';
        l2.company = 'Skedulo Account';
        insert l2;
        
    }



}