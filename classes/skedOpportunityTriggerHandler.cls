public with sharing class skedOpportunityTriggerHandler {

    
    //When type on Opp equals "Add-On", prefill active contract, 1 Account is only has 1 Contract.
    /*public static void preFillSubscriptionContract(list<Opportunity> lstNew){
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(SkeduloConstants.OPP_RECORDTYPE_ADDON).getRecordTypeId();
        map<Id, Subscription_Contract__c> mapAccountContractId =  new map<Id, Subscription_Contract__c>();
        Set<ID> setAccount = new Set<Id>();
        //init setAccount.
        for(Opportunity opp: lstNew){
            if(opp.RecordTypeId == oppRecordTypeId)
                setAccount.add(opp.AccountId);
        }

        if(setAccount.isEmpty()) return;

        list<Subscription_Contract__c> contracts = [select id, Account__c, Contact__c, Payment_Terms__c, Payment_Method__c, Subscription_Contract_End__c, Purchase_Order_No__c
                                                    from Subscription_Contract__c 
                                                    where Account__c IN:setAccount AND Order_Status__c = 'Active'];
        for(Subscription_Contract__c CT: contracts){
            mapAccountContractId.put(CT.Account__c, CT);
        }

        for(Opportunity opp:lstNew){
            if(mapAccountContractId.containsKey(opp.AccountId) && opp.RecordTypeId == oppRecordTypeId){
                Subscription_Contract__c CT     = mapAccountContractId.get(opp.AccountId);
                opp.Subscription_Order__c       = CT.Id;
                opp.Primary_Contact__c          = CT.Contact__c;
                opp.Payment_Terms__c            = CT.Payment_Terms__c;
                opp.License_Payment_Method__c   = CT.Payment_Method__c;
                opp.End_Date__c                 = CT.Subscription_Contract_End__c;
                opp.Purchase_Order_No__c        = CT.Purchase_Order_No__c;
            }
        }

    }*/

    //Prefill End Month with Contract Term Months
    public static void preFillEndDateContract(list<Opportunity> lstNew, map<id,Opportunity> oldMap){
        Id oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(SkeduloConstants.OPP_RECORDTYPE_ADDON).getRecordTypeId();
        for(Opportunity opp: lstNew){
            if(opp.RecordTypeId == oppRecordTypeId) continue;

            if(oldMap == null){
                if(opp.Start_Date__c != null && opp.Contract_Length_Months__c != null){
                    opp.End_Date__c = opp.Start_Date__c.addMonths((integer)opp.Contract_Length_Months__c).addDays(-1);
                }
            }else{
                if(opp.Start_Date__c != null && opp.Contract_Length_Months__c != null && (opp.End_Date__c != oldMap.get(opp.id).End_Date__c || opp.Start_Date__c != oldMap.get(opp.id).Start_Date__c || opp.Contract_Length_Months__c != oldMap.get(opp.id).Contract_Length_Months__c)){
                    opp.End_Date__c = opp.Start_Date__c.addMonths((integer)opp.Contract_Length_Months__c).addDays(-1);
                }
            }
        }
    }

    public static void updatePurchaseNo(list<Opportunity> listNew, map<id,Opportunity> oldMap){
        Id oppInitialRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(SkeduloConstants.OPP_RECORDTYPE_INITIAL).getRecordTypeId();
        Id oppRenewalRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(SkeduloConstants.OPP_RECORDTYPE_RENEWAL).getRecordTypeId();
        list<Subscription_Contract__c> lstSub = new list<Subscription_Contract__c>();
        for(Opportunity opp: listNew){
            if(opp.Purchase_Order_No__c != oldMap.get(opp.id).Purchase_Order_No__c && opp.Subscription_Order__c != null 
                && (opp.RecordTypeId == oppInitialRecordTypeId || opp.RecordTypeId == oppRenewalRecordTypeId)){
                Subscription_Contract__c Sub = new Subscription_Contract__c(id=opp.Subscription_Order__c);
                Sub.Purchase_Order_No__c = opp.Purchase_Order_No__c;
                lstSub.add(Sub);
            }
        }

        if(!lstSub.IsEmpty()){
            update lstSub;
        }
    }

    //check Opportunity product before Closed Won
    public static void checkOpportunityLineBeforeCloseWon(Map<Id, Opportunity> mapNew, Map<Id, Opportunity> mapOld) {
        Map<Id, Opportunity> mapCheckOpps = new Map<Id, Opportunity>();

        for (Opportunity opp : mapOld.values()) {
            String oldStage = opp.StageName;
            if (mapNew.containsKey(opp.id)) {
                Opportunity newOpp = mapNew.get(opp.id);
                String newStage = newOpp.StageName;

                if (newStage == 'Closed Won' && oldStage != 'Closed Won') {
                    mapCheckOpps.put(newOpp.id, newOpp);
                }
            }
        }

        if (!mapCheckOpps.isEmpty()) {
            for (OpportunityLineItem item : [SELECT Id, Product2.Name, OpportunityId 
                                                FROM OpportunityLineItem 
                                                WHERE OpportunityId IN :mapCheckOpps.keySet()]) {
                if (item.Product2.Name == 'Skedulo Opp Forecast') {
                    if (mapCheckOpps.containsKey(item.OpportunityId)) {
                        Opportunity opp = mapCheckOpps.get(item.OpportunityId);
                        opp.addError(SkeduloConstants.NOT_CLOSE_WON_OPP_WITH_FORECAST_PRODUCT);
                    }
                }
            }
        }
    }
}