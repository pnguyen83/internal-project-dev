public with sharing class skedUpdateBillingDate_Temp {
	public static void updateBillingDate(Set<Id> contractIds) {
		try {
			List<Subscription_Contract__c> contracts = [SELECT Id, Payment_Terms__c, Subscription_Contract_Start__c, Subscription_Contract_End__c
													FROM Subscription_Contract__c WHERE Id =: contractIds];
			Map<Id, List<Order__c>> mapOrders = new Map<Id, List<Order__c>>();
			for (Order__c order : [select id, Subscription_Term__c, Subscription_Entry_Start_Date__c, Subscription_Entry_End_Date__c, Invoices_Processed__c, Order__c,
										(select id, Related_Product__c, Related_Product__r.Family, Related_Product__r.Name, Line_Item_Unit_Price__c, Line_Item_Total_ACV__c, Line_Item_Total__c, 
											Line_Item_Quantity__c, Line_Item_Name__c, Line_Item_Description__c 
										 from Sales_Order_Items__r)
	                                from Order__c 
	                                where Order__c =: contractIds
									order by Subscription_Entry_Start_Date__c asc]) {
				List<Order__c> orders = new List<Order__c>();
				if (mapOrders.containsKey(order.Order__c)) {
					orders = mapOrders.get(order.Order__c);
				}
				orders.add(order);
				mapOrders.put(order.Order__c, orders);
			}

			Map<Id, List<Invoice_Draft__c>> mapInvoices = new Map<Id, List<Invoice_Draft__c>>();
			for (Invoice_Draft__c invoice : [select id, CurrencyIsoCode, Invoice_Due_Date__c, Billing_Period_End_Date__c, Billing_Period_Start_Date__c, Subscription_Order__c,
												(select id, Product__c, Line_Item_Description__c, Line_Item_Name__c, Line_Item_Quantity__c, Line_Item_Total__c, Line_Item_Tax__c, Line_Item_Unit_Price__c 
												from Invoice_Line_Items__r) 
											from Invoice_Draft__c 
											where Subscription_Order__c =: contractIds and Invoice_Status__c in('Draft')
											and (Billing_Period_End_Date__c = null or Billing_Period_Start_Date__c = null)]) {
				List<Invoice_Draft__c> invoices = new List<Invoice_Draft__c>();
				if (mapInvoices.containsKey(invoice.Subscription_Order__c)) {
					invoices = mapInvoices.get(invoice.Subscription_Order__c);
				}
				invoices.add(invoice);
				mapInvoices.put(invoice.Subscription_Order__c, invoices);
			}

			Set<Id> updatedInvoiceIds = new Set<Id>();
			List<Invoice_Draft__c> updateInvoices = new List<Invoice_Draft__c>();
			for (Subscription_Contract__c SC : contracts) {

				if (!mapInvoices.containsKey(SC.id) || !mapOrders.containsKey(SC.id)) {
					continue;
				}

				Map<Date, String> date_map_product_map_price = new Map<Date, String> ();
				list<Order__c> orderNeedToCreateInvoices = mapOrders.get(SC.Id);
				processDatemapProductMapPrice(SC, date_map_product_map_price, orderNeedToCreateInvoices);
				map<Date, Invoice_Draft__c> map_date_invoice = processCurrentInvoices(mapInvoices.get(SC.id));

				system.debug('#map_date_invoice#' + map_date_invoice.keySet());
				List<Date> lstProductDate = new List<Date>();
	            lstProductDate.addAll(date_map_product_map_price.keyset());
	            lstProductDate.sort();

				System.debug('Contract Id: ' + SC.Id);
				System.debug('lstProductDate: ' + lstProductDate);

	            Date currentBillingEndDate = null; 
				integer numberOfMonths = getNumberOfMonths(SC.Payment_Terms__c);
				for (Date d : lstProductDate) {
					if (map_date_invoice.containsKey(d)) {
						Invoice_Draft__c invoice = map_date_invoice.get(d);
						invoice.Billing_Period_Start_Date__c = d;
						invoice.Billing_Period_End_Date__c = d.addMonths(numberOfMonths).addDays(-1);
						if (d <= currentBillingEndDate) {
							invoice.Billing_Period_End_Date__c = currentBillingEndDate;
						}
						currentBillingEndDate = invoice.Billing_Period_End_Date__c;

						updateInvoices.add(invoice);
						updatedInvoiceIds.add(invoice.Id);
					}

					if (currentBillingEndDate == null || d > currentBillingEndDate) {
						currentBillingEndDate = d.addMonths(numberOfMonths).addDays(-1);
					}

					system.debug('#currentBillingEndDate#' + d + ' - ' + currentBillingEndDate);
				}

				Set<Id> processedInvoiceIds = new Set<Id>();
				for (Date d : lstProductDate) {
					for (Invoice_Draft__c invoice : map_date_invoice.values()) {
						if (!updatedInvoiceIds.contains(invoice.Id)) {
							invoice.Billing_Period_Start_Date__c = d;
							invoice.Billing_Period_End_Date__c = d.addMonths(numberOfMonths).addDays(-1);
							updateInvoices.add(invoice);
							updatedInvoiceIds.add(invoice.Id);
						}
					}
				}
			}

			if (!updateInvoices.isEmpty()) {
				update updateInvoices;
			}
		} catch (Exception ex) {
			system.debug('#Error#' + ex.getMessage() + ex.getStackTraceString());
		}
	}

	private static void processDatemapProductMapPrice(Subscription_Contract__c SC, Map<Date, String> date_map_product_map_price, list<Order__c> orderNeedToCreateInvoices) {
		for (Order__c order : orderNeedToCreateInvoices) {
			Date contractDate = SC.Subscription_Contract_Start__c;
			Date oDate = order.Subscription_Entry_Start_Date__c;

			while (true) {
				if (contractDate < oDate) {
					contractDate = contractDate.addMonths(getNumberOfMonths(SC.Payment_Terms__c));
					continue;
				}
				date currentEnd = contractDate;

				if (oDate < currentEnd) {
					date_map_product_map_price.put(oDate, null);
					oDate = currentEnd;
				}
				contractDate = contractDate.addMonths(getNumberOfMonths(SC.Payment_Terms__c));

				if (contractDate> SC.Subscription_Contract_End__c.addDays(1))
				break;
			}
		}
	}

	private static integer getNumberOfMonths(string freQuency) {
		if (freQuency == 'Monthly') return 1;
		if (freQuency == 'Quarterly') return 3;
		if (freQuency == 'Bi-annual') return 6;
		return 12;
	}

	private static map<Date, Invoice_Draft__c> processCurrentInvoices(List<Invoice_Draft__c> invoices) {
		map<Date, Invoice_Draft__c> map_date_invoice = new map<date, Invoice_Draft__c> ();

		for (Invoice_Draft__c invoice : invoices) {
			map_date_invoice.put(invoice.Invoice_Due_Date__c, invoice);
		}

		return map_date_invoice;
	}
}