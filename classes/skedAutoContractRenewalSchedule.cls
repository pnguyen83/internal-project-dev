global class skedAutoContractRenewalSchedule implements Schedulable {
	global void execute(SchedulableContext sc) {
		skedAutoContractRenewalBatch b = new skedAutoContractRenewalBatch();
		database.executebatch(b, 100); 
	}
}