/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Subscription_ContractTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Subscription_ContractTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Subscription_Contract__c());
    }
}