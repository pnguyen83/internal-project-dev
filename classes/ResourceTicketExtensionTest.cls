@isTest
private class ResourceTicketExtensionTest
{
	static testmethod void doTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Project__c prj1 = (Project__c)mapData.get('project 1');
		 ApexPages.StandardController sc = new ApexPages.StandardController(prj1);
		 PageReference pageRef = Page.ResourceTicket;

		 test.starttest();
		 ResourceTicketExtension controller = new ResourceTicketExtension(sc);
		 controller.checkDeveloper();
		 controller.next();
		 controller.previous();
		 test.stoptest();
	}
}