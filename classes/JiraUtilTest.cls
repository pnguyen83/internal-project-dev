@isTest
private class JiraUtilTest
{
	static testmethod void createProjectTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Jira_Credential__c jiraLogin = new Jira_Credential__c();
		jiraLogin.Host__c = 'test host';
		jiraLogin.Username__c = 'test username';
		JiraLogin.Password__c = 'Test password';

		test.startTest();
		JiraUtil.createProject('PRJ001', 'prj name','description', 'type', 'contactName', jiraLogin);
		JiraUtil.updateJiraProject('PRJ001', 'prj name','description', 'type', 'contactName', jiraLogin);
		test.stopTest();
	}

	
}