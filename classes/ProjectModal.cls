global with sharing class ProjectModal {
    public String prjId {get;set;}
    public String recordTypeId {get;set;}
    public String name {get;set;}
    public String manager {get;set;}
    public String managerID {get;set;}
    public String region {get;set;}
    public String status {get;set;}
    public Decimal resourceAllocation {get;set;}
    public Decimal resourceNeccessary {get;set;}
    public String completion {get;set;}
    public Date startDate {get;set;}
    public Date uatDueDate {get;set;}
    public Date goLiveDate {get;set;}
    public Date actualScopeKickOff {get; set;}
    public Date actualUATKickOff {get; set;}
    public Date actualDevelopmentKickOff {get; set;}
    public Date actualDeployToProduction {get; set;}
    public Date actualGoLiveDate {get; set;}
    public Date actualHyperCaseKickOff {get; set;}
    public Date actualProjectCompletion {get; set;}

    public List<ChangeRequest> changeRequests {get;set;}

    public List<ResourceModal> resources {get;set;}
    public List<ResourceModal> potentialResources {get;set;}
    public List<ResourceRequirement> resourceRequirements {get;set;}
    
    public Id accID {get;set;}
    public Id oppId {get;set;}
    public String background {get;set;}
    public boolean have_a_trial {get;set;}
    public String trialType {get;set;}
    public boolean is_app_installed_into_their_sf {get;set;}
    public String userInfo {get;set;}
    public String detail_of_demo {get;set;}
    public String do_they_already_use_sf {get;set;}
    public String partnerConsultantInfo {get;set;}
    public String clientExpectation {get;set;}
    public String add_On_Components {get;set;}
    public String type {get;set;}

    public ProjectModal() {
        this.resources = new List<ResourceModal>();
        this.potentialResources = new List<ResourceModal>();
        this.resourceRequirements = new List<ResourceRequirement>();
        this.changeRequests = new List<ChangeRequest>();
    }

    public ProjectModal (Project_Resource__c prjR) {
        this.prjID = prjR.Project__c;
        this.name = prjR.Project__r.Name;
        this.manager = prjR.Project__r.Project_Manager_Name__c;
        this.region = prjR.Project__r.Project_Region__c;
        this.status = prjR.Project__r.Status__c;
        this.resourceAllocation = prjR.Project__r.Resource_Allocation__c != null ? prjR.Project__r.Resource_Allocation__c : 0;
        this.resourceNeccessary = prjR.Project__r.Num_of_Req_Resources__c != null ? Decimal.valueOf(prjR.Project__r.Num_of_Req_Resources__c) : 0;
        this.completion = prjR.Project__r.Complete__c;
        this.startDate = prjR.Project__r.Start__c;
        this.uatDueDate = prjR.Project__r.Actual_UAT_Kick_Off__c;
        this.goLiveDate = prjR.Project__r.Actual_Go_Live_Date__c;
        this.resources = new List<ResourceModal>();
        this.potentialResources = new List<ResourceModal>();
        this.resourceRequirements = new List<ResourceRequirement>();
        this.changeRequests = new List<ChangeRequest>();
        this.actualScopeKickOff = prjR.Project__r.Actual_Scope_Kick_Off__c;
        this.actualUATKickOff = prjR.Project__r.Actual_UAT_Kick_Off__c;
        this.actualDevelopmentKickOff = prjR.Project__r.Actual_Development_Kick_Off__c;
        this.actualDeployToProduction = prjR.Project__r.Actual_Deploy_to_Prod__c;
        this.actualGoLiveDate = prjR.Project__r.Actual_Go_Live_Date__c;
        this.actualHyperCaseKickOff = prjR.Project__r.Actual_Hypercare_Kick_Off__c;
        this.actualProjectCompletion = prjR.Project__r.Actual_Project_Completion__c;
    }

    public ProjectModal(String prjID, String name, String manager, String region, String status, Decimal allocation, String completion, Date startDate, Date uatDueDate, Date goLiveDate, String resourceNeccessary) {
        this.prjID = String.isNotBlank(prjID) ? prjID : '';
        this.name = String.isNotBlank(name) ? name : '';
        this.manager = String.isNotBlank(manager) ? manager : '';
        this.region = String.isNotBlank(region) ? region : '';
        this.status = String.isNotBlank(status) ? status : '';
        this.resourceAllocation = allocation != null ? allocation : 0;
        this.resourceNeccessary = resourceNeccessary != null ? Decimal.valueOf(resourceNeccessary) : 0;
        this.completion = String.isNotBlank(completion) ? completion : '';
        this.startDate = startDate;
        this.uatDueDate = uatDueDate;
        this.goLiveDate = goLiveDate;
        this.resources = new List<ResourceModal>();
        this.potentialResources = new List<ResourceModal>();
        this.resourceRequirements = new List<ResourceRequirement>();
        this.changeRequests = new List<ChangeRequest>();
    } 

    //===========================================================================================//
    /**
    Init ProjectModal from Opportunity
    */
    public void initFromOpportunity(Opportunity opp) {
        this.background = String.isNotBlank(opp.Background__c) ? opp.Background__c : '';
        this.have_a_trial = opp.Do_they_have_a_trial__c;
        this.trialType = String.isNotBlank(opp.Trial_Type__c) ? opp.Trial_Type__c : '';
        this.is_app_installed_into_their_sf = opp.Is_app_installed_into_their_SF_Instance__c == null ? false : opp.Is_app_installed_into_their_SF_Instance__c;
        this.userInfo = String.isNotBlank(opp.User_Info__c) ? opp.User_Info__c : '';
        this.detail_of_demo = String.isNotBlank(opp.Details_of_Demo__c) ? opp.Details_of_Demo__c : '';
        this.do_they_already_use_sf = String.isNotBlank(opp.Do_they_already_use_Salesforce__c) ? opp.Do_they_already_use_Salesforce__c : '';
        this.partnerConsultantInfo = String.isNotBlank(opp.Partner_Consultant_Info__c) ? opp.Partner_Consultant_Info__c : '';
        this.clientExpectation = String.isNotBlank(opp.Client_Expectations__c) ? opp.Client_Expectations__c : '';
        this.type = String.isNotBlank(opp.SF_Project_Type__c) ? opp.SF_Project_Type__c : '';
        this.add_On_Components = String.isNotBlank(opp.Add_On_Components__c) ? opp.Add_On_Components__c : '';
        this.oppId = opp.Id;
        this.changeRequests = new List<ChangeRequest>();
    }

    //===========================================================================================//
    /**
    Init ProjectModal from Project
    */
    public ProjectModal(Project__c prj) {
        this.prjId = prj.Id;
        this.name = prj.name;
        this.recordTypeId = prj.RecordTypeId;
        this.manager = String.isNotBlank(prj.Project_Manager_Name__c) ? prj.Project_Manager_Name__c : '';
        this.region = String.isNotBlank(prj.Project_Region__c) ? prj.Project_Region__c : '';
        this.status = String.isNotBlank(prj.Status__c) ? prj.Status__c : '';
        this.resourceAllocation = prj.Project_Resources__r != null ? prj.Project_Resources__r.size() : 0;
        this.completion = String.isNotBlank(prj.Complete__c) ? prj.Complete__c : '';
        this.startDate = prj.Start__c != null ? prj.Start__c : null;
        this.uatDueDate = prj.Actual_UAT_Kick_Off__c != null ? prj.Actual_UAT_Kick_Off__c : null;
        this.goLiveDate = prj.Actual_Go_Live_Date__c != null ? prj.Actual_Go_Live_Date__c : null;
        this.resourceNeccessary = prj.Num_of_Req_Resources__c != null ? Decimal.valueOf(prj.Num_of_Req_Resources__c) : 0;
        this.managerID = prj.Project_Manager__c == null ? '' : prj.Project_Manager__c;
        this.resources = new List<ResourceModal>();
        this.potentialResources = new List<ResourceModal>();
        this.changeRequests = new List<ChangeRequest>();
        this.actualScopeKickOff = prj.Actual_Scope_Kick_Off__c;
        this.actualUATKickOff = prj.Actual_UAT_Kick_Off__c;
        this.actualDevelopmentKickOff = prj.Actual_Development_Kick_Off__c;
        this.actualDeployToProduction = prj.Actual_Deploy_to_Prod__c;
        this.actualGoLiveDate = prj.Actual_Go_Live_Date__c;
        this.actualHyperCaseKickOff = prj.Actual_Hypercare_Kick_Off__c;
        this.actualProjectCompletion = prj.Actual_Project_Completion__c;

        if (prj.Project_Resources__r != null && !prj.Project_Resources__r.isEmpty()) {
            for (Project_Resource__c prjResource : prj.Project_Resources__r) {
                ResourceModal rModal = new ResourceModal(prjResource);
                this.resources.add(rModal);
            }
        }

        if (prj.Project_Change_Requests__r != null && !prj.Project_Change_Requests__r.isEmpty()) {
            for (Project_Change_Request__c pcr : prj.Project_Change_Requests__r) {
                ChangeRequest cr = new ChangeRequest();
                cr.id = pcr.id;
                cr.prjId = prj.id;
                cr.status = pcr.CR_Status__c;
                cr.completeDate = pcr.CR_Completion__c;
                cr.deployToProduct = pcr.Deploy_to_Prod__c;
                cr.developmentKickOff = pcr.Development_Kick_Off__c;
                cr.scopeKickOff = pcr.Scope_Kick_Off__c;
                cr.uatKickOff = pcr.UAT_Kick_Off__c;
                this.changeRequests.add(cr);
            }
        }
    }

    //=====================================================================================//
    /**
    get all project modal
    */
    public static List<ProjectModal> getAllProjectModals() {
        List<ProjectModal> lstProjectModals = new List<ProjectModal>();
        
        List<Project__c> lstProjects = [SELECT Id, Name, Project_Manager__c, Project_Manager_Name__c, Project_Region__c, Status__c, RecordTypeId,  
                                    Complete__c, UAT_Due_Date__c, End__c, Start__c, Num_of_Req_Resources__c,
                                    Actual_Deploy_to_Prod__c, Actual_Development_Kick_Off__c, Actual_Go_Live_Date__c,
                                    Actual_Hypercare_Kick_Off__c, Actual_Project_Completion__c, Actual_Scope_Kick_Off__c, Actual_UAT_Kick_Off__c,  
                                    (SELECT ID, Contact_Role__c, Contact_Role__r.name, Resource__c, Resource__r.Name, Project__c,Project__r.name, Project__r.RecordTypeId,
                                    Project__r.Project_Manager_Name__c, Project__r.Project_Region__c, Project__r.Status__c,Project__r.Complete__c,Project__r.Start__c, 
                                    Project__r.UAT_Due_Date__c, Project__r.Resource_Allocation__c, Project__r.End__c, Project__r.Num_of_Req_Resources__c, 
                                    Resource__r.Main_Category_Name__c, Project__r.Actual_Deploy_to_Prod__c, Project__r.Actual_Development_Kick_Off__c, Project__r.Actual_Go_Live_Date__c,
                                    Project__r.Actual_Hypercare_Kick_Off__c, Project__r.Actual_Project_Completion__c, Project__r.Actual_Scope_Kick_Off__c, Project__r.Actual_UAT_Kick_Off__c 
                                    FROM Project_Resources__r ORDER BY Index__c),
                                    (SELECT Id, Project__c, CR_Completion__c, CR_Status__c, Deploy_to_Prod__c, Development_Kick_Off__c, Scope_Kick_Off__c, UAT_Kick_Off__c
                                        FROM Project_Change_Requests__r WHERE CR_Status__c != 'Completed' AND CR_Status__c != 'Cancelled' ORDER BY UAT_Kick_Off__c)
                                FROM Project__c WHERE Status__c != 'Completed' AND Status__c != 'Cancelled' ORDER BY End__c, UAT_Due_Date__c];
        if (lstProjects != null && !lstProjects.isEmpty()) {
            for (Project__c prj : lstProjects) {
                ProjectModal prjModal = new ProjectModal(prj);
                lstProjectModals.add(prjModal);
            }
        }   

        //set vacation information for project resources
        setProjectResourceVacation(lstProjectModals);
        
        return lstProjectModals;
    }

    //=====================================================================================//
    /**
    Set vacation for project resources
    */
    public static void setProjectResourceVacation(List<ProjectModal> lstProjectModals) {
        Map<String, ResourceModal> mapResourceModal = new Map<String, ResourceModal>();
        Map<String, List<ResourceModal>> mapPrjResources = new Map<String, List<ResourceModal>>();
        Map<String, Map<String, List<ResourceModal.Vacation>>> mapPrjResourceVacation = new Map<String, Map<String, List<ResourceModal.Vacation>>>();
        List<String> lstPrjIDs = new List<String>();

        for (ProjectModal prj : lstProjectModals) {
            if (prj.resources != null) {
                List<ResourceModal> lstResources = prj.resources;
                if (lstResources != null) {
                    mapPrjResources.put(prj.prjId, lstResources);
                }
            }
        }

        mapPrjResourceVacation = ResourceModal.getVacationFromProjectID(new List<String>(mapPrjResources.keySet())); //get resource vacation for each project

        for (String prjID : mapPrjResources.keySet()) {
            if (mapPrjResourceVacation.containsKey(prjID)) {
                Map<String, List<ResourceModal.Vacation>> mapResourceVacations = mapPrjResourceVacation.get(prjID);
                List<ResourceModal> lstResources = mapPrjResources.get(prjID);
                for (ResourceModal resource : lstResources) {
                    String resourceID = resource.resourceID;
                    if (mapResourceVacations.containsKey(resourceID)) {
                        List<ResourceModal.Vacation> lstVacations = mapResourceVacations.get(resourceID);
                        resource.vacations = lstVacations;
                    }
                }
            }
        }
        
    }

    /**
    convert a list to a string that can use in soql
    */
    public static String convertListToString(List<String> lstToConvert) {
        String result = '(';    

        for (String s : lstToConvert) {
            result += '\''+ s + '\'' + ',';
        }

        result = result.removeEnd(',');
        result += ')';

        return result;
    }

    //=====================================================================================//
    /**
    Filter Project based on condition
    */
    public static List<ProjectModal> queryProjects(List<String> lstRegions, List<String> lstManagers, List<String> lstStatus, List<String> lstCategory) {
        List<ProjectModal> lstProjectModals = new List<ProjectModal>();
        List<ProjectModal> lstTemps = new List<ProjectModal>();

        String sql = 'SELECT Id, RecordTypeId, Name, Project_Manager__c, Project_Manager_Name__c, Project_Region__c, Status__c, Num_of_Req_Resources__c,';
        sql += 'Actual_Deploy_to_Prod__c, Actual_Development_Kick_Off__c, Actual_Go_Live_Date__c,';
        sql += 'Actual_Hypercare_Kick_Off__c, Actual_Project_Completion__c, Actual_Scope_Kick_Off__c, Actual_UAT_Kick_Off__c,';
        sql += 'Complete__c, UAT_Due_Date__c, End__c, Start__c, (SELECT ID, Contact_Role__c, Contact_Role__r.name, Resource__c,';
        sql += 'Resource__r.Name, Project__c,Project__r.name,Project__r.Project_Manager_Name__c, Project__r.Project_Region__c, Project__r.Status__c,';
        sql += 'Project__r.Actual_Deploy_to_Prod__c, Project__r.Actual_Development_Kick_Off__c,';
        sql += 'Project__r.Actual_Go_Live_Date__c, Project__r.Actual_Hypercare_Kick_Off__c,';
        sql += 'Project__r.Actual_Project_Completion__c, Project__r.Actual_Scope_Kick_Off__c,Project__r.Actual_UAT_Kick_Off__c,'; 
        sql += 'Project__r.Complete__c,Project__r.Start__c, Project__r.UAT_Due_Date__c, Project__r.Resource_Allocation__c, Project__r.End__c, Project__r.Num_of_Req_Resources__c, Resource__r.Main_Category_Name__c FROM Project_Resources__r ORDER BY Index__c),';
        sql += '(SELECT Id, Project__c, CR_Completion__c, CR_Status__c, Deploy_to_Prod__c, Development_Kick_Off__c, Scope_Kick_Off__c, UAT_Kick_Off__c ';
        sql += ' FROM Project_Change_Requests__r WHERE CR_Status__c != \'Completed\' AND CR_Status__c != \'Cancelled\' ORDER BY UAT_Kick_Off__c)';
        sql += ' FROM Project__c WHERE Status__c != \'Completed\' AND Status__c != \'Cancelled\' ';
        String filter = '';

        boolean hasRegion = lstRegions.isEmpty() ? false : true;
        boolean hasManager = lstManagers.isEmpty() ? false : true;
        boolean hasStatus = lstStatus.isEmpty() ? false : true;
        boolean hasCategory = lstCategory.isEmpty() ? false : true;
        boolean alreadyHasCondition = false;

        if (hasCategory || hasStatus || hasManager || hasRegion) {
            filter += ' AND ';
            if (hasRegion) {
                filter += ' Project_Region__c IN ' + convertListToString(lstRegions);
                alreadyHasCondition = true;
            }
            if (hasManager) {
                if (alreadyHasCondition) {
                    filter += ' AND Project_Manager_Name__c IN ' + convertListToString(lstManagers);
                } else {
                    filter += ' Project_Manager_Name__c IN ' + convertListToString(lstManagers);
                }
                alreadyHasCondition = true;
            }
            if (hasStatus) {
                if (alreadyHasCondition) {
                    filter += ' AND Status__c IN ' + convertListToString(lstStatus);
                } else {
                    filter += ' Status__c IN ' + convertListToString(lstStatus);
                }
                alreadyHasCondition = true;
            }
        }

        if (String.isNotBlank(filter)) {
            sql += filter;
        }

        //Sort projects by Go live date and UAT Date
        sql += ' ORDER BY Actual_Go_Live_Date__c, UAT_Due_Date__c';
        System.debug('sql = ' + sql);
        List<Project__c> lstPrjs = (List<Project__c>)Database.query(sql);

        for (Project__c prj : lstPrjs) {
            ProjectModal prjModal = new ProjectModal(prj);
            lstTemps.add(prjModal);
        }

        if (!lstTemps.isEmpty()) {
            if (hasCategory) {
                Set<String> setCategories = new Set<String>(lstCategory);
                for (ProjectModal prj : lstTemps) {
                    if (prj.resources != null) {
                        List<ResourceModal> lstResources = prj.resources;
                        for (ResourceModal resource : lstResources) {
                            if (setCategories.contains(resource.category)) {
                                lstProjectModals.add(prj);
                            }
                        }
                    }
                }
            } else {
                lstProjectModals = lstTemps;
            }
        }

        //set vacation for resources
        setProjectResourceVacation(lstProjectModals);

        //set primary project manager
        setPrimaryManger(lstProjectModals);

        return lstProjectModals;
    }

    //==========================================================================================//
    /**
    Set primary project manager
    */
    public static void setPrimaryManger(List<ProjectModal> lstPrjModals) {
        for (ProjectModal prjModal : lstPrjModals) {
                if (prjModal.resources != null) {
                    for (ResourceModal rModal : prjModal.resources) {
                        if (rModal.resourceID == prjModal.managerID) {
                            rModal.primaryProjectManager = true;
                        } else {
                            rModal.primaryProjectManager = false;
                        }
                    }
                }
            }
    }

    //==========================================================================================//
    /**
    Remove resource from project
    */
    public static void removeResource(String resourceID, String projectID) {
        if (String.isNotBlank(resourceID) && String.isNotBlank(projectID)) {
            List<Project_Resource__c> lstPrjResource = [SELECT Id FROM Project_Resource__c WHERE Resource__c = :resourceID AND Project__c = :projectID];
            if (lstPrjResource != null && !lstPrjResource.isEmpty()) {
                SavePoint sp = Database.setSavePoint();
                try {
                    delete lstPrjResource;
                } catch(Exception e) {
                    Database.rollback(sp);
                    throw new CustomException(e.getMessage());
                }

            }   
        } else {
            throw new CustomException('Missing Resource information!');
        }
    }

    //===========================================================================================//
    /**
    Get a Project Modal based on Project ID
    */
    public static List<ProjectModal> getProjectModalDetail(String prjID) {
        List<ProjectModal> lstPrjModals = new List<ProjectModal>();
        Set<String> setResourceIDs = new Set<String>();
        Set<String> setRequiredCategory = new Set<String>();
        Map<String, Set<String>> mapResReqTags = new Map<String, Set<String>>();
        Map<String, Decimal> mapRequiredNumber = new Map<String, Decimal>();

        if (String.isNotBlank(prjID)) {
            //Get list of tag for each kind of resource requirement
            for (Resource_Requirement__c resReq : [SELECT Id, Resource_Category_Name__c, Required_Number__c, (SELECT Id, Tag__c, Tag__r.name FROM Resource_Requirement_Tags__r) 
                                                    FROM Resource_Requirement__c WHERE Project__c = :prjID]) {
                //Get required number for each resource category
                mapRequiredNumber.put(resReq.Resource_Category_Name__c, resReq.Required_Number__c);
                //Get list of required tags for each resource
                Set<String> setTagIDs = mapResReqTags.get(resReq.Resource_Category_Name__c);
                if (setTagIDs == null) {
                    setTagIDs = new Set<String>();
                }

                for (Resource_Requirement_Tag__c resTag : resReq.Resource_Requirement_Tags__r) {
                    setTagIDs.add(resTag.Tag__c);
                }

                mapResReqTags.put(resReq.Resource_Category_Name__c, setTagIDs);
            }

            //Get resources that have been assigned to projects
            for (Project__c prj : [SELECT Id, RecordTypeId, Name, Project_Manager__c, Project_Manager_Name__c, Project_Region__c, Status__c,  
                                    Complete__c, UAT_Due_Date__c, End__c, Start__c, Num_of_Req_Resources__c, 
                                    Actual_Deploy_to_Prod__c, Actual_Development_Kick_Off__c, Actual_Go_Live_Date__c,
                                    Actual_Hypercare_Kick_Off__c, Actual_Project_Completion__c, Actual_Scope_Kick_Off__c, Actual_UAT_Kick_Off__c,
                                    (SELECT ID, Contact_Role__c, Contact_Role__r.name, Resource__c, Resource__r.Name, Project__c,Project__r.name,
                                            Project__r.Project_Manager_Name__c, Project__r.Project_Region__c, Project__r.Status__c,
                                            Project__r.Complete__c,Project__r.Start__c, Project__r.RecordTypeId,
                                            Project__r.Actual_Deploy_to_Prod__c, Project__r.Actual_Development_Kick_Off__c,
                                            Project__r.Actual_Go_Live_Date__c, Project__r.Actual_Hypercare_Kick_Off__c,
                                            Project__r.Actual_Project_Completion__c, Project__r.Actual_Scope_Kick_Off__c, 
                                            Project__r.Actual_UAT_Kick_Off__c,
                                            Project__r.UAT_Due_Date__c, Project__r.Resource_Allocation__c, Project__r.End__c, 
                                            Project__r.Num_of_Req_Resources__c, Resource__r.Main_Category_Name__c 
                                    FROM Project_Resources__r ORDER BY Index__c),
                                    (SELECT Id, Project__c, CR_Completion__c, CR_Status__c, Deploy_to_Prod__c, Development_Kick_Off__c, Scope_Kick_Off__c, UAT_Kick_Off__c
                                        FROM Project_Change_Requests__r WHERE CR_Status__c != 'Completed' AND CR_Status__c != 'Cancelled' ORDER BY UAT_Kick_Off__c)
                                    FROM Project__c WHERE Status__c != 'Cancelled' AND Status__c != 'Completed' AND Id = :prjID]) {
                if (prj != null) {
                    ProjectModal prjModal = new ProjectModal(prj);
                    lstPrjModals.add(prjModal);

                    //Get number of resource categories that are allocated
                    Map<String, Decimal> mapAllocatedNumber = new Map<String, Decimal>();
                    if (prj.Project_Resources__r != null) {
                        for (Project_Resource__c prjR : prj.Project_Resources__r) {
                            setResourceIDs.add(prjR.Resource__c);
                            Decimal quantity = mapAllocatedNumber.get(prjR.Contact_Role__r.name);
                            if (quantity == null) {
                                quantity = 0;
                            }
                            quantity += 1;
                            mapAllocatedNumber.put(prjR.Contact_Role__r.name, quantity);
                        }
                    }

                    //filter the categories that are already allocated full
                    for (String category : mapRequiredNumber.keySet()) {
                        Decimal tempAllocated = mapAllocatedNumber.get(category);
                        Decimal tempRequired = mapRequiredNumber.get(category);
                        if (tempAllocated == null || tempRequired != tempAllocated) {
                            setRequiredCategory.add(category);
                        }
                    }
                }
            }

            if (!lstPrjModals.isEmpty()) {
                ProjectModal prjModal = lstPrjModals.get(0);
                List<sked__Resource__c> lstResource = new List<sked__Resource__c>();
                if (!setResourceIDs.isEmpty()) {
                    lstResource = [SELECT Id, Name, Main_Category_Name__c, sked__Is_Active__c, (SELECT ID, sked__Tag__c FROM sked__ResourceTags__r) 
                                                            FROM sked__Resource__c WHERE NOT (ID IN :setResourceIDs) ORDER BY Main_Category_Name__c];
                } else {
                    lstResource = [SELECT Id, Name, Main_Category_Name__c, sked__Is_Active__c, (SELECT ID, sked__Tag__c FROM sked__ResourceTags__r) 
                                                            FROM sked__Resource__c ORDER BY Main_Category_Name__c];
                }
                //Get potential resources
                Map<String, ResourceModal> mapPotentialResourceModals = new Map<String, ResourceModal>();
                // if (!setResourceIDs.isEmpty()) {
                    for (sked__Resource__c resource : lstResource) {
                        if (resource.sked__Is_Active__c) { //only get the active resource
                        /** if (setRequiredCategory.contains(resource.Main_Category_Name__c)) { //filter the requirement resources that are not assigned enough resource
                                Set<String> setTagIDs = mapResReqTags.get(resource.Main_Category_Name__c); //get the required tag for each requirement resource
                                if (!setTagIDs.isEmpty()) {
                                    if (resource.sked__ResourceTags__r != null) {
                                        for (sked__Resource_Tag__c rTag : resource.sked__ResourceTags__r) {
                                            if (setTagIDs.contains(rTag.sked__Tag__c)) {
                                                insertResourceModalMapElement(mapPotentialResourceModals, resource);
                                            }
                                        }
                                    } else {
                                        insertResourceModalMapElement(mapPotentialResourceModals, resource);
                                    }
                                } else {
                                    insertResourceModalMapElement(mapPotentialResourceModals, resource);
                                }
                            } */
                        } 
                        insertResourceModalMapElement(mapPotentialResourceModals, resource);
                    }
                //}
                
                //get vacation for potential resources
                if (!mapPotentialResourceModals.isEmpty()) {
                    setResourceVacations(new List<String>(mapPotentialResourceModals.keySet()), prjModal.potentialResources, mapPotentialResourceModals);
                }
            }
            //set vacation for resources
            setProjectResourceVacation(lstPrjModals);

            //get resource requirement
            List<ResourceRequirement> lstResReq = new List<ResourceRequirement>();
            for (Resource_Requirement__c rr : [SELECT ID, Resource_Category_Name__c, Required_Number__c, Assigned__c, (SELECT Id, Tag__c, Tag__r.Name FROM Resource_Requirement_Tags__r) FROM Resource_Requirement__c WHERE Project__c = :prjID]) {
                if (rr.Assigned__c == null || rr.Required_Number__c > rr.Assigned__c) { //only get the resource requirement that does not have enough resource assigned
                    ResourceRequirement resReq = new ResourceRequirement();
                    resReq.category = rr.Resource_Category_Name__c;
                    resReq.noRequired = rr.Required_Number__c;
                    resReq.noAssigned = rr.Assigned__c;
                    if (rr.Resource_Requirement_Tags__r != null && !rr.Resource_Requirement_Tags__r.isEmpty()) {
                        for (Resource_Requirement_Tag__c rrT : rr.Resource_Requirement_Tags__r) {
                            ResourceRequirementTag resTag = new ResourceRequirementTag(rrT.Tag__r.Name, rrT.Id);
                            resReq.requiredTags.add(resTag);
                        }
                    }
                    lstResReq.add(resReq);
                }
            }

            if (!lstResReq.isEmpty()) {
                lstPrjModals.get(0).resourceRequirements = lstResReq;

                //set primary project manager
                setPrimaryManger(lstPrjModals);
                
            }
        }
        return lstPrjModals;
    }

    //===========================================================================================//
    /**
    set vacation for resources
    */
    private static void setResourceVacations(List<String> lstResourceIDs, List<ResourceModal> lstRsModal, Map<String, ResourceModal> mapResourceModals) {
        Map<String, List<ResourceModal.Vacation>> mapResourceVacation = ResourceModal.getVacations(new List<String>(mapResourceModals.keySet()));
                
        for (String resourceID : mapResourceModals.keySet()) {
            ResourceModal rModal = mapResourceModals.get(resourceID);
            if (mapResourceVacation.containsKey(resourceID)) {
                List<ResourceModal.Vacation> lstVacations = mapResourceVacation.get(resourceID);
                rModal.vacations =  lstVacations;
            }
            lstRsModal.add(rModal);
        }
    }

    //===========================================================================================//
    /**
    Insert element into a ResourceModal map
    */
    private static void insertResourceModalMapElement(Map<String, ResourceModal> mapPotentialResourceModals, sked__Resource__c resource) {
        if (!mapPotentialResourceModals.containsKey(resource.Id)) {
            ResourceModal rModal = new ResourceModal(resource.Id, resource.Name);
            rModal.category = resource.Main_Category_Name__c;
            mapPotentialResourceModals.put(resource.Id, rModal);
        }
    }

    //=========================================================================================//
    /**
    Resource Requirement
    */
    public class ResourceRequirement {
        public String category {get;set;}
        public List<String> tags {get;set;} //sked__Tag__c Id
        public Decimal noRequired {get;set;}
        public Decimal noAssigned {get;set;}
        public List<ResourceRequirementTag> requiredTags {get;set;}
        public ResourceRequirement(String category, List<String> lstTags, Decimal requiredNo) {
            this.category = category;
            this.tags = lstTags;
            this.noRequired = requiredNo;
            requiredTags = new List<ResourceRequirementTag>();
        }

        public ResourceRequirement() {
            requiredTags = new List<ResourceRequirementTag>();
        }
    }

    //=========================================================================================//
    /**
    Required Tag for each type of resouce
    */
    public class ResourceRequirementTag {
        public String tagName {get;set;}
        public String tagId {get;set;}

        public ResourceRequirementTag(String name, String tagId) {
            this.tagName = name;
            this.tagId = tagId;
        }
    }

    /**
    Change Request Modal
    */
    public class ChangeRequest {
        public String id {get;set;}
        public String prjId {get;set;}
        public String status {get;set;}
        public Date completeDate {get;set;}
        public Date deployToProduct {get;set;}
        public Date developmentKickOff {get; set;}
        public Date scopeKickOff {get;set;}
        public Date uatKickOff {get; set;}
    }
}