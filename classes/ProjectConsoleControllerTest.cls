@isTest
private class ProjectConsoleControllerTest
{
	static testmethod void getAllTagCategoriesTest() {
		Map<String, sObject> mapData = TestUtilities.createData();

		test.starttest();
		ProjectConsoleController.getAllTagCategories();
		test.stoptest(); 
	}

	static testmethod void getAllProjectsTest() {
		Map<String, sObject> mapData = TestUtilities.createData();

		test.starttest();
		ProjectConsoleController.getAllProjects();
		test.stoptest();
	}

	static testmethod void getAllProjectManagerTest() {
		Map<String, sObject> mapData = TestUtilities.createData();

		test.starttest();
		ProjectConsoleController.getAllProjectManager();
		test.stoptest();
	}

	static testmethod void getAllProjectStatusTest() {
		Map<String, sObject> mapData = TestUtilities.createData();


		test.starttest();
		ProjectConsoleController.getAllProjectStatus();
		test.stoptest();
	}

	static testmethod void getAllRegionTest() {
		Map<String, sObject> mapData = TestUtilities.createData();

		test.starttest();
		ProjectConsoleController.getAllRegion();
		test.stoptest();
	}	

	static testmethod void filterProjectsTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		sked__region__c region1 = (sked__region__c)mapData.get('region 1');
		sked__resource__c resource1 = (sked__resource__c)mapData.get('resource 1');
		Project__c prj1 = (Project__c)mapData.get('project 1');
		Contact_Role__c category1 = (Contact_Role__c)mapData.get('category 1');

		test.starttest();
		ProjectConsoleController.filterProjects(region1.name, resource1.name, prj1.Status__c, category1.name,'');
		test.stoptest();
	}

	static testmethod void removeResourceTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		sked__resource__c resource1 = (sked__resource__c)mapData.get('resource 1');
		Project__c prj1 = (Project__c)mapData.get('project 2');

		test.starttest();
		ProjectConsoleController.removeResource(resource1.Id, prj1.Id);
		test.stoptest();
	}

	static testmethod void getAllResourcesTest() {
		Map<String, sObject> mapData = TestUtilities.createData();

		test.starttest();
		ProjectConsoleController.getAllResources();
		test.stoptest();	
	}

	static testmethod void filterResourcesTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Contact_Role__c category1 = (Contact_Role__c)mapData.get('category 1');

		test.starttest();
		ProjectConsoleController.filterResources(category1.name);
		test.stoptest();
	}

	static testmethod void getProjectModalDetailTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Project__c prj1 = (Project__c)mapData.get('project 1');

		test.starttest();
		ProjectConsoleController.getProjectModalDetail(prj1.Id);
		test.stoptest();
	}

	static testmethod void getAllResourcesOfProjectTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Project__c prj1 = (Project__c)mapData.get('project 1');

		test.starttest();
		ProjectConsoleController.getAllResourcesOfProject(prj1.Id);
		test.stoptest();
	}

	static testmethod void searchProjectResourceTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Project__c prj1 = (Project__c)mapData.get('project 1');
		sked__resource__c res1 = (sked__resource__c)mapData.get('resource 1');
		Set<Id> setSearchResult = new Set<Id> {prj1.Id, res1.Id};
		Test.setFixedSearchResults(new List<Id>(setSearchResult));

		test.starttest();
		ProjectConsoleController.searchProjectResource('test');
		test.stoptest();
	}

	static testmethod void getOneResourceTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		sked__resource__c resource1 = (sked__resource__c)mapData.get('resource 1');

		test.starttest();
		ProjectConsoleController.getOneResource(resource1.Id);
		test.stoptest();
	}

	static testmethod void assignResourceToProjectTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Project__c prj1 = (Project__c)mapData.get('project 3');
		sked__resource__c resource1 = (sked__resource__c)mapData.get('resource 1');
		Contact_Role__c category = (Contact_Role__c)mapData.get('category 1');
		String startTime = String.valueOf(Date.today());
		String endTime = String.valueOf(Date.today().addDays(10));

		test.starttest();
		try {
				ProjectConsoleController.assignResourceToProject(prj1.Id, resource1.Id, category.name, startTime, endTime);
			} catch(Exception ex) {

			}
		
		test.stoptest();
	}

	static testmethod void filterPotentialResourcesTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Project__c prj1 = (Project__c)mapData.get('project 3');
		Contact_Role__c category = (Contact_Role__c)mapData.get('category 2');


		test.starttest();
		ProjectConsoleController.filterPotentialResources(prj1.Id, category.name);
		test.stoptest();
	}

	static testmethod void testProjectModal() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Opportunity opp1 = (Opportunity)mapData.get('opportunity 1');

		test.starttest();
		ProjectModal prjModal = new ProjectModal();
		prjModal.initFromOpportunity(opp1);
		test.stoptest();

	}

	static testmethod void testResourceModal() {
		Map<String, sObject> mapData = TestUtilities.createData();
		sked__resource__c resource1 = (sked__resource__c)mapData.get('resource 1');
		sked__resource__c resource2 = (sked__resource__c)mapData.get('resource 2');
		

		test.starttest();
		ResourceModal rsModal1 = new ResourceModal(resource1.Id, resource1.name);
		ResourceModal rsModal2 = new ResourceModal(resource2);
		List<ResourceModal> lstResource = new List<ResourceModal>{rsModal1, rsModal2};
		ResourceModal.Vacation vacation = new ResourceModal.Vacation(System.now(), System.now().addHours(1),'test vacation');
		ResourceModal.AssignedProject assignedPrj = new ResourceModal.AssignedProject(Date.today(), 2);
		ResourceModal.getAssignedProjects(lstResource, Date.today().addDays(-1), Date.today().addDays(1));
		test.stoptest();
	}
}