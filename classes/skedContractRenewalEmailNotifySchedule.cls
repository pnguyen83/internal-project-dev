global class skedContractRenewalEmailNotifySchedule implements Schedulable {
    global void execute(SchedulableContext sc) {
        skedContractRenewalEmailNotifyBatch b = new skedContractRenewalEmailNotifyBatch();
        database.executebatch(b, 100);
    }
}