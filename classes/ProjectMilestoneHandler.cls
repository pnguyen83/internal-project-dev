public class ProjectMilestoneHandler {
	private boolean m_isExecuting = false;

	public boolean IsTriggerContext
    {
        get{ return m_isExecuting;}
    }
    
    public ProjectMilestoneHandler(boolean isExecuting)
    {
        m_isExecuting = isExecuting;
    }

	public void onAfterUpdate(List<Project_Milestone__c> lstNews, Map<Id, Project_Milestone__c> mapOlds) {
		updateProjectTimeLine(lstNews, mapOlds);
	}

	public void updateProjectTimeLine(List<Project_Milestone__c> lstNews, Map<Id, Project_Milestone__c> mapOlds) {
		Map<String, Project__c> mapProjects = new Map<String, Project__c>();

		for (Project_Milestone__c milestone : lstNews) {
			String name = milestone.name;
			Project__c prj;

			if (mapProjects == null || !mapProjects.containsKey(milestone.Project__c)) {
				prj	= new Project__c(id = milestone.Project__c); 
			}
			else {
				prj = mapProjects.get(milestone.Project__c);
			}
			 
			Project_Milestone__c oldMilestone = mapOlds.get(milestone.id);

			if (oldMilestone.Actual_Date__c != milestone.Actual_Date__c && name == SkeduloConstants.MILESTONE_SCOPE_KICK_OFF) {
				prj.Actual_Scope_Kick_Off__c = milestone.Actual_Date__c;
			}

			if (oldMilestone.Actual_Date__c != milestone.Actual_Date__c && name == SkeduloConstants.MILESTONE_DEVELOPEMENT_KICK_OFF) {
				prj.Actual_Development_Kick_Off__c = milestone.Actual_Date__c;
			}

			if (oldMilestone.Actual_Date__c != milestone.Actual_Date__c && name == SkeduloConstants.MILESTONE_UAT_KICK_OFF) {
				prj.Actual_UAT_Kick_Off__c = milestone.Actual_Date__c;
			}

			if (oldMilestone.Actual_Date__c != milestone.Actual_Date__c && name == SkeduloConstants.MILESTONE_DEPLOY_TO_PRODUCTION) {
				prj.Actual_Deploy_to_Prod__c = milestone.Actual_Date__c;
			}

			if (oldMilestone.Actual_Date__c != milestone.Actual_Date__c && name == SkeduloConstants.MILESTONE_GO_LIVE_DATE) {
				prj.Actual_Go_Live_Date__c = milestone.Actual_Date__c;
			}

			if (oldMilestone.Actual_Date__c != milestone.Actual_Date__c && name == SkeduloConstants.MILESTONE_HYPERCARE_KICK_OFF) {
				prj.Actual_Hypercare_Kick_Off__c = milestone.Actual_Date__c;
			}

			if (oldMilestone.Actual_Date__c != milestone.Actual_Date__c && name == SkeduloConstants.MILESTONE_PROJECT_COMPLETION) {
				prj.Actual_Project_Completion__c = milestone.Actual_Date__c;
			}

			mapProjects.put(prj.id, prj);
		}

		update mapProjects.values();
	}
}