@isTest
public class TestUtilities
{
	public static Map<String, sObject> createData() {
		Map<String, sObject> mapData = new Map<String, sObject>();
		List<sObject> lstObject1 = new List<sObject>();
		List<sObject> lstObject2 = new List<sObject>();
		List<sObject> lstObject3 = new List<sObject>();
		List<sObject> lstObject4 = new List<sObject>();
		List<sObject> lstObject5 = new List<sObject>();

		//=================================================Group 1=============================================//
		//create account
		Account acc1 = new Account(
				name = 'test account',
				Billing_Entity__c = 'Skedulo',
				Client_Google_Docs_Link__c = 'test.com',
				Client_Dropbox_Link__c = 'testlink.com'
			);
		mapData.put('account 1', acc1);
		lstObject1.add(acc1);

		//Create resource category (Contact_Role__c)
		Contact_Role__c category1 = new Contact_Role__c(
				name = 'Project Manager',
				Role_Key__c = 'PM'
			);
		mapData.put('category 1', category1);
		lstObject1.add(category1);

		Contact_Role__c category2 = new Contact_Role__c(
				name = 'category 2',
				Role_Key__c = 'DEV'
			);
		mapData.put('category 2', category2);
		lstObject1.add(category2);

		//Create tag
		sked__Tag__c tag1 = new sked__Tag__c(
				name = 'tag 1'
			);
		mapData.put('tag 1', tag1);
		lstObject1.add(tag1);

		sked__Tag__c tag2 = new sked__Tag__c(
				name = 'tag 2'
			);
		mapData.put('tag 2', tag2);
		lstObject1.add(tag2);

		//Create Region
		sked__Region__c region1 = new sked__Region__c(
				name = 'region 1',
				sked__Timezone__c = 'Asia/Ho_Chi_Minh'
			);
		mapData.put('region 1', region1);
		lstObject1.add(region1);

		sked__Region__c region2 = new sked__Region__c(
				name = 'region 2',
				sked__Timezone__c = 'Australia/Brisbane'
			);
		mapData.put('region 2', region2);
		lstObject1.add(region2);

		//Create Holiday
		sked__Holiday__c holiday1 = new sked__Holiday__c(
				name = 'test holiday',
				sked__End_Date__c = Date.today().addDays(1),
				sked__Start_Date__c = Date.today().addDays(1),
				sked__Global__c = true
			);
		mapData.put('holiday 1', holiday1);
		lstObject1.add(holiday1);

		//Create Shift Type Setting
		sked_Shift_Type_Setting__c typeSetting = new sked_Shift_Type_Setting__c(
				name = 'Annual Leave Setting 1',
				Available_Start__c = 0,
				Color__c = '#D8D8D8',
				Default_Start__c = 0,
				Step__c = 60,
				Available_End__c = 2.400,
				Default_End__c = 2.400,
				Shift_Type__c = 'Annual Leave'
			);
		mapData.put('shift type setting 1', typeSetting);
		lstObject1.add(typeSetting);

		sked_Shift_Type_Setting__c typeSetting2 = new sked_Shift_Type_Setting__c(
				name = 'Available Setting 1',
				Available_Start__c = 0,
				Color__c = '#EBFFC6',
				Default_Start__c = 0,
				Step__c = 30,
				Available_End__c = 2.400,
				Default_End__c = 2.400,
				Shift_Type__c = 'Available',
				Is_Available__c = true
			);
		mapData.put('shift type setting 2', typeSetting2);
		lstObject1.add(typeSetting2);

		sked_Shift_Type_Setting__c typeSetting3 = new sked_Shift_Type_Setting__c(
				name = 'Sick Leave Setting 1',
				Available_Start__c = 0,
				Color__c = '#D8D8D8',
				Default_Start__c = 0,
				Step__c = 30,
				Available_End__c = 2.400,
				Default_End__c = 2.400,
				Shift_Type__c = 'Sick Leave',
				Can_Be_Pre_Dated__c = true
			);
		mapData.put('shift type setting 3', typeSetting3);
		lstObject1.add(typeSetting3);

		//Create sked_Admin_Settings__c
		sked_Admin_Settings__c adminSeting = new sked_Admin_Settings__c(
				Replicate_Max_Weeks__c = 20,
				End_Working_Time__c = 2.400,
				Number_of_Jobs_to_alert__c = 32,
				Start_Working_Time__c = 0
			);
		mapData.put('admin setting 3', adminSeting);
		lstObject1.add(adminSeting);

		insert lstObject1;

		//=================================================Group 2============================================//
		//Create opportunity
		Opportunity opp1 = new Opportunity(
				name = 'test opportunity',
				accountid = acc1.Id,
				CloseDate = Date.today().addDays(1),
				StageName = 'Prospecting',
				ForecastCategoryName = 'Active Pipeline',
				Type = 'New Business',
				Add_On_Components__c = 'Booking Grid'
			);
		mapData.put('opportunity 1', opp1);
		lstObject2.add(opp1);

		Opportunity opp2 = new Opportunity(
				name = 'test opportunity 2',
				accountid = acc1.Id,
				CloseDate = Date.today().addDays(2),
				StageName = 'Prospecting',
				ForecastCategoryName = 'Active Pipeline',
				Type = 'New Business',
				Add_On_Components__c = 'Booking Grid'
			);
		mapData.put('opportunity 2', opp2);
		lstObject2.add(opp2);

		Opportunity opp3 = new Opportunity(
				name = 'test opportunity 2',
				accountid = acc1.Id,
				CloseDate = Date.today().addDays(2),
				StageName = 'Prospecting',
				ForecastCategoryName = 'Active Pipeline',
				Type = 'New Business',
				Add_On_Components__c = 'Booking Grid'
			);
		mapData.put('opportunity 3', opp3);
		lstObject2.add(opp3);

		//Create resource
		sked__Resource__c resource1 = new sked__Resource__c(
				name = 'test resource 1',
				sked__Resource_Type__c = 'Person',
				Main_Category__c = category1.Id, //Project Manager
				sked__Alias__c = 'admin',
				sked__Weekly_Hours__c = 40
			);
		mapData.put('resource 1', resource1);
		lstObject2.add(resource1);

		sked__Resource__c resource2 = new sked__Resource__c(
				name = 'test resource 2',
				sked__Resource_Type__c = 'Person',
				Main_Category__c = category2.Id, 
				sked__Alias__c = 'resource2',
				sked__Weekly_Hours__c = 40
			);
		mapData.put('resource 2', resource2);
		lstObject2.add(resource2);

		//Create Region Holiday
		sked__Holiday_Region__c regionHld1 = new sked__Holiday_Region__c(
				sked__Holiday__c = holiday1.Id,
				sked__Region__c = region1.Id
			);
		mapData.put('region holiday 1', regionHld1);
		lstObject2.add(regionHld1);

		sked__Holiday_Region__c regionHld2 = new sked__Holiday_Region__c(
				sked__Holiday__c = holiday1.Id,
				sked__Region__c = region2.Id
			);
		mapData.put('region holiday 2', regionHld2);
		lstObject2.add(regionHld2);

		insert lstObject2;

		//================================================Group 3=============================================//
		//Create Project
		Project__c prj1 = new Project__c(
				name = 'test project 1',
				SF_Project_Type__c = 'Internal Project',
				Project_Manager__c = resource1.Id,
				Account__c = acc1.Id,
				Opportunity__c = opp2.Id,
				Status__c = 'In Development',
				Start__c = Date.today().addDays(1),
				UAT_Due_Date__c = Date.today().addDays(30),
				End__c = Date.today().addDays(40),
				Num_of_Req_Resources__c = '6'
			);
		mapData.put('project 1', prj1);
		lstObject3.add(prj1);

		Project__c prj2 = new Project__c(
				name = 'test project 2',
				SF_Project_Type__c = 'Internal Project',
				Project_Manager__c = resource1.Id,
				Account__c = acc1.Id,
				Opportunity__c = opp3.Id,
				Status__c = 'In Development',
				Start__c = Date.today().addDays(1),
				UAT_Due_Date__c = Date.today().addDays(30),
				End__c = Date.today().addDays(40),
				Num_of_Req_Resources__c = '6'
			);
		mapData.put('project 2', prj2);
		lstObject3.add(prj2);

		Project__c prj3 = new Project__c(
				name = 'test project 3',
				SF_Project_Type__c = 'Internal Project',
				Project_Manager__c = resource2.Id,
				Account__c = acc1.Id,
				Opportunity__c = opp1.Id,
				Status__c = 'In Development',
				Start__c = Date.today().addDays(1),
				UAT_Due_Date__c = Date.today().addDays(30),
				End__c = Date.today().addDays(40),
				Num_of_Req_Resources__c = '6',
				Project_Key__c = 'PRJ003'
			);
		mapData.put('project 3', prj3);
		lstObject3.add(prj3);

		insert lstObject3;

		//================================================Group 4============================================//
		//Create Resource requirement
		Resource_Requirement__c resReq1 = new Resource_Requirement__c(
				Project__c = prj1.Id,
				Resource_Category__c = category1.Id,
				Required_Number__c = 1,
				Assigned__c = 1
			);
		mapData.put('resource requirement 1', resReq1);
		lstObject4.add(resReq1);

		Resource_Requirement__c resReq2 = new Resource_Requirement__c(
				Project__c = prj1.Id,
				Resource_Category__c = category2.Id,
				Required_Number__c = 2,
				Assigned__c = 1
			);
		mapData.put('resource requirement 2', resReq2);
		lstObject4.add(resReq2);

		//Create project resource
		Project_Resource__c prjRes1 = new Project_Resource__c(
				Project__c = prj1.Id,
				Resource__c = resource1.Id,
				Contact_Role__c = category1.Id
			);
		mapData.put('project resource 1', prjRes1);
		lstObject4.add(prjRes1);

		//Create Jira ticket
		Jira_Ticket__c jira1 = new Jira_Ticket__c(
				Description__c = 'jira ticket 1 description',
				Jira_Issue_Key__c = 'JIRA1',
				Priority__c = 'Medium',
				Project__c = prj1.Id,
				Resource__c = resource1.Id,
				Status__c = 'Ready for Test',
				Subject__c = 'this is test jira ticket',
				Ticket_Type__c = 'Task'
			);
		mapData.put('jira ticket 1', jira1);
		lstObject4.add(jira1);

		//Create availability
		sked__Availability__c avai1 = new sked__Availability__c(
				sked__Start__c = system.now().addDays(2),
				sked__Finish__c = System.now().addDays(4),
				sked__Resource__c = resource1.Id,
				sked__Timezone__c = 'Australia/Brisbane',
				sked__Type__c = 'Sick Leave'
			);
		mapData.put('availability 1', avai1);
		lstObject4.add(avai1);

		sked__Availability__c avai2 = new sked__Availability__c(
				sked__Start__c = system.now().addDays(2),
				sked__Finish__c = System.now().addDays(4),
				sked__Resource__c = resource2.Id,
				sked__Timezone__c = 'Asia/Ho_Chi_Minh',
				sked__Type__c = 'Annual Leave'
			);
		mapData.put('availability 2', avai2);
		lstObject4.add(avai2);

		

		insert lstObject4;

		//=================================================Group 5==========================================//
		//Create resource requirement tag
		Resource_Requirement_Tag__c resReqTag1 = new Resource_Requirement_Tag__c(
				Resource_Requirement__c = resReq1.Id,
				Tag__c = tag1.Id
			);
		mapData.put('resource requirement tag 1', resReqTag1);
		lstObject5.add(resReqTag1);


		Resource_Requirement_Tag__c resReqTag2 = new Resource_Requirement_Tag__c(
				Resource_Requirement__c = resReq2.Id,
				Tag__c = tag2.Id
			);
		mapData.put('resource requirement tag 2', resReqTag2);
		lstObject5.add(resReqTag2);
		
		insert lstObject5;

		return mapData;
	}
}