global class skedCreateBillingSubProcessExt {

    webservice static String createContractForClosedWonOpp(String oppId) { 
        
        Savepoint sp;

        try {

            sp = Database.setSavepoint();
			Id oppAddonRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(SkeduloConstants.OPP_RECORDTYPE_ADDON).getRecordTypeId();

            Opportunity opp = [SELECT Id, StageName, RecordTypeId, Subscription_Order__c, Purchase_Order_No__c, AccountId, Currency__c, Primary_Contact__c, 
                               BillingContactId__c, Start_Date__c, End_Date__c, Payment_Terms__c, License_Payment_Method__c, CurrencyIsoCode, Billing_Subscription_Created__c,
                               (SELECT Id, ContactId, Role, IsPrimary FROM OpportunityContactRoles) 
                               FROM Opportunity WHERE Id = : oppID];
                           
            if (opp == null || opp.StageName != skedUtils.CLOSED_WON) { 
                return 'Hey! You are trying to generate a contract/Order when the Opportunity has not yet closed-won. The Opportunity must be in a closed-won stage before selecting this.';
            }

            if (opp.Billing_Subscription_Created__c) return 'Billing Subscription has already been created';
            
            if (opp.RecordTypeId == oppAddonRecordTypeId) opp.Billing_Subscription_Created__c = true;

			Boolean isActive = false;

			for (Account acc : [SELECT Id, (SELECT Id, Order_Status__c, Subscription_Contract_Start__c, Subscription_Contract_End__c FROM Orders__r WHERE Order_Status__c = 'Active' LIMIT 1) 
                               FROM Account WHERE Id =: opp.AccountId]) {
                if (acc.Orders__r.isEmpty()) isActive = true;
            }

            if (String.isBlank(opp.Subscription_Order__c)) {
                String conId = getActiveContract(opp, isActive);

                if (conId == null) return 'Hey! There is currently no contract record created where this Add On Opportunity start and end dates fall within. Please check if there is a renewal opportunity created for this account, if yes, you will need to generate the contract before clicking "Create Billing Subscription" on this Add On to create an order. Thanks!';

                if (String.isNotBlank(conId)) {

					Subscription_Contract__c cont = [SELECT Id, Contact__c, Payment_Terms__c, Payment_Method__c, Subscription_Contract_End__c, Purchase_Order_No__c
													 FROM Subscription_Contract__c WHERE Id =: conId LIMIT 1];
                    opp.Subscription_Order__c		= conId;
					opp.Primary_Contact__c          = cont.Contact__c;
					opp.Payment_Terms__c            = cont.Payment_Terms__c;
					opp.License_Payment_Method__c   = cont.Payment_Method__c;
					opp.End_Date__c                 = cont.Subscription_Contract_End__c;
					opp.Purchase_Order_No__c        = cont.Purchase_Order_No__c;
                } else return '';
            }

            List<License_Entry__c> lstLE = new List<License_Entry__c> ();
            for (License_Entry__c LIE :[SELECT id, Opportunity__r.CurrencyIsoCode, (SELECT id, Product2Id, Quantity, UnitPrice, TotalPrice, Description, Product2.Name, Product2.Family from Opportunity_Product__r), 
                                            order__c, Opportunity__c, License_Entry_End_Date__c, License_Entry_Start_Date__c
                                        FROM License_Entry__c 
                                        WHERE Opportunity__c =: opp.Id]) {
                if (!LIE.Opportunity_Product__r.isEmpty() && LIE.Opportunity_Product__r.get(0).Product2.Family != 'Services') {
                    lstLE.add(LIE);
                }
            }

            Map<Id, Order__c> orders = createOrder(lstLE, opp);
            insert orders.values();

            Map<Id, Sales_Order_Item__c> map_license_orderItem = new Map<Id, Sales_Order_Item__c> ();
            for (License_Entry__c LIE : lstLE) {
                if (orders.containsKey(LIE.id)) {
                    Order__c o = orders.get(LIE.id);
                    LIE.Order__c = o.id;
                    map_license_orderItem.putAll(createSalesOrderItem(o.id, LIE.Opportunity_Product__r, LIE.Opportunity__r.CurrencyIsoCode));
                }
            }
            insert map_license_orderItem.values();

            List<SObject> updateObjects = new List<SObject> ();
            for (id itemID : map_license_orderItem.keyset()) {
                Sales_Order_Item__c SOI = map_license_orderItem.get(itemID);
                OpportunityLineItem OLI = new OpportunityLineItem(id = itemID, Sales_Order_Item__c = SOI.id);
                updateObjects.add(OLI);
            }

            system.debug('updateObjects $$$' + updateObjects);
            updateObjects.addAll((list<sObject>) lstLE);
            updateObjects.add(opp);
            updateObjects.sort();
            update updateObjects;

            skedCreateInvoicesBatchTemp b = new skedCreateInvoicesBatchTemp(opp.Subscription_Order__c);
            Database.executeBatch(b,1);

            return '';
        } catch (DmlException ex) {
            Database.rollback(sp);
            system.debug('#error#' + ex.getDmlMessage(0) + ex.getLineNumber());
            return ex.getDmlMessage(0) + ex.getLineNumber();
        }
    }

    private static String getActiveContract(Opportunity opp, Boolean isActive) {
        Id oppAddonRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(SkeduloConstants.OPP_RECORDTYPE_ADDON).getRecordTypeId();
        Id oppRenewalRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(SkeduloConstants.OPP_RECORDTYPE_RENEWAL).getRecordTypeId();
        Subscription_Contract__c SC = new Subscription_Contract__c();
        
        if (opp.RecordTypeId == oppAddonRecordTypeId) {
            String availContractId = '';
            for (Subscription_Contract__c con : [SELECT Id, Name, Order_Status__c, Subscription_Contract_Start__c, Subscription_Contract_End__c 
                                                       FROM Subscription_Contract__c 
                                                       WHERE Account__c =: opp.AccountId
													   AND (Order_Status__c = 'Active' OR Order_Status__c = 'Not Started')]) {
                if (opp.Start_Date__c >= con.Subscription_Contract_Start__c && opp.End_Date__c <= con.Subscription_Contract_End__c) {
                    availContractId = con.Id;
                }
            }
        
            if (String.isBlank(availContractId)) {
                return null;
            }

            return availContractId;

        } else {
            SC = new Subscription_Contract__c(
                Purchase_Order_No__c = opp.Purchase_Order_No__c,
                Account__c = opp.AccountId,
                Currency__c = opp.Currency__c,
                Contact__c = opp.Primary_Contact__c,
                Order_Status__c = isActive ? 'Active' : 'Not Started', 
                BillingContact__c = opp.BillingContactId__c,
                Subscription_Contract_Start__c = opp.Start_Date__c,
                Subscription_Contract_End__c = opp.End_Date__c,
                Total_Invoiced__c = 0,
                Payment_Terms__c = opp.Payment_Terms__c,
                Payment_Method__c = opp.License_Payment_Method__c,
                CurrencyIsoCode = opp.CurrencyIsoCode,
                Initial_Opportunity__c = opp.Id
            );
            insert SC;
        }
        return SC.Id;
    }

    private static Map<Id, Order__c> createOrder(List<License_Entry__c> lstLE, Opportunity opp){

        Map<Id, Order__c> mapOrder = new Map<Id, Order__c>();
        for(License_Entry__c LE: lstLE){
            Order__c o = new Order__c(
                Subscription_Term__c = opp.Payment_Terms__c,
                License_Entry__c = Le.id,
                Order__c = opp.Subscription_Order__c,
                Subscription_Entry_End_Date__c = Le.License_Entry_End_Date__c,
                Subscription_Entry_Start_Date__c = Le.License_Entry_Start_Date__c,
                CurrencyIsoCode = Le.Opportunity__r.CurrencyIsoCode
            );
            mapOrder.put(Le.Id, o);
        }
        return mapOrder;
    }

    private static map<id,Sales_Order_Item__c> createSalesOrderItem(Id orderID, List<OpportunityLineItem> oppLines, string currencyCode){
        map<id,Sales_Order_Item__c> rtnMap = new map<id,Sales_Order_Item__c>();
        for(OpportunityLineItem OLI : oppLines){
            Sales_Order_Item__c SOI = new Sales_Order_Item__c(
                Is_subscription_Item__c = true,
                Line_Item_Description__c = OLI.Description,
                Line_Item_Name__c = OLI.Product2.Name,
                Line_Item_Quantity__c = OLI.Quantity,
                Line_Item_Total__c = OLI.TotalPrice,
                Line_Item_Total_ACV__c = OLI.TotalPrice * 12,
                Line_Item_Unit_Price__c = OLI.UnitPrice,
                Related_Product__c = OLI.Product2Id,
                Sales_Order__c = orderID,
                CurrencyIsoCode = currencyCode
            );
            rtnMap.put(OLI.id, SOI);
        }

        return rtnMap;
    }
}