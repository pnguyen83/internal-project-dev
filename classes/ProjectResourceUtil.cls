public class ProjectResourceUtil 
{
    /**
    //Update project resource when the asociated project update Project Manager field
    */
    public static void updateProjectManagerID(Map<String, Project__c> mapProject) {
        SavePoint sp = Database.setSavepoint();
        try {
            Set<Project_Resource__c> setPrjR = new Set<Project_Resource__c>();
            
            for (Project_resource__c prjR : [SELECT Id, Project__c, Resource__c, Contact_Role__c, Contact_Role__r.Role_Key__c 
                                             FROM Project_Resource__c 
                                             WHERE Project__c IN : mapProject.keySet() 
                                             AND Contact_Role_Key__c = :HardcodeUtil.PROJECT_MANAGER_KEY]) {
                if (mapProject.containsKey(prjR.Project__c)) {
                    Project__c newPM = mapProject.get(prjR.Project__c);
                    if (prjR.Resource__c != newPM.Project_Manager__c) {
                        setPrjR.add(prjR); //This is used to delete the old Manager Project Resource records
                    } else {
                        mapProject.remove(prjR.Project__c);
                    }
                }
            }
            //Update the PM Project Resource
            if (!setPrjR.isEmpty()) {
                delete new List<Project_Resource__c>(setPrjR);
            }
            
            //If there is not any PM project resource for these project, create the new one
            List<Project_Resource__c> lstPrjR = new List<Project_Resource__c>();
            String pmRoleID = [SELECT ID FROM Contact_Role__c WHERE Role_Key__c = :HardcodeUtil.PROJECT_MANAGER_KEY].ID;
            for (Project__c prj : mapProject.values()) {
                Project_Resource__c prjR = new Project_Resource__c();
                prjR.Project__c = prj.Id;
                prjR.Contact_Role__c = pmRoleID;
                prjR.Resource__c = prj.Project_Manager__c;
                lstPrjR.add(prjR);
            }
            
            if (!lstPrjR.isEmpty()) {
                insert lstPrjR;
            }
        }
        catch(Exception e) {
            Database.rollback(sp);
            LogUtil.generateLog('Error happened: ' + e.getMessage() + ', at line: ' + e.getLineNumber() + ', with trace stack: ' + e.getStackTraceString() + ' in ProjectUtil class');
        }
        
    }
    
    //==================================================================================================//
    /**
    Get the list of Project need to be update when a project resource update project manager position
    */
    public static void checkUpdateProjectManager(List<Project_Resource__c> lstNew, List<Project_Resource__c> lstOld) {
        Map<String, String> mapPrjManager = new Map<String, String>();
        Map<String, Project_Resource__c> mapProjectResources = new Map<String, Project_Resource__c>();
        
        //Get map of new Project manager resource
        for (Project_Resource__c prjR : lstNew) {
            mapProjectResources.put(prjR.Id, prjR);
        }
        
        if (lstOld != null) {
           for (Project_Resource__c prjR : lstOld) {
                if (mapProjectResources.containsKey(prjR.Id)) {
                    Project_Resource__c temp = mapProjectResources.get(prjR.Id);
                    if ((temp.Contact_Role_Key__c == 'PM' && prjR.Contact_Role_Key__c != 'PM') ||(temp.Contact_Role_Key__c != 'PM' && prjR.Contact_Role_Key__c == 'PM')) {
                        mapPrjManager.put(temp.Project__c, temp.Resource__c);
                    }
                }
            } 
        } else {
            for (Project_Resource__c prjR : lstNew) {
                if (prjR.Contact_Role_Key__c == 'PM') {
                        mapPrjManager.put(prjR.Project__c, prjR.Resource__c);
                }   
            }
        }
        
        
        if (!mapPrjManager.isEmpty()) {
            //Update Project Manager field on corresponding Project record
            SavePoint sp = Database.setSavepoint();
            List<Project__c> lstProjects = new List<Project__c>();

            try {
                for (Project__c prj : [SELECT Id, Project_Manager__c FROM Project__c WHERE Id IN : mapPrjManager.keySet()]) {
                    if (mapPrjManager.containsKey(prj.Id) && mapPrjManager.get(prj.Id) == prj.Project_Manager__c) {
                        mapPrjManager.remove(prj.Id);
                    }
                }
                for (String prjID : mapPrjManager.keySet()) {
                    Project__c prj = new Project__c(
                            Id = prjID,
                            Project_Manager__c = mapPrjManager.get(prjID)
                        );
                    lstProjects.add(prj);
                }
                update lstProjects;
            } catch (Exception e) {
                Database.rollback(sp);
                LogUtil.generateLog('Error happened: ' + e.getMessage() + ', at line: ' + e.getLineNumber() + ', with trace stack: ' + e.getStackTraceString());
            }
        }
    }
    
    //==================================================================================================//
    /**
    Delete the corresponding PM project resource when Project Manager field of associated project is delete
    */
    public static void deletePMProjectResource(Map<String, Project__c> mapPrj) {
        SavePoint sp = Database.setSavepoint();
        
        try {
            List<Project_Resource__c> lstPrjR = new List<Project_Resource__c>();
            
            for (Project_Resource__c prjR : [Select Id, Project__c, Resource__c 
                                             FROM Project_Resource__c 
                                             WHERE Project__c IN : mapPrj.keySet() 
                                             AND Contact_Role_Key__c = :HardcodeUtil.PROJECT_MANAGER_KEY]) {
                if (mapPrj.containsKey(prjR.Project__c) && mapPrj.get(prjR.Project__c).Project_Manager__c.equals(prjR.Resource__c)) {
                    lstPrjR.add(prjR);
                }
            }
            
            if (!lstPrjR.isEmpty()) {
                delete lstPrjR;
            }
        }
        catch(Exception e) {
            Database.rollback(sp);
            LogUtil.generateLog('Error happened: ' + e.getMessage() + ', at line: ' + e.getLineNumber() + ', with trace stack: ' + e.getStackTraceString() + ' in ProjectUtil class');
        }
    }
    
    //==================================================================================================//
    /**
    get list of Project that have PM project resource deleted
    */
    public static void updateProjectKeyContact(List<Project_Resource__c> lstOld) {
        Set<String> setPrjID = new Set<String>();
        for (Project_Resource__c prjR : lstOld) {
            if (prjR.Contact_Role_Key__c != null && prjR.Contact_Role_Key__c.equals(HardcodeUtil.PROJECT_MANAGER_KEY)) {
                setPrjID.add(prjR.Project__c);
            }
        }
        
        if (!setPrjID.isEmpty()) {
            //remove Project Manager field of associated Project
            ProjectUtil.removePMField(setPrjID);
        }
    }
    
    //==================================================================================================//
    /**
    Update Start Date and End Date based on Project information
    */
    public static void updateStartDateAndEndDate(List<Project_Resource__c> lstNew) {
        Map<String, Set<Project_Resource__c>> mapProjectResources = new Map<String, Set<Project_Resource__c>>();
        
        for (Project_Resource__c prjResource : lstNew) {
            if (mapProjectResources.containsKey(prjResource.Project__c)) {
                Set<Project_Resource__c> setProjectResources = mapProjectResources.get(prjResource.Project__c);
                setProjectResources.add(prjResource);
                mapProjectResources.put(prjResource.Project__c,setProjectResources);
            } else {
                mapProjectResources.put(prjResource.Project__c, new Set<Project_Resource__c>{prjResource});
            }
        }
        
        for (Project__c prj : [SELECT Id, Start__c, UAT_Due_Date__c, End__c FROM Project__c WHERE Id IN :mapProjectResources.keySet()]) {
            if (mapProjectResources.containsKey(prj.Id)) {
                Set<Project_Resource__c> setPrjResource = mapProjectResources.get(prj.Id);
                for (Project_Resource__c prjR : setPrjResource) {
                    prjR.Start_Date__c = prj.Start__c;
                    if (prj.End__c != null) {
                        prjR.End_Date__c = prj.End__c;
                    } else if (prj.End__c == null && prj.UAT_Due_Date__c != null) {
                        prjR.End_Date__c = prj.UAT_Due_Date__c;
                    }
                }
            }
        }
    }

    //==============================================================================================//
    /**
    Update Resource Requirement everytime a new resource is assigned to a project
    */
    public static void updateRequiredResource(List<Project_Resource__c> lstNew) {
        Map<String, List<Project_Resource__c>> mapPrjResources = new Map<String, List<Project_Resource__c>>();
        List<Resource_Requirement__c> lstResults = new List<Resource_Requirement__c>();

        for (Project_Resource__c prjRes : lstNew) {
            List<Project_Resource__c> lstPrjResources = mapPrjResources.get(prjRes.Project__c);
            if (lstPrjResources == null) {
                lstPrjResources = new List<Project_Resource__c>();
            }
            lstPrjResources.add(prjRes);
            mapPrjResources.put(prjRes.Project__c, lstPrjResources);
        }

        for (Resource_Requirement__c rr : [SELECT Id, Resource_Category_Name__c, Assigned__c, Project__c FROM Resource_Requirement__c WHERE Project__c IN : mapPrjResources.keySet()]) {
            if (mapPrjResources.containsKey(rr.Project__c)) {
                List<Project_Resource__c> lstPrjResources = mapPrjResources.get(rr.Project__c);
                for (Project_Resource__c prjRes : lstPrjResources) {
                    if (prjRes.Resource_Category_Name__c.equals(rr.Resource_Category_Name__c)) {
                        if (rr.Assigned__c == null) {
                            rr.Assigned__c = 0;
                        }
                        rr.Assigned__c += 1;
                    }
                }
                lstResults.add(rr);
            }
        }
        
        if (!lstResults.isEmpty()) {
            update lstResults;
        }
    }

    //===============================================================================================//
    /**
    Update the number of required resource when delete project resource
    */
    public static void updateRequiredResourceAfterDelete(List<Project_Resource__c> lstOld) {
        Map<String, List<Project_Resource__c>> mapPrjResources = new Map<String, List<Project_Resource__c>>();
        Set<Resource_Requirement__c> setResults =  new Set<Resource_Requirement__c>();

        for (Project_Resource__c prjR : lstOld) {
            List<Project_Resource__c> lstPrjResources = mapPrjResources.get(prjR.Project__c);
            if (lstPrjResources == null)  {
                lstPrjResources = new List<Project_Resource__c>();
            }

            lstPrjResources.add(prjR);
            mapPrjResources.put(prjR.Project__c, lstPrjResources);
        }

        for (Resource_Requirement__c resReq : [SELECT Id, Resource_Category_Name__c, Required_Number__c, Project__c, Assigned__c FROM Resource_Requirement__c WHERE Project__c IN :mapPrjResources.keySet()]) {
            if (mapPrjResources.containsKey(resReq.Project__c)) {
                List<Project_Resource__c> lstPrjResources = mapPrjResources.get(resReq.Project__c);
                for (Project_Resource__c prjR : lstPrjResources) {
                    if (prjR.Resource_Category_Name__c == resReq.Resource_Category_Name__c) {
                        resReq.Assigned__c -= 1;

                        setResults.add(resReq);
                    }
                }
            }
        }

        if (!setResults.isEmpty()) {
            update new List<Resource_Requirement__c>(setResults);
        }
    }

}