public with sharing class skedInvoiceTriggerHandler {
	public static void calculateSentInvoices(list<Invoice_Draft__c> lstNew, map<id, Invoice_Draft__c> oldMap){
		set<id> contractIds = new set<id>();

		for(Invoice_Draft__c inv: lstNew){
			
			system.debug('inv.Invoice_Status__c::' + inv.Invoice_Status__c);
			system.debug('inv.oldMap::' + oldMap.get(inv.id).Invoice_Status__c);

			if(inv.Invoice_Status__c == 'Sent' && inv.Invoice_Status__c != oldMap.get(inv.id).Invoice_Status__c){
				contractIds.add(inv.Subscription_Order__c);
			}
		}


		list<Subscription_Contract__c> updateContracts = new list<Subscription_Contract__c>();

		for(Subscription_Contract__c con : [select id, (select id, Invoice_Total__c from Invoices__r where Invoice_Status__c in ('Sent','Overdue','Complete')) from Subscription_Contract__c where id in:contractIds]){
			system.debug('inside Subscription_Contract__c ');
			Decimal totalInvoice = 0;
			for(Invoice_Draft__c inv : con.Invoices__r){
				totalInvoice += inv.Invoice_Total__c;
			}

			Subscription_Contract__c contract = new Subscription_Contract__c(id = con.id, Total_Invoiced__c = totalInvoice);
			updateContracts.add(contract);
		}

		if (updateContracts.size() > 0) update updateContracts;
	}

	public static void updateBillingPeriodDate(list<Invoice_Draft__c> lstNew){

		for (Invoice_Draft__c inv : lstNew) {

			if (inv.Type__c == 'License') {
				continue;
			}

			// Start Date
			if (inv.Invoice_Due_Date__c != null) {
				inv.Billing_Period_Start_Date__c = inv.Invoice_Due_Date__c;
			}
			// End Date
			if (inv.Months__c != null && inv.Invoice_Due_Date__c != null) {
				inv.Billing_Period_End_Date__c = inv.Invoice_Due_Date__c.addMonths(Integer.valueOf(inv.Months__c)).addDays(-1);
			}
		}
	}
}