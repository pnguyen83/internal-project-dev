global class skedInvoiceDueDateNotificationSchedule implements Schedulable {
	global void execute(SchedulableContext sc) {
		skedInvoiceDueDateNotificationBatch b = new skedInvoiceDueDateNotificationBatch();
		database.executebatch(b, 100);  
	}
}