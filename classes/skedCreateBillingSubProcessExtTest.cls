@isTest 
private class skedCreateBillingSubProcessExtTest {

	@isTest static void createSubContract() {
		skedDataFactory.initCommonData();

		Opportunity opp = [select id from Opportunity limit 1];
		opp.StageName = 'Closed Won';
		//opp.Primary_Win_Loss_Reason__c = 'Pricing';
        opp.Win_Loss_Notes__c = 'Running tests';
		update opp;

		Test.startTest();
		skedCreateBillingSubProcessExt.createContractForClosedWonOpp(opp.Id);
        Test.stopTest();
	}

	@isTest static void createSubContract2() {
		skedDataFactory.initCommonData();

		Account acc = skedDataFactory.createAccount();
		Contact billingContact = skedDataFactory.createBilling_Contactr(acc);
		Contact cont1 = skedDataFactory.createContact(acc);
		acc.Billing_Contact__c = billingContact.id;
		update acc;
		Opportunity opp = skedDataFactory.createOpportunity(acc, 'Initial', 'Negotiation');
		Subscription_Contract__c con = skedDataFactory.createSubscription_Orderr(opp, cont1);
		Opportunity opp1 = skedDataFactory.createOpportunity(acc, 'Add-On', 'Closed Won');
		update opp1;

		Test.startTest();
		skedCreateBillingSubProcessExt.createContractForClosedWonOpp(opp.Id);
        Test.stopTest();
	}
}