@isTest
private class TriggerHandlerTest
{
	static testmethod void handlerTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		sked__Resource__c resource1 = (sked__Resource__c)mapData.get('resource 1');
		Account acc1 = (Account)mapData.get('account 1');
		acc1.Client_Dropbox_Link__c = 'testlink2.com';
		Jira_Ticket__c ticket1 = (Jira_Ticket__c)mapData.get('jira ticket 1');
		update acc1;
		AvailabilityTriggerHandler handler = new AvailabilityTriggerHandler(true);
		Boolean a = handler.isTriggerContext;
		sked__Availability__c avai = (sked__Availability__c)mapData.get('availability 1');
		sked__Availability__c avai1 = new sked__Availability__c(
				sked__Start__c = avai.sked__Start__c,
				sked__Finish__c = avai.sked__Finish__c,
				sked__Resource__c = resource1.Id,
				sked__Timezone__c = 'Australia/Brisbane',
				sked__Type__c = 'Sick Leave'
			);
		
		try {
			List<sked__Availability__c> lstAvais2 = [SELECT Id, sked__Start__c, sked__Finish__c, sked__Type__c, sked__Resource__c 
    										FROM sked__Availability__c WHERE sked__Start__c >= TODAY];
    										System.debug('lstAvais = ' + lstAvais2);
			
			insert avai1;
			
		} catch (Exception ex) {
			System.debug('Error = ' + ex.getMessage());
		}
	}

	static testmethod void handlerTest2() { 
		Map<String, sObject> mapData = TestUtilities.createData();
		sked__Resource__c resource1 = (sked__Resource__c)mapData.get('resource 1');
		Account acc1 = (Account)mapData.get('account 1');
		acc1.Client_Dropbox_Link__c = 'testlink2.com';
		Jira_Ticket__c ticket1 = (Jira_Ticket__c)mapData.get('jira ticket 1');
		update acc1;
		
		try {
			
			update ticket1;
		} catch (Exception ex) {
			System.debug('Error = ' + ex.getMessage());
		}
	}
}