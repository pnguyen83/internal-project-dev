@isTest 
private class skedContractRenewalEmailNotifyBatchTest {

    public static testMethod void testschedule(){
        skedDataFactory.initCommonData();

        Subscription_Contract__c cont = [select id, Order_Status__c from Subscription_Contract__c limit 1];
        cont.Subscription_Contract_Start__c = Date.today();
        cont.Subscription_Contract_End__c = Date.today().addYears(1);
        cont.Order_Status__c = 'Active';
        update cont;

        Account acc = [select Id from Account limit 1];
        acc.Current_Contract_End_Date__c = Date.today().addDays(60);
        acc.CSM__c = skedDataFactory.createUser().Id;
        update acc;

        Test.startTest();
        skedContractRenewalEmailNotifySchedule  os = new skedContractRenewalEmailNotifySchedule();
        string sch =' 0 0 13 * * ?';
        system.schedule('Test', sch, os);

        skedContractRenewalEmailNotifyBatch b = new skedContractRenewalEmailNotifyBatch();
        database.executebatch(b, 100);
        Test.stopTest();
    }
}