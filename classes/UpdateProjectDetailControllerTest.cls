@isTest
private class UpdateProjectDetailControllerTest
{
	static testmethod void getDataTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		test.starttest();
		UpdateProjectDetailController.getAllProjectNames();
		UpdateProjectDetailController.getResourceCategories();
		test.stoptest();
	}

	static testmethod void searchOpportunityTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Opportunity opp1 = (Opportunity)mapData.get('opportunity 1');
		test.starttest();
		UpdateProjectDetailController.searchOpportunity(opp1.name);
		UpdateProjectDetailController.searchOpportunity('');
		test.stoptest();
	}

	static testmethod void searchRegionTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		sked__Region__c region1 = (sked__Region__c)mapData.get('region 1');
		test.starttest();
		UpdateProjectDetailController.searchRegion(region1.name);
		UpdateProjectDetailController.searchRegion('');
		test.stoptest();
	}

	static testmethod void searchAccountTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Account acc1 = (Account)mapData.get('account 1');
		Test.startTest();
        UpdateProjectDetailController.searchAccount(acc1.name); 
		Test.stopTest();
	}

	static testmethod void getOpportunityTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Opportunity opp1 = (Opportunity)mapData.get('opportunity 1');
		test.starttest();
		UpdateProjectDetailController.getOpportunity(opp1.Id);
		test.stoptest();
	}

	static testmethod void getAccountTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Account acc1 = (Account)mapData.get('account 1');
		Test.startTest();
        UpdateProjectDetailController.getAccount(acc1.name); 
		Test.stopTest();	
	}

	static testmethod void getConfigDataTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		test.starttest();
		UpdateProjectDetailController.getConfigData();
		test.stoptest();
	}

	static testmethod void saveNewProjectTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		Opportunity opp1 = (Opportunity)mapData.get('opportunity 1');
		sked__Resource__c resource1 = (sked__Resource__c)mapData.get('resource 1');
		sked__Region__c region1 = (sked__Region__c)mapData.get('region 1');
		Account acc1 = (Account)mapData.get('account 1');
		sked__Tag__c tag1 = (sked__Tag__c)mapData.get('tag 1');
		List<String> lstTagIDs = new List<String>{tag1.Id};

		ProjectModal.ResourceRequirementTag resTag = new ProjectModal.ResourceRequirementTag(tag1.Name, tag1.Id);
		List<ProjectModal.ResourceRequirementTag> lstTags = new List<ProjectModal.ResourceRequirementTag>{resTag};

		Resource_Requirement__c resReq1 = (Resource_Requirement__c)mapData.get('resource requirement 1');
		ProjectModal.ResourceRequirement prjResReq1 = new ProjectModal.ResourceRequirement();
		ProjectModal.ResourceRequirement prjResReq2 = new ProjectModal.ResourceRequirement(resReq1.Resource_Category__c, lstTagIDs, 2);
		prjResReq2.requiredTags = lstTags;
		List<ProjectModal.ResourceRequirement> lstResReqs = new List<ProjectModal.ResourceRequirement>{prjResReq2};

		ProjectModal prjModal = new ProjectModal('','test project 111',resource1.Id, region1.Id, 'In Development', 1, '100%', Date.today(), Date.today().addDays(10), Date.today().addDays(20), '6');
		prjModal.oppId = opp1.Id;
		prjModal.type = 'Internal Project';
		prjModal.accID = acc1.Id;
		prjModal.resourceRequirements = lstResReqs;

		test.startTest();
		UpdateProjectDetailController.saveNewProject(prjModal);
		test.stoptest();
	}

	static testmethod void getPickListFieldValuesTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		test.startTest();
		UpdateProjectDetailController.getPickListFieldValues('Project__c', 'Trial_Type__c');
		test.stoptest();
	}

	static testmethod void searchTagsTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		sked__Tag__c tag1 = (sked__Tag__c)mapData.get('tag 1');
		test.starttest();
		UpdateProjectDetailController.searchTags('');
		UpdateProjectDetailController.searchTags(tag1.name);
		test.stoptest();
	}

	static testmethod void searchProjectManagersTest() {
		Map<String, sObject> mapData = TestUtilities.createData();
		sked__Resource__c resource1 = (sked__Resource__c)mapData.get('resource 1');
		test.starttest();
		UpdateProjectDetailController.searchProjectManagers('');
		UpdateProjectDetailController.searchProjectManagers(resource1.name);
		test.stoptest();
	}
}