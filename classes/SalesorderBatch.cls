/********************* < Batch Class to execute Invoice creation class> ************************/

global class SalesorderBatch implements Database.Batchable<sObject>,  Database.AllowsCallouts, Database.Stateful{
     public boolean isApexTest = false;

    // Query to get the record of account.
    global String query = 'select Id, name from Account limit 1';
    
    global database.querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
    
        for(sObject s : scope){
           Account a = (Account) s;
        }
        
        // Executes classes        
        InvoiceCreation u = new InvoiceCreation();
        u.doAction();
       
    }
    
    global void finish(Database.BatchableContext sc){
    }

}