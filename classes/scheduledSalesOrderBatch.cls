/********************* < Schudable Class for the batch to execute all the classes of Geckoboard   > ************************/

global class scheduledSalesOrderBatch  implements Schedulable{

 //Exceute Method
    global void execute(SchedulableContext SC) {
        SalesorderBatch M = new SalesorderBatch ();
        database.executebatch(M);
    }
}