public with sharing class skedCloneInvoiceController {
	public Invoice_Draft__c invoice {get; set;}
	public Invoice_Draft__c originalInvoice {get; set;}
	
	public skedCloneInvoiceController(ApexPages.StandardController std) {
		if(!Test.isRunningTest()) {
			std.addFields(new List<String> { 'Invoice_Date__c', 'Invoice_Due_Date__c', 'Invoice_Status__c',
										'Type__c', 'Project__c', 'Subscription_Order__c', 'CurrencyIsoCode', 'OwnerId'});	
		}
		
		originalInvoice = (Invoice_Draft__c)std.getRecord();
		initInvoice(originalInvoice);
	}

	public PageReference save() {
		SavePoint sp = Database.setSavePoint();
		PageReference returnPage;

		try {
			saveInvoice();
			returnPage = new PageReference('/' + invoice.id);
		} catch(Exception ex) {
			Database.rollback(sp);
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error when insert: ' + ex.getMessage()));
		}
		
		return returnPage;
	}

	public PageReference cancel() {
		return new PageReference('/' + originalInvoice.Id);
	}

	public PageReference saveAndNew() {
		SavePoint sp = Database.setSavePoint();

		try {
			saveInvoice();
			invoice = new Invoice_Draft__c();
		} catch(Exception ex) {
			Database.rollback(sp);
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error when insert: ' + ex.getMessage()));
			System.debug(ex.getStackTraceString());
		}
		
		return null;
	}

	private void initInvoice(Invoice_Draft__c oldInvoice) {
		invoice = new Invoice_Draft__c();
		invoice.Invoice_Date__c = oldInvoice.Invoice_Date__c;
		invoice.Invoice_Due_Date__c = oldInvoice.Invoice_Due_Date__c;
		invoice.Invoice_Status__c = oldInvoice.Invoice_Status__c;
		invoice.Type__c = oldInvoice.Type__c;
		invoice.Project__c = oldInvoice.Project__c;
		invoice.Subscription_Order__c = oldInvoice.Subscription_Order__c;
		invoice.CurrencyIsoCode = oldInvoice.CurrencyIsoCode;
		invoice.OwnerId = oldInvoice.OwnerId;
	}

	private Invoice_Line_Items__c initLineItem (Invoice_Line_Items__c oldItem) {
		Invoice_Line_Items__c newItem = new Invoice_Line_Items__c(
			Line_Item_Description__c = oldItem.Line_Item_Description__c,
			Line_Item_Name__c = oldItem.Line_Item_Name__c,
			Line_Item_Quantity__c = oldItem.Line_Item_Quantity__c,
			Line_Item_Tax__c = oldItem.Line_Item_Tax__c,
			Line_Item_Total__c = oldItem.Line_Item_Total__c,
			Line_Item_Unit_Price__c = oldItem.Line_Item_Unit_Price__c,
			Product__c = oldItem.Product__c,
			Sales_Order_Item__c = oldItem.Sales_Order_Item__c,
			Type__c = oldItem.Type__c,
			Invoice__c = invoice.Id,
			CurrencyIsoCode = oldItem.CurrencyIsoCode
		);

		return newItem;
	}

	private void saveInvoice() {
		List<Invoice_Line_Items__c> lstNewInvoiceLineItems = new List<Invoice_Line_Items__c>();

		insert invoice;

		for (Invoice_Line_Items__c iLine : [SELECT Id, Line_Item_Description__c, Line_Item_Name__c, Line_Item_Quantity__c,
											Line_Item_Tax__c, Line_Item_Total__c, Line_Item_Unit_Price__c, Product__c,
											Sales_Order_Item__c, Type__c,CurrencyIsoCode
											FROM Invoice_Line_Items__c
											WHERE Invoice__c = :originalInvoice.Id
											]) {
			Invoice_Line_Items__c newItem = initLineItem(iLine);
			lstNewInvoiceLineItems.add(newItem);
		}

		if (!lstNewInvoiceLineItems.isEmpty()) {
			insert lstNewInvoiceLineItems;
		}
	}
}