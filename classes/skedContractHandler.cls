public with sharing class skedContractHandler {

    public static void onBeforeInsert(list<Subscription_Contract__c> newItems){        
        updateFields(newItems);
        checkActiveContract(newItems);
    }

    public static void onBeforeUpdate(list<Subscription_Contract__c> newItems, map<Id,Subscription_Contract__c> oldMap){        
        updateFields(newItems);
        checkActiveContract(newItems);
    }

    /*
    * Update fields.
    */
    private static void updateFields(list<Subscription_Contract__c> newItems){
        
        Set<Id> oppIds = new Set<Id>();
        for(Subscription_Contract__c con: newItems){
            if(String.isNotBlank(con.Initial_Opportunity__c)) oppIds.add(con.Initial_Opportunity__c);
        }
        
        Map<Id, Opportunity> mapOpps = new Map<Id, Opportunity>([SELECT Id, Terms_and_Conditions__c FROM Opportunity WHERE Id =: oppIds]);
        for(Subscription_Contract__c con: newItems){
            //1. update Order_Status__c.
            if(con.Order_Status__c == 'Not Started'){if(DATE.today() == con.Subscription_Contract_Start__c){con.Order_Status__c = 'Active';}}

            if(DATE.today() == con.Subscription_Contract_End__c  && con.Order_Status__c != 'Suspended' && con.Order_Status__c != 'Cancelled' ){con.Order_Status__c = 'Completed';}

            if(String.isNotBlank(con.Initial_Opportunity__c)) {
                con.Terms_and_Conditions__c = mapOpps.get(con.Initial_Opportunity__c).Terms_and_Conditions__c;
            }
        }
    }

    private static void checkActiveContract(List<Subscription_Contract__c> newItems) {
        
        Map<Id, Subscription_Contract__c> mapAccount = new Map<Id, Subscription_Contract__c>();
        Map<Id, String> mapAccountName = new Map<Id, String>();
        String baseURL = URL.getSalesforceBaseUrl().toExternalForm() + '/';
        for (Subscription_Contract__c con: newItems) {
            mapAccount.put(con.Account__c, null);
        }

        if(mapAccount.isEmpty()) return;

        for (Account acc : [SELECT Id, Name, (SELECT Id, Name, Order_Status__c, Subscription_Contract_Start__c, Subscription_Contract_End__c FROM Orders__r WHERE Order_Status__c = 'Active' LIMIT 1) 
                           FROM Account WHERE Id IN: mapAccount.keySet()]) {
            if (acc.Orders__r.isEmpty()) continue;
            mapAccount.put(acc.Id, acc.Orders__r.get(0));
            mapAccountName.put(acc.Id, acc.Name);
        }

        for (Subscription_Contract__c con: newItems) {
            if (mapAccount.get(con.Account__c) == null) continue;
            if (con.Id == mapAccount.get(con.Account__c).Id) continue;

            if (con.Subscription_Contract_Start__c <= mapAccount.get(con.Account__c).Subscription_Contract_End__c) {
                con.addError('Hey! You are trying to create a new contract where the start date is on or before the end date of this account\'s currently active Contract <a href="' + baseURL + mapAccount.get(con.Account__c).Id + '">' + mapAccount.get(con.Account__c).Name + '</a>', false);
                break;
            }
        }
    }

}