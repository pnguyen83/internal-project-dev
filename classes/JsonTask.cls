public class JsonTask{
	public String expand;	//schema,names
	public cls_issues[] issues;
    public Decimal cls_timespent;
    public Decimal cls_aggregatetimespent;
    public Decimal cls_timeoriginalestimate;
    public Decimal cls_aggregatetimeestimate;
    public Date cls_duedate;
	public class cls_issues {
		public String expand;	//operations,versionedRepresentations,editmeta,changelog,renderedFields
		public String id;	//11301
		public String self;	//https://jforce.atlassian.net/rest/api/2/issue/11301
		public String key;	//BU01-3
		public cls_fields fields;
	}
	public class cls_fields {
		public String summary;	//Sub task 1
		public cls_status status;
		public cls_priority priority;
		public cls_issuetype issuetype;
        public String description;
		public cls_reporter reporter;
        public Decimal aggregatetimeoriginalestimate;
        public Decimal timeestimate;
		public cls_aggregateprogress aggregateprogress;
		public Date duedate;
		public cls_progress progress;
		public cls_votes votes;
        public cls_assignee assignee;
	}
	public class cls_issuetype {
		public String self;	//https://jforce.atlassian.net/rest/api/2/issuetype/10002
		public String id;	//10002
		public String description;	//The sub-task of the issue
		public String iconUrl;	//https://jforce.atlassian.net/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype
		public String name;	//Sub-task
		public boolean subtask;
		public Integer avatarId;	//10316
	}
	public class cls_parent {
		public String id;	//11300
		public String key;	//BU01-2
		public String self;	//https://jforce.atlassian.net/rest/api/2/issue/11300
		public cls_fields fields;
	}
	public class cls_status {
		public String self;	//https://jforce.atlassian.net/rest/api/2/status/10000
		public String description;	//
		public String iconUrl;	//https://jforce.atlassian.net/
		public String name;	//To Do
		public String id;	//10000
		public cls_statusCategory statusCategory;
	}
	public class cls_statusCategory {
		public String self;	//https://jforce.atlassian.net/rest/api/2/statuscategory/2
		public Integer id;	//2
		public String key;	//new
		public String colorName;	//blue-gray
		public String name;	//To Do
	}
	public class cls_priority {
		public String self;	//https://jforce.atlassian.net/rest/api/2/priority/3
		public String iconUrl;	//https://jforce.atlassian.net/images/icons/priorities/medium.svg
		public String name;	//Medium
		public String id;	//3
	}
	public class cls_project {
		public String self;	//https://jforce.atlassian.net/rest/api/2/project/10306
		public String id;	//10306
		public String key;	//BU01
		public String name;	//BUS1
		public cls_projectCategory projectCategory;
	}
	public class cls_projectCategory {
		public String self;	//https://jforce.atlassian.net/rest/api/2/projectCategory/10000
		public String id;	//10000
		public String description;	//This is a testing project category
		public String name;	//Business
	}
	public class cls_watches {
		public String self;	//https://jforce.atlassian.net/rest/api/2/issue/BU01-3/watchers
		public Integer watchCount;	//1
		public boolean isWatching;
	}
	public class cls_assignee {
		public String self;	//https://jforce.atlassian.net/rest/api/2/user?username=admin
		public String name;	//admin
		public String key;	//admin
		public String emailAddress;	//hiepllp@gmail.com
		public cls_avatarUrls avatarUrls;
		public String displayName;	//hiep  [Administrator]
		public boolean active;
		public String timeZone;	//Asia/Jakarta
	}
	public class cls_avatarUrls {
		public String avatarURL;	//https://jforce.atlassian.net/secure/useravatar?size=medium&avatarId=10336
	}
	
	public class cls_creator {
		public String self;	//https://jforce.atlassian.net/rest/api/2/user?username=admin
		public String name;	//admin
		public String key;	//admin
		public String emailAddress;	//hiepllp@gmail.com
		public cls_avatarUrls avatarUrls;
		public String displayName;	//hiep  [Administrator]
		public boolean active;
		public String timeZone;	//Asia/Jakarta
	}
	public class cls_subtasks {
		public String id;	//11301
		public String key;	//BU01-3
		public String self;	//https://jforce.atlassian.net/rest/api/2/issue/11301
		public cls_fields fields;
	}
	public class cls_reporter {
		public String self;	//https://jforce.atlassian.net/rest/api/2/user?username=admin
		public String name;	//admin
		public String key;	//admin
		public String emailAddress;	//hiepllp@gmail.com
		public cls_avatarUrls avatarUrls;
		public String displayName;	//hiep  [Administrator]
		public boolean active;
		public String timeZone;	//Asia/Jakarta
	}
	public class cls_aggregateprogress {
		public Integer progress;	//0
		public Integer total;	//0
	}
	
	public class cls_progress {
		public Integer progress;	//0
		public Integer total;	//0
	}
	public class cls_votes {
		public String self;	//https://jforce.atlassian.net/rest/api/2/issue/BU01-3/votes
		public Integer votes;	//0
		public boolean hasVoted;
	}
	public static JsonTask parse(String json){
		return (JsonTask) System.JSON.deserialize(json, JsonTask.class);
	}
}