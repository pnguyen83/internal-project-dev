global with sharing class ResourceModal {
    public String projectResourceID {get;set;}
    public String resourceID {get;set;}
    public String name {get;set;}
    public String category {get;set;}
    public String mainCategory {get;set;}
    public List<ProjectModal> projects {get;set;}
    public Map<String, ProjectModal> mapProjectModals {get;set;}
    public List<Vacation> vacations {get;set;}
    public List<AssignedProject> projectsOnDate {get;set;}
    public boolean primaryProjectManager {get;set;}
    
    
    public ResourceModal(String resourceID, String name) {
        this.resourceID = resourceID;
        this.name = name;
        this.projects = new List<ProjectModal>();
        this.mapProjectModals = new Map<String, ProjectModal>();
        this.vacations = new List<Vacation>();
        this.projectsOnDate = new List<AssignedProject>();
    }

    public ResourceModal (sked__Resource__c resource) {
        this.resourceID = resource.Id;
        this.name = resource.name;
        this.projects = new List<ProjectModal>();
        this.vacations = new List<Vacation>();
        this.projectsOnDate = new List<AssignedProject>();
        this.mainCategory = String.isNotBlank(resource.Main_Category_Name__c) ? resource.Main_Category_Name__c : '';
    }

    public ResourceModal(Project_Resource__c prjResource) {
        this.projectResourceID = prjResource.Id;
        this.resourceID = prjResource.Resource__c;
        this.name = prjResource.Resource__r.Name;
        this.category = prjResource.Contact_Role__c != null ? prjResource.Contact_Role__r.name : '';
        this.mainCategory = prjResource.Resource__r.Main_Category_Name__c == null ? '' : prjResource.Resource__r.Main_Category_Name__c;

        ProjectModal prjModal = new ProjectModal(prjResource); 
        prjModal.recordTypeId = prjResource.Project__r.RecordTypeId;
        this.projects = new List<ProjectModal>{prjModal};
        this.mapProjectModals = new Map<String, ProjectModal>{prjModal.prjID => prjModal}; 
        this.vacations = new List<Vacation>();
        this.projectsOnDate = new List<AssignedProject>();
    }

    //====================================================================================//
    /**
    Get a specified resource
    */
    public static List<ResourceModal> getOneResource(String resourceID) {
        Map<String, ResourceModal> mapResourceModals = new Map<String, ResourceModal>();
        List<Vacation> lstVacations = new List<Vacation>();

        if (String.isNotBlank(resourceID)) {
            //Get vacation of resource
            Map<String, List<Vacation>> mapVacations = getVacations(new List<String>{resourceID});
            
            List<Project_Resource__c> lstPrjResources = [SELECT Id, Contact_Role__c, Contact_Role__r.name, Resource__c,Resource__r.Name, Project__c, 
                                                            Project__r.name, Resource__r.Main_Category_Name__c, Project__r.Project_Manager_Name__c, 
                                                            Project__r.Project_Region__c, Project__r.Status__c, Project__r.Complete__c, Project__r.Start__c, 
                                                            Project__r.UAT_Due_Date__c, Project__r.Resource_Allocation__c, Project__r.End__c , 
                                                            Project__r.Num_of_Req_Resources__c, Project__r.RecordTypeId,
                                                            Project__r.Actual_Deploy_to_Prod__c, Project__r.Actual_Development_Kick_Off__c,
                                                            Project__r.Actual_Go_Live_Date__c, Project__r.Actual_Hypercare_Kick_Off__c,
                                                            Project__r.Actual_Project_Completion__c, Project__r.Actual_Scope_Kick_Off__c, 
                                                            Project__r.Actual_UAT_Kick_Off__c 
                                                            FROM Project_Resource__c
                                                            WHERE   Resource__c = :resourceID 
                                                            AND Project__r.Status__c != 'Completed' 
                                                            AND Project__r.Status__c != 'Cancelled'
                                                            AND Project__r.Status__c != 'Closed'
                                                            AND Resource__r.sked__Is_Active__c = true 
                                                            ORDER By Project__r.UAT_Due_Date__c, Project__r.End__c
                                                        ];
            if (lstPrjResources != null && !lstPrjResources.isEmpty()) {
                for (Project_Resource__c prjR : lstPrjResources) {
                    if (mapResourceModals.containsKey(prjR.Resource__c)) {
                        insertProjectModaltoMap(mapResourceModals, prjR);
                    } else {
                        ResourceModal tempResourceModal = new ResourceModal(prjR);
                        lstVacations = mapVacations.get(prjR.Resource__c);
                        if (lstVacations != null && !lstVacations.isEmpty()) {
                            for (Vacation vaca : lstVacations) {
                                setResourceVacation(vaca, tempResourceModal);
                            }   
                        }
                        
                        mapResourceModals.put(tempResourceModal.resourceID, tempResourceModal);
                    }
                }
            }
        }

        return mapResourceModals.values();
    }

    //====================================================================================//
    /**
    Set vacation to Resource modal based on vacation type
    */
    private static void setResourceVacation(Vacation vaca, ResourceModal rsModal) {
        rsModal.vacations.add(vaca);
    }

    //====================================================================================//
    /**
    Get all Resource
    */
    public static List<ResourceModal> getAllResources(String category) {
        Map<String, ResourceModal> mapResourceModals = new Map<String, ResourceModal>();
        
        String sql = 'SELECT Id, Project__r.RecordTypeId, Contact_Role__c, Contact_Role__r.name, Resource__c,Resource__r.Name, Project__c, Project__r.name, Resource__r.Main_Category_Name__c,';
        sql += 'Project__r.Project_Manager_Name__c, Project__r.Project_Region__c, Project__r.Status__c, Project__r.Complete__c, Project__r.Num_of_Req_Resources__c,';
        sql += 'Project__r.Actual_Scope_Kick_Off__c,Project__r.Actual_UAT_Kick_Off__c,Project__r.Actual_Development_Kick_Off__c,';
        sql += 'Project__r.Actual_Deploy_to_Prod__c,Project__r.Actual_Go_Live_Date__c,Project__r.Actual_Hypercare_Kick_Off__c,Project__r.Actual_Project_Completion__c,';
        sql += 'Project__r.Start__c, Project__r.UAT_Due_Date__c, Project__r.Resource_Allocation__c, Project__r.End__c FROM Project_Resource__c WHERE Resource__r.sked__Is_Active__c = true'; 

        boolean hasCategory = String.isNotBlank(category);

        if (hasCategory) {
            sql += ' AND Contact_Role__r.name  = \'' + category + '\'';
        }
        System.debug('resource sql = ' + sql);
        List<Project_Resource__c> lstPrjResources = Database.query(sql);

        if (!hasCategory) {
            for (Project_Resource__c prjResource : lstPrjResources) {
                if (prjResource.Project__r.Status__c != 'Completed' && prjResource.Project__r.Status__c != 'Cancelled' && prjResource.Project__r.Status__c != 'Closed') {
                    if (mapResourceModals.containsKey(prjResource.Resource__c)) {
                        insertProjectModaltoMap(mapResourceModals, prjResource);
                    } else {
                        ResourceModal tempResourceModal = new ResourceModal(prjResource);
                        mapResourceModals.put(tempResourceModal.resourceID, tempResourceModal);
                    }
                }
            }
        } else {
            for (Project_Resource__c prjResource : lstPrjResources) {
                if (prjResource.Project__r.Status__c != 'Completed' && prjResource.Project__r.Status__c != 'Cancelled' && prjResource.Project__r.Status__c != 'Closed') {
                    if (mapResourceModals.containsKey(prjResource.Resource__c) && prjResource.Contact_Role__r.name.equals(category)) {
                        insertProjectModaltoMap(mapResourceModals, prjResource);
                    } else {
                        if (prjResource.Contact_Role__r.name.equals(category)) {
                            ResourceModal tempResourceModal = new ResourceModal(prjResource);
                            mapResourceModals.put(tempResourceModal.resourceID, tempResourceModal);
                        } 
                    }
                }
            }
        }

        //Get all resources and filter the resources that are not assigned to any project
        String setResInMap = getStringFromMapKeySet(mapResourceModals.keySet());
        String sql2 = 'SELECT Id, Name, Main_Category_Name__c, (SELECT Id, sked__Status__c, sked__Type__c, sked__Start__c, sked__Finish__c,'; 
        sql2 += 'sked__Timezone__c FROM sked__Availabilities1__r WHERE sked__Is_Available__c = false) FROM sked__Resource__c WHERE sked__Is_Active__c = true';
        if (hasCategory) {
            sql2 += ' AND Main_Category_Name__c = \'' + category + '\'';
        }
        System.debug('sql2 = ' + sql2);
        List<sked__Resource__c> lstResources = Database.query(sql2);

        for (sked__Resource__c resource : lstResources) {
            if (!mapResourceModals.containsKey(resource.Id)) {
                ResourceModal rsModal = new ResourceModal(resource);
                mapResourceModals.put(resource.Id, rsModal);    
            }
        }

        //Set vacation for resource Modal   
        Map<String, List<Vacation>> mapVacations = new Map<String, List<Vacation>>();
        mapVacations = getVacations(new List<String>(mapResourceModals.keySet()));

        for (ResourceModal tempResourceModal : mapResourceModals.values()) {
            if (mapVacations.containsKey(tempResourceModal.resourceID)) {
                List<Vacation> lstTempVacations = mapVacations.get(tempResourceModal.resourceID);
                if (lstTempVacations != null && !lstTempVacations.isEmpty()) {
                    for (vacation vaca : lstTempVacations) {
                        setResourceVacation(vaca, tempResourceModal);
                    }
                }
                
            }
        }

        return mapResourceModals.values();
    }

    //===============================================================================================//
    /**
    Generate a string of id from map key set
    */
    public static String getStringFromMapKeySet(Set<String> setKey) {
        String result = '(';

        for (String key : setKey) {
            result += '\'' + key + '\',';
        }

        result = result.removeEnd(',');
        result += ')';

        return result;
    }

    //===============================================================================================//
    /**
    Get resource details of a project
    */
    public static List<ResourceModal> getResourceModalsOfProject(String prjID) {
        Map<String, ResourceModal> mapResourceModals = new Map<String, ResourceModal>();
        Map<String, List<Vacation>> mapVacations = new Map<String, List<Vacation>>();
        Set<String> setResourceIDs = new Set<String>();

        if (String.isNotBlank(prjID)) {
            List<Project_Resource__c> lstPrjResources = [SELECT Id, Project__r.RecordTypeId, Contact_Role__c, Contact_Role__r.name, Resource__c,Resource__r.Name, Project__c, Project__r.name, Resource__r.Main_Category_Name__c,
                                                Project__r.Project_Manager_Name__c, Project__r.Project_Region__c, Project__r.Status__c, Project__r.Complete__c,
                                                Project__r.Start__c, Project__r.UAT_Due_Date__c, Project__r.Resource_Allocation__c, Project__r.End__c, project__r.Num_of_Req_Resources__c,
                                                Project__r.Actual_Deploy_to_Prod__c, Project__r.Actual_Development_Kick_Off__c,
                                                Project__r.Actual_Go_Live_Date__c, Project__r.Actual_Hypercare_Kick_Off__c,
                                                Project__r.Actual_Project_Completion__c, Project__r.Actual_Scope_Kick_Off__c, 
                                                Project__r.Actual_UAT_Kick_Off__c 
                                            FROM Project_Resource__c
                                            WHERE Project__c = :prjID AND Resource__r.sked__Is_Active__c = true
                                            ];
            if (lstPrjResources != null && !lstPrjResources.isEmpty()) {
                //get vacations
                for (Project_Resource__c prjResource : lstPrjResources) {
                    setResourceIDs.add(prjResource.Resource__c);
                }

                mapVacations = getVacations(new List<String>(setResourceIDs));

                for (Project_Resource__c prjResource : lstPrjResources) {
                    if (mapResourceModals.containsKey(prjResource.Resource__c)) {
                        insertProjectModaltoMap(mapResourceModals, prjResource);
                    } else {
                        ResourceModal tempResourceModal = new ResourceModal(prjResource);
                        if (mapVacations.containsKey(tempResourceModal.resourceID)) {
                            List<Vacation> lstTempVacations = mapVacations.get(tempResourceModal.resourceID);
                            if (lstTempVacations != null && !lstTempVacations.isEmpty()) {
                                for (Vacation vaca : lstTempVacations) {
                                    setResourceVacation(vaca, tempResourceModal);
                                }   
                            }
                        }
                        mapResourceModals.put(tempResourceModal.resourceID, tempResourceModal);
                    }
                }
            }
        }

        return mapResourceModals.values();
    }   

    //===============================================================================================//
    /**
    insert ProjectModal into a map
    */
    private static void insertProjectModaltoMap(Map<String, ResourceModal> mapResourceModals, Project_Resource__c prjResource) {
        ResourceModal tempResourceModal = mapResourceModals.get(prjResource.Resource__c);
        Map<String, ProjectModal> mapProjectModals = tempResourceModal.mapProjectModals;

        if (!mapProjectModals.containsKey(prjResource.Project__c)) {
            ProjectModal prjModal = new ProjectModal(prjResource); 
            tempResourceModal.mapProjectModals.put(prjModal.prjId, prjModal);
            tempResourceModal.projects.add(prjModal);
            mapResourceModals.put(prjResource.Resource__c, tempResourceModal);
        } 
    }

    //================================================================================================//
    /**
    Get vaccations of resources based on resource ids
    */
    public static Map<String, List<Vacation>> getVacations(List<String> lstResourceIDs) {
        Map<String, List<Vacation>> mapResults = new Map<String, List<Vacation>>();
        for (sked__Availability__c avai: [SELECT Id, sked__Start__c, sked__Finish__c, sked__Timezone__c, sked__Status__c, sked__Resource__c, sked__Type__c 
                                            FROM sked__Availability__c 
                                            WHERE sked__Resource__c IN : lstResourceIDs 
                                            //AND sked__Status__c = 'Approved' 
                                            AND sked__Finish__c >= TODAY AND sked__Type__c != 'Available' AND sked__Is_Available__c = false]) {
            List<Vacation> lstTemps = mapResults.get(avai.sked__Resource__c);
            if (lstTemps == null) {
                lstTemps = new List<Vacation>();
            }
            lstTemps.add(new Vacation(avai.sked__Start__c, avai.sked__Finish__c, avai.sked__Type__c));
            mapResults.put(avai.sked__Resource__c, lstTemps); 
            
        }
        return mapResults;
    }

    //==============================================================================================//
    /**
    Get vacation of resources based on project IDs
    */
    public static Map<String, Map<String, List<Vacation>>> getVacationFromProjectID(List<String> lstPrjIDs) {
        Map<String, List<String>> mapResourcePrj = new Map<String, List<String>>();
        Map<String, List<Vacation>> mapResourceVacations = new Map<String, List<Vacation>>();
        Map<String, Map<String, List<Vacation>>> mapResult = new Map<String, Map<String, List<Vacation>>>();

        for (Project_Resource__c prjResource : [SELECT Id, Resource__c, Project__c FROM Project_Resource__c WHERE Project__c IN : lstPrjIDs]) {
            List<String> lstTempPrjIDs = mapResourcePrj.get(prjResource.Resource__c);
            if (lstTempPrjIDs == null) {
                lstTempPrjIDs = new List<String>();
            }
            lstTempPrjIDs.add(prjResource.Project__c);
            mapResourcePrj.put(prjResource.Resource__c, lstTempPrjIDs);
        }       

        if (!mapResourcePrj.isEmpty()) {
            mapResourceVacations = getVacations(new List<String>(mapResourcePrj.keySet()));
            for (String resourceID : mapResourceVacations.keySet()) { //get Id of resource who has vacations
                if (mapResourcePrj.containsKey(resourceID)) { //check the resource ID
                    List<String> lstTempPrjIDs = mapResourcePrj.get(resourceID); //Get list of project ID of that resource
                    List<Vacation> lstVacations = mapResourceVacations.get(resourceID); //get list vacations of that resource
                    for (String prjID : lstTempPrjIDs) {
                        Map<String, List<Vacation>> mapTempResourceVacation = mapResult.get(prjID);
                        if (mapTempResourceVacation == null) {
                            mapTempResourceVacation = new Map<String, List<Vacation>>();
                        }
                        mapTempResourceVacation.put(resourceID, lstVacations);
                        mapResult.put(prjID, mapTempResourceVacation);
                    }
                }
            }
        }

        return mapResult;
    }

    //==============================================================================================//
    /**
    Get number of projects assign to a resource in a day
    */
    public static void getAssignedProjects(List<ResourceModal> lstRsModal, Date startDate, Date endDate) {
        //Map<String, List<AssignedProject>> mapResAssignedPrjs = new Map<String, List<AssignedProject>>();
        Map<String, Map<Date,AssignedProject>> mapResAssignedPrjs = new Map<String, Map<Date,AssignedProject>>();
        List<String> lstResourceIDs = new List<String>();

        
        for (ResourceModal rsModal : lstRsModal) {
            lstResourceIDs.add(rsModal.resourceID);
        }

        for (Project_Resource__c prjRes : [SELECT Id, Name, Project__c, Resource__c, Resource__r.name, Start_Date__c, End_Date__c 
                                            FROM Project_Resource__c 
                                            WHERE Project__r.Status__c != 'Completed' 
                                            AND Project__r.Status__c != 'Cancelled'
                                            AND Project__r.Status__c != 'Closed' 
                                            AND Resource__c IN : lstResourceIDs]) {
            Date onDate = startDate;
            while (onDate <= endDate) {
                if (onDate >= prjRes.Start_Date__c && onDate <= prjRes.End_Date__c) {
                    //List<AssignedProject> lstAssignedProjects = mapResAssignedPrjs.get(prjRes.Resource__c);
                    Map<Date, AssignedProject> mapAssignPrjs = mapResAssignedPrjs.get(prjRes.Resource__c);

                    if (mapAssignPrjs == null) {
                        mapAssignPrjs = new Map<Date, AssignedProject>();
                        AssignedProject tempPrj = new AssignedProject(onDate, 1);
                        mapAssignPrjs.put(onDate, tempPrj);
                    } else {
                        AssignedProject tempAssignedPrj = mapAssignPrjs.get(onDate);
                        if (tempAssignedPrj == null) {
                            tempAssignedPrj = new AssignedProject(onDate, 0);
                        }
                        tempAssignedPrj.quantity += 1;
                        mapAssignPrjs.put(onDate, tempAssignedPrj);
                    }
                    
                    mapResAssignedPrjs.put(prjRes.Resource__c, mapAssignPrjs);
                }
                onDate = onDate.addDays(1);
            }
        }

        for (ResourceModal rsModal : lstRsModal) {
            if (mapResAssignedPrjs.containsKey(rsModal.resourceID)) {
                Map<Date, AssignedProject> mapTempAssignedPrjs = mapResAssignedPrjs.get(rsModal.resourceID);
                rsModal.projectsOnDate = mapTempAssignedPrjs.values();
                //System.debug('rsModal.projectOnDate = ' + rsModal.projectOnDate + ', resourceID =  ' + rsModal.resourceID);
            }
        }

    }

    
    //==============================================================================================//
    /**
    Vacation class
    */
    public class Vacation {
        public DateTime startTime {get;set;}
        public DateTime endTime {get;set;}
        public String   name    {get;set;}

        public Vacation (DateTime startTime, DateTime endTime, String name) {
            this.startTime = startTime;
            this.endTime = endTime;
            this.name = name;
        }
    }

    //===============================================================================================//
    /**
    Assign Project class
    */
    public class AssignedProject {
        public Date onDate {get;set;}
        public Decimal quantity {get;set;}
        
        public AssignedProject(Date selectedDate, Decimal quantity) {
            this.onDate = selectedDate;
            this.quantity = quantity;
        }
    }   
}