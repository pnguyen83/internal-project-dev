public class ProjectResourceTriggerHandler 
{
	private boolean m_isExecuting = false;
    
	public ProjectResourceTriggerHandler(boolean isExecuting)
    {
        m_isExecuting = isExecuting;
    }
    
    public boolean isTriggerContext
    {
        get{ return m_isExecuting;}
    }
    
    public void onAfterInsert(List<Project_Resource__c> lstNew)
    {
        //check if the PM contact Id is the same with corresponding Project Manager
        ProjectResourceUtil.checkUpdateProjectManager(lstNew, null);
        //update related required resource when assign a new resource to a project
        ProjectResourceUtil.updateRequiredResource(lstNew);
    }
    
    public void onAfterUpdate(List<Project_Resource__c> lstNew, List<Project_Resource__c> lstOld)
    {
        //check if the PM contact Id is the same with corresponding Project Manager
        ProjectResourceUtil.checkUpdateProjectManager(lstNew, lstOld);
    }
    
    public void onAfterDelete(List<Project_Resource__c> lstOld)
    {
        //If the deleted project resource is a PM project resource, remove Project Manager field on associated project
        ProjectResourceUtil.updateProjectKeyContact(lstOld);
        //Update number of required resource if a project resource is deleted
        ProjectResourceUtil.updateRequiredResourceAfterDelete(lstOld);
    }
    
    public void onBeforeInsert(List<Project_Resource__c> lstNew) {
        ProjectResourceUtil.updateStartDateAndEndDate(lstNew);
    }
}