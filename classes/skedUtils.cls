public class skedUtils {
    
    public static string TIMESLOT_KEY_FORMAT = 'dd/MM/yyyy hh:mm a';
    public static string DATE_FORMAT = 'yyyy-MM-dd';
    public static string DATE_ISO_FORMAT = '"yyyy-MM-dd"';

    public static string CLOSED_WON = 'Closed Won';

    
    public static integer GetDifferentDays(string fromDateIso, string toDateIso) {
        Date fromDate = (Date)Json.deserialize(fromDateIso, Date.class);
        Date toDate = (Date)Json.deserialize(toDateIso, Date.class);
        
        return fromDate.daysBetween(toDate);
    }
    
    public static DateTime GetStartOfDate(string dateIsoString, string timezoneSidId) {
        Date tempDate = (Date)Json.deserialize(dateIsoString, Date.class);
        DateTime result = DateTime.newInstance(tempDate, time.newInstance(0, 0, 0, 0));
        result = skedUtils.ConvertBetweenTimezones(result, timezoneSidId, UserInfo.getTimeZone().getID());
        return result;
    }
    
    public static DateTime GetStartOfDate(DateTime input, string timezoneSidId) {
        String dateIsoString = input.format(DATE_ISO_FORMAT, timezoneSidId);
        return GetStartOfDate(dateIsoString, timezoneSidId);
    }
    
    public static DateTime GetEndOfDate(DateTime input, string timezoneSidId) {
        return GetStartOfDate(input, timezoneSidId).addDays(1);
    }
    
    public static string ConvertDateToIsoString(Date input) {
        string result = Json.serialize(input).replace('"', '');
        return result;
    }
    
    public static Datetime convertToTimezone(Datetime currentDT, string TZ) {
        Timezone SetTZ = Timezone.getTimeZone(TZ); // get target timezone  
        Timezone currentTZ = Userinfo.getTimeZone(); // get current user timezone

        integer setOffSet = SetTZ.getoffset(currentDT); //calculate target timezone offset
        integer curOffSet = currentTZ.getoffset(currentDT); //calculate current timezone offset

        integer resOffSet = curOffSet - setOffSet;

        integer offSet = resOffSet / (1000);
        DateTime localDT = currentDT.addSeconds(offSet);
        return localDT;
    }
    
    /**

    */
    public static Date ConvertToDateValue(string dateString) {
        String[] temp = dateString.split('-');
        return Date.newInstance(Integer.valueOf(temp[0]), Integer.valueOf(temp[1]), Integer.valueOf(temp[2]));
    }
    
    /**

    */
    public static DateTime ConvertBetweenTimezones(DateTime input, string fromTimezoneSidId, string toTimezoneSidId) {
        if (fromTimezoneSidId == toTimezoneSidId) {
            return input;
        }
        TimeZone fromTz = Timezone.getTimeZone(fromTimezoneSidId);
        Timezone toTz = Timezone.getTimeZone(toTimezoneSidId);
        integer offsetMinutes = toTz.getOffset(input) - fromTz.getOffset(input);
        offsetMinutes = offsetMinutes / 60000;
        input = input.addMinutes(offsetMinutes);
        return input;
    }
    
    /**

    */
    // @param inputTime: 730
    // @result: 730 will be considered as 7 hour and 30 minutes so the result should be 7 * 60 + 30 = 450
    public static integer ConvertTimeNumberToMinutes(integer inputTime) {
        return integer.valueOf(inputTime / 100) * 60 + Math.mod(inputTime, 100);
    }
    
    public static Set<Date> getHolidaysByRegion(Id regionId) {
        Set<Date> allHolidays = new Set<Date>();
        
        Map<string, Set<Date>> mapHolidays = skedUtils.getHolidays(system.today());
        if (mapHolidays.containsKey(SkeduloConstants.GLOBAL_HOLIDAYS)) {
            Set<Date> globalHolidays = mapHolidays.get(SkeduloConstants.GLOBAL_HOLIDAYS);
            allHolidays.addAll(globalHolidays);
        }
        if (mapHolidays.containsKey(regionId)) {
            Set<Date> regionHolidays = mapHolidays.get(regionId);
            allHolidays.addAll(regionHolidays);
        }
        
        return allHolidays;
    }
    
    /**

    */
    public static Map<string, Set<Date>> getHolidays(Date currentDate) {
        Map<string, Set<Date>> mapHolidays = new Map<string, Set<Date>>();
        List<sked__Holiday__c> skedGlobalHolidays = [SELECT Id, sked__Start_Date__c, sked__End_Date__c
                                                     FROM sked__Holiday__c
                                                     WHERE sked__Global__c = TRUE
                                                     AND sked__End_Date__c >= :currentDate];
        List<sked__Holiday_Region__c> skedRegionHolidays = [SELECT Id, sked__Holiday__r.sked__Start_Date__c, sked__Holiday__r.sked__End_Date__c, 
                                                            sked__Region__r.Name
                                                            FROM sked__Holiday_Region__c
                                                            WHERE sked__Holiday__r.sked__End_Date__c >= :currentDate];
        
        Set<Date> globalHolidays = new Set<Date>();
        for (sked__Holiday__c globalHoliday : skedGlobalHolidays) {
            Date tempDate = globalHoliday.sked__Start_Date__c;
            while (tempDate <= globalHoliday.sked__End_Date__c) {
                globalHolidays.add(tempDate);
                tempDate = tempDate.addDays(1);
            }
        }
        mapHolidays.put('global', globalHolidays);
        
        for (sked__Holiday_Region__c regionHoliday : skedRegionHolidays) {
            Set<Date> regionHolidays;
            if (mapHolidays.containsKey(regionHoliday.sked__Region__r.Name)) {
                regionHolidays = mapHolidays.get(regionHoliday.sked__Region__r.Name);
            } else {
                regionHolidays = new Set<Date>();
            }
            
            Date tempDate = regionHoliday.sked__Holiday__r.sked__Start_Date__c;
            while (tempDate <= regionHoliday.sked__Holiday__r.sked__End_Date__c) {
                regionHolidays.add(tempDate);
                tempDate = tempDate.addDays(1);
            }
            
            if (!mapHolidays.containsKey(regionHoliday.sked__Region__r.Name)) {
                mapHolidays.put(regionHoliday.sked__Region__r.Name, regionHolidays);
            }
        }
        return mapHolidays;
    }

    /**
     * 
     */
    public static string toDateString(Date inputDate){
        if(inputDate == null) return '';
        Datetime dtime = Datetime.newInstance(inputDate, Time.newInstance(0, 0, 0, 0));
        return dtime.format(DATE_FORMAT);
    }

    /**
     * 
     */
    public static Date toDate(String ds){
        list<String> dString = ds.split('-');
        if(dString.size() <3) return null;
        return Date.newInstance(Integer.valueOf(dString[0]), Integer.valueOf(dString[1]), Integer.valueOf(dString[2].substring(0,2)));

    }

    public static list<skedOpportunityDataModal.PickListItem> getPicklistValues(string objName, String fld){

          list<skedOpportunityDataModal.PickListItem> options = new list<skedOpportunityDataModal.PickListItem>();
          Schema.DescribeSObjectResult objDescribe = Schema.getGlobalDescribe().get(objName).getDescribe();       
          // Get a map of fields for the SObject
          map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
          // Get the list of picklist values for this field.
          list<Schema.PicklistEntry> values =fieldMap.get(fld).getDescribe().getPickListValues();
          // Add these values to the selectoption list.
          for (Schema.PicklistEntry a : values)
          { 
             options.add(new skedOpportunityDataModal.PickListItem(a.getValue(), a.getLabel() )); 
          }
          return options;
     }

}