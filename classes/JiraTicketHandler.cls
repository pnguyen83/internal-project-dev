public class JiraTicketHandler {
    private boolean m_isExecuting = false;
    
    public JiraTicketHandler(boolean isExecuting) {
        m_isExecuting = isExecuting;
    }
    
    public boolean isTriggerContext {
        get{ return m_isExecuting;}
    }
    
    public void onBeforeUpdate(List<Jira_Ticket__c> lstNew, List<Jira_Ticket__c> lstOld) {
        List<Jira_Ticket__c> lstJiraTickets = new List<Jira_Ticket__c>();
        PreventEditFromSalesforce(lstNew, lstOld, lstJiraTickets);
        
    }
    
        
    //***********************************************************PRIVATE FUNCTIONS************************************************************//
    private void PreventEditFromSalesforce(List<Jira_Ticket__c> lstNew, List<Jira_Ticket__c> lstOld, List<Jira_Ticket__c> lstJiraTickets) {
        Map<Id, Jira_Ticket__c> mapJiraTickets = new Map<Id, Jira_Ticket__c>();
        
        for (Jira_Ticket__c ticket : lstNew) {
            if (ticket.isUpdateFromJira__c == false) {
                mapJiraTickets.put(ticket.Id, ticket);
            }
            else {
                lstJiraTickets.add(ticket);
            }
        }
        
        for (Jira_Ticket__c ticket : lstOld) {
            if (ticket.isUpdateFromJira__c == false && mapJiraTickets.containsKey(ticket.Id)) {
                Jira_Ticket__c tempTicket = mapJiraTickets.get(ticket.Id);
                tempTicket.addError(HardcodeUtil.EDIT_JIRA_TICKET_ERROR);
            }
        }
    }
    
}