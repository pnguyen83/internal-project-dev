public class SkeduloConstants {
    
    public static final string DATE_PARSE_FORMAT = 'M/d/yyyy';
    
    public static final string JOB_STATUS_CANCELLED = 'Cancelled';
    public static final string JOB_STATUS_COMPLETE = 'Complete';
    public static final string JOB_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_STATUS_IN_PROGRESS = 'In Progress';
    public static final string JOB_STATUS_PENDING_ALLOCATION = 'Pending Allocation';
    public static final string JOB_STATUS_QUEUED = 'Queued';
    public static final string JOB_STATUS_READY = 'Ready';
    
    public static final string ALLOCATION_STATUS_NO_ALLOCATION = 'No Allocation';
    public static final string ALLOCATION_STATUS_PARTIALLY_ALLOCATED = 'Partially Allocated';
    public static final string ALLOCATION_STATUS_FULLY_ALLOCATED = 'Fully Allocated';
    
    public static final string JOB_ALLOCATION_STATUS_COMPLETE = 'Complete';
    public static final string JOB_ALLOCATION_STATUS_CONFIRMED = 'Confirmed';
    public static final string JOB_ALLOCATION_STATUS_DECLINED = 'Declined';
    public static final string JOB_ALLOCATION_STATUS_DELETED = 'Deleted';
    public static final string JOB_ALLOCATION_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_ALLOCATION_STATUS_IN_PROGRESS = 'In Progress';
    public static final string JOB_ALLOCATION_STATUS_PENDING_DISPATCHED = 'Pending Dispatched';
    
    public static final string RESOURCE_ROLE_REMOVALIST = 'Removalist';
    public static final string RESOURCE_ROLE_DIVER = 'Diver';
    public static final string RESOURCE_ROLE_PORTER_TEAM_LEADER = 'Porter team Leader';
    public static final string RESOURCE_ROLE_TECH_PORTER_ITFM = 'Tech Porter - ITFM';
    public static final string RESOURCE_ROLE_SERVER_PORTER_ITFM = 'Server Porter - ITFM';
    public static final string RESOURCE_ROLE_SERVER_TECH_ITFM = 'Server Tech - ITFM';
    public static final string RESOURCE_ROLE_DESK_TEAM_LEAD_ITFM = 'Desk Team Lead - ITFM';
    public static final string RESOURCE_ROLE_SERVER_TEAM_LEAD_ITFM = 'Server Team Lead - ITFM';
    
    public static final string RESOURCE_TYPE_PERSON = 'Person';
    
    public static final string GLOBAL_HOLIDAYS = 'global';
    
    public static final string USER_USERTYPE = '';
    
    public static final string JOB_GROUP_ALLOCATION_STATUS_CONSOLE_PINK = 'pink';
    public static final string JOB_GROUP_ALLOCATION_STATUS_CONSOLE_YELLOW = 'yellow';
    public static final string JOB_GROUP_ALLOCATION_STATUS_CONSOLE_GREEN = 'green';

    public static final String OPP_FORECAST_PRODUCT = 'Skedulo Opp Forecast';
    public static final String NOT_CLOSE_WON_OPP_WITH_FORECAST_PRODUCT = 'Hey! You are trying to Close Won an opportunity with the "Skedulo Opp Forecast" product assigned. This is only used to forecast the opportunity pipeline. Please can you remove this product line item and add the contract products sold for this opp';

	public static final String OPP_RECORDTYPE_INITIAL = 'License - Initial';
	public static final String OPP_RECORDTYPE_ADDON = 'License - Add-On';
	public static final String OPP_RECORDTYPE_RENEWAL = 'License - Renewal';

    public static final String MILESTONE_DEVELOPEMENT_KICK_OFF = 'Development Kick Off';
    public static final String MILESTONE_DEPLOY_TO_PRODUCTION = 'Deploy to Prod';
    public static final String MILESTONE_GO_LIVE_DATE = 'Go Live Date';
    public static final String MILESTONE_HYPERCARE_KICK_OFF = 'Hypercare Kick Off';
    public static final String MILESTONE_PROJECT_COMPLETION = 'Project Completion';
    public static final String MILESTONE_SCOPE_KICK_OFF = 'Scope Kick Off';
    public static final String MILESTONE_UAT_KICK_OFF = 'UAT Kick Off';
    
}