global class skedGenerateInvoiceCrl extends skedOpportunityDataModal{
    /*
    * Inner Modal classes
    */
    public decimal taxPercent {get;set;}//percentage.

    public class ProjectModal {
        public string id {get;set;}
        public list<InvoiceEntryModal>  entries         {get;set;} //
        public list<ProductModal>       products        {get;set;} //
        public list<PicklistItem> invoiceTypes {get;set;}
        
        public ProjectModal(string id, list<InvoiceEntryModal>  entries, list<ProductModal> products, list<PicklistItem> invoiceTypes){
            this.id =  id;
            this.entries = entries;
            this.products = products;
            this.invoiceTypes = invoiceTypes;
        }
    }

    /*
    */
    global skedGenerateInvoiceCrl(ApexPages.StandardController stdController){
        taxPercent = 0;
        Project__c proj = (Project__c)stdController.getRecord();

        list<Project__c> projects = [select id,Account__r.Country_Code__c from Project__c where id=:proj.Id limit 1];
        if(!projects.isEmpty()){
            CountryTax__c countryTax = CountryTax__c.getvalues(projects[0].Account__r.Country_Code__c);
            if(countryTax != null) taxPercent = countryTax.Tax__c/100;
        } 
    }	


	public static list<ProductModal> loadProducts(string countryCode){
		list<PricebookEntry> productObjs = [SELECT id, Product2.Id, UnitPrice, Product2.Name, IsActive, ProductCode,Product2.Description FROM PricebookEntry 
    		WHERE Pricebook2.isStandard = true and Product2.Family !=:'License' and CurrencyIsoCode = : countryCode AND IsActive = true];	

	    list<ProductModal> products = new list<ProductModal>();
	    for(PricebookEntry entry:productObjs){
	    		products.add(new ProductModal(entry.Product2.Id, entry.Product2.Name,entry.Product2.description, entry.UnitPrice) );
	    }
	   return products;
	}

	@RemoteAction
    global static skedResponse loadInvoiceData(String projectId){
    	skedResponse response = new skedResponse(true, '', '');

    	try{    		   		

            Project__c pro = [select id, Account__r.Country_Code__c from Project__c where id=:projectId];
            string currencyIso = 'USD';
            if(pro.Account__r.Country_Code__c == 'au' || pro.Account__r.Country_Code__c == 'nz') currencyIso = 'AUD';

	    	list<InvoiceEntryModal> entries = new list<InvoiceEntryModal>();    	
	    	list<Invoice_Draft__c> invoiceEntries = [select id, name, CurrencyIsoCode,
	    		Currency__c, Invoice_Date__c,Invoice_Due_Date__c, Invoice_Status__c, Invoice_Tax__c, Invoice_Total__c, Invoice_Total_Ex_Tax__c,
	    		Project__c,Subscription_Order__c, Total_Outstanding__c, Total_Paid__c,    		
	    			(select id, name, Invoice__c, Type__c, Line_Item_Description__c,Line_Item_Name__c, Line_Item_Quantity__c, Line_Item_Tax__c, Line_Item_Total__c,
	    				Line_Item_Unit_Price__c, Product__c, Sales_Order_Item__c
	    			from Invoice_Line_Items__r order by CreatedDate)
	    	from Invoice_Draft__c 
	    	where Project__c =:projectId order by CreatedDate];

	    	// map to modal.
	    	for(Invoice_Draft__c entry: invoiceEntries){
	    		InvoiceEntryModal entryModal = new InvoiceEntryModal(entry);
	    		//line items.
	    		for(Invoice_Line_Items__c lineItem: entry.Invoice_Line_Items__r){
	    			InvoiceEntryItemModal lineItemModal = new InvoiceEntryItemModal(lineItem);
	    			entryModal.lines.add(lineItemModal);
	    		}
	    		entries.add(entryModal);
	    	}            

            response.result = new ProjectModal(projectId, entries, loadProducts(currencyIso), skedUtils.getPicklistValues('Invoice_Line_Items__c','Type__c'));

    	}  catch(Exception ex){
    		response.getErrorMessage(ex);response.success = false;
    		
    	}

    	return response;

    }

    /*
    * Save data:
    * - Invoice__c
    * --- Invoice_Line_Items__c
    */

    @RemoteAction
    global static skedResponse saveEntries(String entriesJson,string projectId){
    	skedResponse response = new skedResponse(true, '', '');


        Project__c pro = [select id, Account__r.Country_Code__c from Project__c where id=:projectId];
        string currencyIso = 'USD';
            if(pro.Account__r.Country_Code__c == 'au' || pro.Account__r.Country_Code__c == 'nz') currencyIso = 'AUD';

    	list<InvoiceEntryModal> entriesModal = (list<InvoiceEntryModal>)JSON.deserialize(entriesJson, list<InvoiceEntryModal>.class);
        //system.debug('entriesModal::' + entriesModal );
        list<Invoice_Draft__c> entries = new list<Invoice_Draft__c>();
        list<Invoice_Line_Items__c> lineItems = new list<Invoice_Line_Items__c>();
        map<id, id> mapProd_Pricebook = new map<id, id>();
        set<string> setOppLineItemId = new set<string>(); 

        Project__c project = [select id, Account__c, Account__r.Billing_Contact__c, Purchase_Order_No__c from Project__c where id=:projectId][0];

        for(InvoiceEntryModal entryModal: entriesModal){
            for(InvoiceEntryItemModal lineItemModal: entryModal.lines){
                mapProd_Pricebook.put(lineItemModal.productId, null);
                setOppLineItemId.add(lineItemModal.id);
            }           
        }

        map<string,Invoice_Draft__c> map_id_entry = new map<string,Invoice_Draft__c>();
        for(InvoiceEntryModal entryModal: entriesModal){
            Invoice_Draft__c entry = new Invoice_Draft__c();
            if(entryModal.id.length() > 18 ){
                entry.id = null; 
                entry.Project__c = projectId;
                entry.Invoice_Date__c = Date.today();
                entry.Type__c = 'Service';
                entry.CurrencyIsoCode = currencyIso;
                entry.RecordTypeId = Schema.SObjectType.Invoice_Draft__c.getRecordTypeInfosByName().get('Services').getRecordTypeId();
				entry.Invoice_Status__c = 'Draft';
            }else{
                entry.id  = entryModal.id;
            }
            entry.Purchase_Order_No__c = project.Purchase_Order_No__c;        
			entry.Account__c = project.Account__c;
		    entry.Billing_Contact__c = project.Account__r.Billing_Contact__c;          
            map_id_entry.put(entryModal.id, entry);
        }


        SavePoint sPoint = Database.setSavepoint();
        try{
            // Invoice_Draft__c --------------------
            upsert map_id_entry.values();
            // insert/update Invoice Line Items.
            for(InvoiceEntryModal entryModal: entriesModal){
                Invoice_Draft__c entry = map_id_entry.get(entryModal.id);
                for(InvoiceEntryItemModal lineItemModal: entryModal.lines){
                    lineItemModal.invoiceId = entry.Id;
                    lineItemModal.currencyCountryCode = currencyIso;
                    lineItemModal.totalPrice = lineItemModal.unitPrice*lineItemModal.quantity;
                    lineItems.add(lineItemModal.toInvoiceItemRecord());
                }
            }
            //--- Invoice_Line_Items__c
            upsert lineItems;
            response.result = lineItems;

        } catch(Exception ex){            
            response.getErrorMessage(ex);response.success = false;            
            Database.rollback(sPoint);
        }          
        return response;
    }

    /*
    * delete:
    * 
    * --- Invoice_Line_Items__c
    */
    @RemoteAction
    global static skedResponse deleteInvoiceEntryItem(string recordId){        
       skedResponse response = new skedResponse(true, '', '');
    	try{
            delete [select id from Invoice_Line_Items__c where id =:recordId];
    	}  catch(Exception ex){
    		response.getErrorMessage(ex);response.success = false;
    		
    	}

    	return response;
    }

    /*
    * delete:
    * 
    * --- Invoice_Line_Items__c
    */
    @RemoteAction
    global static skedResponse deleteInvoiceEntry(string recordId){        
       skedResponse response = new skedResponse(true, '', '');
    	try{
            delete [select id from Invoice_Draft__c where id =:recordId];
    	}  catch(Exception ex){
    		response.getErrorMessage(ex);response.success = false;    		
    	}

    	return response;
    }




}