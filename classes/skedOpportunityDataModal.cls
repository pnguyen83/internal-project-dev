global virtual class skedOpportunityDataModal {	

	
	public string 					id   			{get;set;} //opportunity id.
	public string 					startDate 		{get;set;}
	public string 					endDate 		{get;set;}
	public list<LicenseEntryModal>  entries 		{get;set;} //
	public list<ProductModal> 		products 		{get;set;} //all license products.//test
	public list<ProductFamily>		productFamilies {get;set;}

	/**
	 * License Entry. map with License_Entry__c object.
	 */
	public class LicenseEntryModal {
		public string id   							{get;set;}
		public string name  						{get;set;} //record name
		public boolean isDirty	 					{get;set;}
		public string startDate 					{get;set;}//YYYY-MM-DD
		public string endDate 						{get;set;}//YYYY-MM-DD
		public integer licenseEntryTotalMonths  	{get;set;}
		// SIS-137
		public String productFamily 				{get;set;}
		public list<LicenseEntryItemModal>  lines 	{get;set;} //License Entry Items.

		public LicenseEntryModal(License_Entry__c entry){
			this.id = entry.id;
			this.name = entry.name;
    		this.startDate = skedUtils.toDateString(entry.License_Entry_Start_Date__c);
    		this.endDate   = skedUtils.toDateString(entry.Opportunity__r.End_Date__c);//
    		this.licenseEntryTotalMonths = Integer.valueOf(entry.License_Entry_Total_Months__c);
    		this.lines 	= new list<LicenseEntryItemModal>();
    		this.isDirty = false;
		}
		
	}

	//-- License Entry Item.
	/**
	 * 
	 */
	public class LicenseEntryItemModal {
		public string id   				{get;set;}
		public boolean isDirty	 		{get;set;}
		public string productId 		{get;set;}		
		public string  description 		{get;set;}
		public integer quantity 		{get;set;}
		public Decimal unitPriceMonth 	{get;set;}
		public Decimal totalPriceMonth 	{get;set;}
		public Decimal totalACV 		{get;set;}

		public LicenseEntryItemModal(OpportunityLineItem lineItem){
				this.id = lineItem.id;
    			this.productId = lineItem.Product2Id;
    			this.description = lineItem.Description;
    			this.quantity = Integer.valueOf(lineItem.Quantity);
    			this.unitPriceMonth = lineItem.UnitPrice;//Sales Price.
    			this.totalPriceMonth = lineItem.TotalPrice;
    			this.totalACV = lineItem.Total_ACV__c;
    			this.isDirty = false;
		}
	}

	//Product
	/**
	 * 
	 */
	public class ProductModal {
		public string id   				{get;set;}
		public string name 				{get;set;}
		public string description 		{get;set;}
		public Decimal unitPriceMonth 	{get;set;}
		public ProductModal(string id, string name,string description, Decimal unitPriceMonth){
			this.id = id;
			this.name = name;
			this.description = description;
			this.unitPriceMonth = unitPriceMonth;
		}
	}

	 global class PickListItem {
        global string id    {get;set;}
        global string label {get;set;}

        global PickListItem(string id, string label){
            this.id     = id;
            this.label  = label;
        }
    }


	/**
	 * Invoice Entry. map with License_Entry__c object.
	 */
	public class InvoiceEntryModal {
		public string id   							{get;set;}
		public string name  						{get;set;} //record name
		public string projectId  					{get;set;}
		public boolean isDirty	 					{get;set;}
		public string startDate 					{get;set;}//YYYY-MM-DD
		public string endDate 						{get;set;}//YYYY-MM-DD
		public integer total  						{get;set;}
		//public string invoiceType					{get;set;}

		public list<InvoiceEntryItemModal>  lines 	{get;set;} //License Entry Items.	

		public InvoiceEntryModal(Invoice_Draft__c entry){
			this.id = entry.id;
			this.name = entry.name;
			this.projectId = entry.Project__c;
    		//this.startDate = skedUtils.toDateString(entry.License_Entry_Start_Date__c);
    		//this.endDate   = skedUtils.toDateString(entry.Opportunity__r.End_Date__c);//
    		//this.total = Integer.valueOf(entry.License_Entry_Total_Months__c);
    		this.lines 	= new list<InvoiceEntryItemModal>();
    		this.isDirty = false;
    		//this.invoiceType = entry.Type__c;
		}
		
	}

	//-- Invoice Entry Item.
	/**
	 * 
	 */
	public class InvoiceEntryItemModal {
		public string id   				{get;set;}
		public boolean isDirty	 		{get;set;}
		public string productId 		{get;set;}		
		public string invoiceId 		{get;set;}//master 
		public string  description 		{get;set;}
		public string  invoiceType 		{get;set;}
		public Decimal quantity 		{get;set;}
		public Decimal addon			{get;set;}
		public Decimal unitPrice 	{get;set;}
		public Decimal tax 	{get;set;}
		public Decimal totalPrice 	{get;set;}
		public Decimal discount 		{get;set;}
		public string currencyCountryCode {get;set;}
		

		public InvoiceEntryItemModal(Invoice_Line_Items__c lineItem){
				this.id = lineItem.id;
    			this.productId = lineItem.Product__c;
    			this.description = lineItem.Line_Item_Description__c;
    			this.quantity = lineItem.Line_Item_Quantity__c;
    			this.unitPrice = lineItem.Line_Item_Unit_Price__c;
    			this.totalPrice = lineItem.Line_Item_Total__c;    		
    			this.invoiceId = lineItem.Invoice__c;	
    			this.invoiceType = lineItem.Type__c;
    			this.tax = lineItem.Line_Item_Tax__c;
    			this.isDirty = false;
    			//this.discount = lineItem.Discount__c;
    			//this.addon = lineItem.Line_Item_Add_on__c;
		}

		public Invoice_Line_Items__c toInvoiceItemRecord(){
			string recordId = this.id;			
			boolean isInsert = recordId.length() > 18 ;
			if(isInsert){
                recordId = null;               
            }

			Invoice_Line_Items__c item = new Invoice_Line_Items__c(
				id = recordId,
				Product__c = this.productId,
				Line_Item_Description__c = this.description,
				Line_Item_Quantity__c = this.quantity,
				Line_Item_Unit_Price__c = this.unitPrice,
				Type__c = this.invoiceType,
				Line_Item_Total__c = this.totalPrice,
				Line_Item_Tax__c = this.tax,
				CurrencyIsoCode = String.isBlank(this.currencyCountryCode) ? 'USD':this.currencyCountryCode
				//Discount__c = this.discount != null ? this.discount : 0,
				//Line_Item_Add_on__c = this.addon != null ? this.addon : 0
				);	

			if(isInsert){
				item.Invoice__c = this.invoiceId;
			}

			system.debug(isInsert + ', item.Invoice__c:::' + item.Invoice__c );		

			return item;
		}
	}

	public class ProductFamily {
        public skedOpportunityDataModal.PickListItem productFamily {get; set;}
        public List<ProductModal> products {get; set;}

        public ProductFamily() {
        	this.products = new List<ProductModal>();
        }
    }
	
}