global class UpdateProjectDetailController{
    //==============================================================================//
    /**
    Get all Project Names
    */
    @remoteaction
    global static List<String> getAllProjectNames() {
        List<String> lstPrjNames = new List<String>();
        lstPrjNames = ProjectUtil.getAllProjectNames();
        return lstPrjNames;
    }

    //=============================================================================//
    /**
    Search opportunity based on name
    */
    @remoteaction
    global static List<OpportunityModal> searchOpportunity(String name) {
        List<OpportunityModal> lstResult = new List<OpportunityModal>();
        List<Opportunity> lstOpps = [SELECT Id, Name, Region__c, Region__r.Name, AccountId, Account.Name FROM Opportunity WHERE Project__c = '' ];
        if (lstOpps != null && !lstOpps.isEmpty()) {
            if (String.isNotBlank(name)) {
                for (Opportunity opp : lstOpps) {
                    if (opp.Name.toUpperCase().contains(name.toUpperCase())) {
                        OpportunityModal oppModal = new OpportunityModal(opp);
                        lstResult.add(oppModal);
                    }
                }
            } else {
                for (Opportunity opp : lstOpps) {
                    OpportunityModal oppModal = new OpportunityModal(opp);
                    lstResult.add(oppModal);    
                }
            }
        }

        return lstResult;
    }

    //==============================================================================//
    /**
    Search reagion based on Name
    */
    @remoteaction
    global static List<RegionModal> searchRegion(String name) {
        List<RegionModal> lstResult = new List<RegionModal>();
        List<sked__Region__c> lstRegions = [SELECT Id, Name FROM sked__Region__c];

        if (lstRegions != null && !lstRegions.isEmpty()) {
            if (String.isBlank(name)) {
                for (sked__Region__c region : lstRegions) {
                    RegionModal rModal = new RegionModal(region.Id, region.Name);
                    lstResult.add(rModal);
                }
            } else {
                for (sked__Region__c region : lstRegions) {
                    if (region.name.toUpperCase().contains(name.toUpperCase())) {
                        RegionModal rModal = new RegionModal(region.Id, region.Name);
                        lstResult.add(rModal);
                    }
                }
            }
        }

        return lstResult;
    }

    //==============================================================================//
    /**
    search account by name
    */
    @remoteaction
    global static List<AccountModal> searchAccount(String name) {
        List<AccountModal> lstResult = new List<AccountModal>();
        String soql = 'SELECT Id, Name FROM Account WHERE Name LIKE \'%' + name + '%\'';
        System.debug('soql = ' + soql);
        List<Account> lstAccounts = Database.query(soql);
        
        if (lstAccounts != null && !lstAccounts.isEmpty()) {
            for (Account acc : lstAccounts) {
                AccountModal accModal = new AccountModal(acc.Id, acc.Name);
                lstResult.add(accModal);
            }
        }

        return lstResult;
    }

    /**
    Get opportunity information
    */
    @remoteaction
    global static OpportunityModal getOpportunity(String oppID) {
        OpportunityModal oppModal = new OpportunityModal();
        List<Opportunity> lstOpps = [SELECT Id, Name, Region__c, Region__r.Name, AccountId, Account.Name FROM Opportunity WHERE Id = :oppID];
        if (lstOpps != null && !lstOpps.isEmpty()) {
            Opportunity opp = lstOpps.get(0);
            oppModal = new OpportunityModal(opp);
        }

        return oppModal;
    }

    //==============================================================================//
    /**
    Get Account information
    */
    @remoteaction
    global static AccountModal getAccount(String accID) {
        AccountModal accModal = new AccountModal();
        List<Account> lstAccs = [SELECT Id, Name FROM Account WHERE Id = :accID];
        if (lstAccs != null && !lstAccs.isEmpty()) {
            Account acc = lstAccs.get(0);
            accModal = new AccountModal(acc.Id, acc.Name);
        }

        return accModal;
    }

    /**
    Get Configure Data
    */
    @remoteaction
    global static ConfigData getConfigData() {
        return new ConfigData();
    }
    
    //==============================================================================//
    /**
    Create new Project
    */
    @remoteaction
    global static ResultReturn saveNewProject(ProjectModal prjData) {
        List<Opportunity> lstOpps = new List<Opportunity>();
        List<Resource_Requirement_Tag__c> lstReqTags = new List<Resource_Requirement_Tag__c>();
        Opportunity opp;
        ResultReturn result = new ResultReturn();
        String accId = '';
        Decimal requiredResource = 0;

        SavePoint sp = Database.setSavePoint();
        try {
            if (prjData.oppID != null) {
                lstOpps = [SELECT Id, AccountID, Project__c, Background__c, Do_they_have_a_trial__c, Trial_Type__c, Is_app_installed_into_their_SF_Instance__c, 
                           User_Info__c, Details_of_Demo__c, Do_they_already_use_Salesforce__c, Partner_Consultant_Info__c, Client_Expectations__c, 
                           SF_Project_Type__c, Add_On_Components__c FROM Opportunity WHERE Id = :prjData.oppID];
            }

            //Get resource category Id based on Name
            Map<String, String> mapResourceCategoryNameID = new Map<String, String>();
            for (Contact_Role__c rsCategory : [SELECT Id, Name FROM Contact_Role__c]) {
                mapResourceCategoryNameID.put(rsCategory.Name, rsCategory.Id);
            }

            Project__c prj = new Project__c(
                Name = prjData.name,
                Project_Manager__c = String.isNotBlank(prjData.manager) ? prjData.manager : null,
                Start__c = prjData.startDate,
                Status__c = String.isNotBlank(prjData.status) ? prjData.status : null,
                SF_Project_Type__c = prjData.type,
                Region2__c = String.isNotBlank(prjData.region) ? prjData.region : null,
                Account__c = String.isNotBlank(prjData.accID) ? prjData.accID : null,
                Opportunity__c = String.isNotBlank(prjData.oppID) ? prjData.oppID : null,
                UAT_Due_Date__c = prjData.uatDueDate,
                End__c = prjData.goLiveDate
            );
            if (!lstOpps.isEmpty()) {
                opp = lstOpps.get(0);
                initProjectFromOpportunity(prj, opp);
            }
            
            insert prj;
            
            if (prjData.resourceRequirements != null && !prjData.resourceRequirements.isEmpty()) {
                List<Resource_Requirement__c> lstResRequirements = new List<Resource_Requirement__c>();
                Map<String, List<Resource_Requirement_Tag__c>> mapReqTags = new Map<String, List<Resource_Requirement_Tag__c>>();

                Decimal index = 1;
                for (ProjectModal.ResourceRequirement resReq : prjData.resourceRequirements) {
                    Resource_Requirement__c newResReq = new Resource_Requirement__c(
                        Project__c = prj.Id,
                        Required_Number__c = resReq.noRequired,
                        Resource_Category__c = mapResourceCategoryNameID.get(resReq.category),
                        Index__c = index
                    );
                    requiredResource += resReq.noRequired;
                    
                    lstResRequirements.add(newResReq);

                    if (resReq.tags != null && !resReq.tags.isEmpty()) {
                        for (String tagId : resReq.tags) {
                            Resource_Requirement_Tag__c reqTag = new Resource_Requirement_Tag__c(
                                Resource_Requirement__c = newResReq.Id,
                                Tag__c = tagId
                            );
                            String key = newResReq.Resource_Category__c + '@' + index;
                            List<Resource_Requirement_Tag__c> lstTempReqTags = mapReqTags.get(key);
                            if (lstTempReqTags == null) {
                                lstTempReqTags = new List<Resource_Requirement_Tag__c>();
                            }
                            lstTempReqTags.add(reqTag);
                            mapReqTags.put(key, lstTempReqTags);
                        }
                    }

                    index++;
                }

                if (String.isNotBlank(prjData.manager)) {
                    Resource_Requirement__c newResReq = new Resource_Requirement__c(
                        Project__c = prj.Id,
                        Required_Number__c = 1,
                        Resource_Category__c = mapResourceCategoryNameID.get('Project Manager'),
                        Assigned__c = 1
                    );
                    requiredResource += 1;
                    lstResRequirements.add(newResReq);    
                }
                
                if (!lstResRequirements.isEmpty()) {
                    insert lstResRequirements;
                
                    //Insert Resource_Requirement_Tag__c
                    if (!mapReqTags.isEmpty()) {
                        for (Resource_Requirement__c resReq : lstResRequirements) {
                            String key = resReq.Resource_Category__c + '@' + resReq.Index__c;
                            if (mapReqTags.containsKey(key)) {
                                List<Resource_Requirement_Tag__c> lstTempReqTags = mapReqTags.get(key);
                                for (Resource_Requirement_Tag__c reqTag : lstTempReqTags) {
                                    reqTag.Resource_Requirement__c = resReq.Id;
                                    lstReqTags.add(reqTag);
                                }
                            }
                        }
                        if (!lstReqTags.isEmpty()) {
                            insert lstReqTags;
                        }
                    }
                }
            }

            prj.Num_of_Req_Resources__c = String.valueOf(requiredResource);
            List<sObject> lstSObjects = new List<sObject>{prj};

            if (opp != null) {
                opp.Project__c = prj.Id;
                lstSObjects.add(opp);    
            }

            update lstSObjects;

            result.data = prj.Id;
            result.isSuccess = true;
            
        } catch(Exception ex) {
            Database.rollback(sp);
            result.isSuccess = false;
            result.message = ex.getMessage();
            System.debug('Error: ' + ex.getMessage() + ', at line: ' + ex.getLineNumber() + ', stacktrace = ' + ex.getStackTraceString());
        } 
        return result;
    }

    //==============================================================================//
    /**
    initiate project from Opportunity
    */
    private static void initProjectFromOpportunity(Project__c prj, Opportunity opp) {
        prj.Background__c = String.isNotBlank(opp.Background__c) ? opp.Background__c : '';
        prj.Do_they_have_a_trial__c = opp.Do_they_have_a_trial__c;
        prj.Trial_Type__c = String.isNotBlank(opp.Trial_Type__c) ? opp.Trial_Type__c : '';
        prj.Is_app_installed_into_their_SF_Instance__c = opp.Is_app_installed_into_their_SF_Instance__c;
        prj.User_Info__c = String.isNotBlank(opp.User_Info__c) ? opp.User_Info__c : '';
        prj.Details_of_Demo__c = String.isNotBlank(opp.Details_of_Demo__c) ? opp.Details_of_Demo__c : '';
        prj.Do_they_already_use_Salesforce__c = opp.Do_they_already_use_Salesforce__c;
        prj.Partner_Consultant_Info__c = String.isNotBlank(opp.Partner_Consultant_Info__c) ? opp.Partner_Consultant_Info__c : '';
        prj.Client_Expectations__c = String.isNotBlank(opp.Client_Expectations__c) ? opp.Client_Expectations__c : '';
        prj.Opportunity__c = opp.Id;
        prj.Account__c = opp.AccountID;
        prj.Add_On_Components__c = opp.Add_On_Components__c;
    }
    
    //===============================================================================//
    /**
    Get picklist value from a field based on sObject Type Name and field name
    */
    public static List<String> getPickListFieldValues(String sObjectName, String fieldName) {
        List<String> lstResults = new List<String>();

        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SObjectType sObjectType = gd.get(sObjectName); 
        Schema.DescribeSObjectResult sObjectDescribe = sObjectType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = sObjectDescribe.fields.getMap();
        List<Schema.PicklistEntry> categoryValues = fieldMap.get(fieldName).getDescribe().getPickListValues();
        for (Schema.PicklistEntry a : categoryValues) {
            lstResults.add(a.getLabel());
        }

        return lstResults;
    }

    //===============================================================================//
    /**
    Search tag based on keyword
    */
    @remoteaction
    global static List<sked__Tag__c> searchTags(String searchStr) {
        List<sked__Tag__c> lstResults = new List<sked__Tag__c>();
        List<sked__Tag__c> lstTags = [SELECT Id, Name FROM sked__Tag__c];
        
        if (lstTags != null && !lstTags.isEmpty()) {
            if (String.isBlank(searchStr)) {
                lstResults = lstTags;
            } else {
                for (sked__Tag__c tag : lstTags) {
                    if (tag.Name.toUpperCase().contains(searchStr.toUpperCase())) {
                        lstResults.add(tag);
                    }
                }        
            }
        }

        return lstResults;
    }

    //===============================================================================//
    /**
    Search Project manager based on Name
    */
    @remoteaction
    global static List<sked__resource__c> searchProjectManagers(String searchStr) {
        List<sked__resource__c> lstResults = new List<sked__resource__c>();
        List<sked__resource__c> lstProjectManagers = [SELECT Id, Name, Main_Category_Name__c FROM sked__resource__c]; // WHERE Main_Category_Name__c = : HardcodeUtil.PROJECT_MANAGER];
        if (lstProjectManagers != null && !lstProjectManagers.isEmpty()) {
            if (String.isBlank(searchStr)) {
                lstResults = lstProjectManagers;
            } else {
                for (sked__resource__c resource : lstProjectManagers) {
                    if (resource.Name.toUpperCase().contains(searchStr.toUpperCase())) {
                        lstResults.add(resource);
                    }
                }
            }
        }

        return lstResults;
    }

    //==============================================================================//
    /**
    Get all resource categories
    */
    public static List<String> getResourceCategories() {
        List<String> lstResult = new List<String>();

        for (Contact_Role__c rCategory : [SELECT Id, Name FROM Contact_Role__c WHERE Name != 'Project Manager']) {
            lstResult.add(rCategory.Name);
        }

        return lstResult;
    }

    //===============================================================================//
    /**
    Opportunity Modal
    */
    global class OpportunityModal {
        public String oppID {get;set;}
        public String oppName {get;set;}
        public RegionModal region {get;set;}
        public AccountModal account {get;set;}
        
        public OpportunityModal() {
            this.oppID = '';
            this.oppName = '';
            this.region = new RegionModal('','');
            this.account = new AccountModal('','');
        }
        public OpportunityModal(Opportunity opp) {
            System.debug('opp = ' + opp);
            this.oppID = opp.Id;
            this.oppName = String.isNotBlank(opp.Name) ? opp.Name : '';
            String accID = String.isNotBlank(opp.AccountID) ? opp.AccountID : '';
            String accName = String.isNotBlank(opp.Account.Name) ? opp.Account.Name : '';
            this.account = new AccountModal(accId, accName);
            String regionID = opp.Region__c == null ? null : opp.Region__c ;
            String regionName = opp.Region__c == null || String.isBlank(opp.Region__r.Name) ? '' : opp.Region__r.Name;
            this.region = regionID == null ? null : new RegionModal(regionId, regionName);
        }
    }

    /**
    Project Config data
    */
    global class ConfigData {
        public List<String> categories {get;set;}
        public List<String> salesforceProjectTypes {get;set;}
        public List<String> jiraProjectType {get;set;}
        public List<String> projectStatuses {get;set;}

        public ConfigData() {
            this.categories = getResourceCategories();
            this.salesforceProjectTypes = getPickListFieldValues('Project__c', 'SF_Project_Type__c');
            this.jiraProjectType = getPickListFieldValues('Project__c', 'Jira_Project_Type__c');
            this.projectStatuses = getPickListFieldValues('Project__c', 'Status__c');
        }
    }

    //============================================================================//
    /**
    Region modal
    */
    global class RegionModal {
        String id {get; set;}
        String name {get; set;}

        public RegionModal(String regionID, String regionName) {
            this.id = regionID;
            this.name = regionName;
        }
    }

    //===========================================================================//
    /**
    Account modal
    */
    global class AccountModal {
        String id {get; set;}
        String name {get; set;}

        public AccountModal(String accId, String accName) {
            this.id = accId;
            this.name = accName;
        }

        public AccountModal() {
            
        }
    }

    //===========================================================================//
    /**
    Result Return class
    */
    global class ResultReturn {
        public String message {get;set;}
        public boolean isSuccess {get;set;}
        public object data {get;set;}
    }
}