@isTest 
private class skedAutoContractRenewalBatchTest {

    public static testMethod void testschedule(){
        skedDataFactory.initCommonData();
        Opportunity opp = [select Id, Subscription_Order__c from Opportunity limit 1];

        Subscription_Contract__c cont = [select id, Order_Status__c from Subscription_Contract__c limit 1];
        cont.Subscription_Contract_End__c = Date.today().addDays(60);
        cont.Order_Status__c = 'Active';
        cont.Initial_Opportunity__c = opp.Id;
        update cont;

        opp.Subscription_Order__c = cont.Id;
        update opp;

        Test.startTest();
        skedAutoContractRenewalSchedule  os = new skedAutoContractRenewalSchedule();
        string sch =' 0 0 13 * * ?';
        system.schedule('Test', sch, os);

        skedAutoContractRenewalBatch b = new skedAutoContractRenewalBatch();
        database.executebatch(b, 100);
        Test.stopTest();
    }
}