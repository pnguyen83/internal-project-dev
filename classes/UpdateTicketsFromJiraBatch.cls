global class UpdateTicketsFromJiraBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Database.QueryLocator locator;
        String query = 'SELECT Id, Project_Key__c FROM Project__c WHERE (NOT Status__c IN  (\'Completed\',\'Cancelled\'))';
        locator = Database.getQueryLocator(query);
        return locator;
    }
    
    global void execute(Database.BatchableContext bc, List<Project__c> scope) {
        Map<String, sked__Resource__c> mapResources = new Map<String, sked__Resource__c>();
        Map<String, Jira_Ticket__c> mapJiraTicket = new Map<String, Jira_Ticket__c>();
        Map<String, String> mapResourceTickets = new Map<String, String>();
        Map<String, String> mapPrjIDsAndKeys = new Map<String, String>();
        
        for (Project__c prj : scope) {
            mapPrjIDsAndKeys.put(prj.Id, prj.Project_Key__c);
        }
        
        for (Jira_Ticket__c ticket : [SELECT ID, Name, Project__c, Jira_Issue_Key__c 
                                      FROM Jira_Ticket__c WHERE Project__c IN : mapPrjIDsAndKeys.keySet()]) {
            mapJiraTicket.put(ticket.Jira_Issue_Key__c, ticket);
        }
        
        for (sked__Resource__c a : [SELECT Id, sked__Alias__c FROM sked__Resource__c]) {
            if (String.isNotEmpty(a.sked__Alias__c)) mapResources.put(a.sked__Alias__c, a);
        }
        
        for (String prjID : mapPrjIDsAndKeys.keySet()) {
            String prjKey = mapPrjIDsAndKeys.get(prjID);
            JiraUtil.getAllJiraTickets(prjKey, prjID, mapJiraTicket, mapResources, mapResourceTickets);
        }
        
        SavePoint sp = Database.setSavepoint();
        try {
            if (mapJiraTicket != null && !mapJiraTicket.isEmpty()) {
                upsert mapJiraTicket.values();
            }
            if (mapResources != null && !mapResources.isEmpty()) {
                upsert mapResources.values();
            }
            
            ProjectUtil.connectJiraTicketWithResource(mapResourceTickets);
            ProjectUtil.updateProjectResource(new List<String>(mapPrjIDsAndKeys.keySet()));
        }catch (Exception e) {
            Database.rollback(sp);
            LogUtil.generateLog('Error happens: ' + e.getMessage() + ', at line: ' + e.getLineNumber() + ', with stack trace setting: ' + e.getStackTraceString() + ' in ProjectUtil class');
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
}