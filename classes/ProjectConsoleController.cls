global with sharing class ProjectConsoleController {
	//======================================================================================//
	/**
	get all Tag Category to use on Project Console
	*/
	@remoteaction
	global static List<String> getAllTagCategories() {
		Set<String> setCategories = new Set<String>();
		for (Contact_Role__c resourceCategory : [SELECT Id, Name FROM Contact_Role__c /*WHERE Name != :HardcodeUtil.PROJECT_MANAGER*/]) {
			setCategories.add(resourceCategory.name);
		}

		return new List<String>(setCategories);
	}

	@remoteaction
	global static List<OptionItem> getAllRecordTypes() {
		List<OptionItem> lstResults = new List<OptionItem>();

		for (RecordType rt : [SELECT id, Name from RecordType where sObjectType = 'Project__c']) {
			OptionItem item = new OptionItem();
			item.id = rt.id;
			item.name = rt.name;
			lstResults.add(item);
		}

		return lstResults;
	}

	
	//======================================================================================//
	/**
	get all Project to use on Project Console
	*/
	@remoteaction
	global static List<ProjectModal> getAllProjects() {
		List<ProjectModal> lstProjectModals = new List<ProjectModal>();
		lstProjectModals = ProjectModal.getAllProjectModals();
		
		return lstProjectModals;
	}

	//======================================================================================//
	/**
	get all Project Manager to use on Project Console
	*/
	@remoteaction
	global static List<String> getAllProjectManager() {
		Set<String> setProjectManager = new Set<String>();

		for (Project__c prj : [SELECT Id, Project_Manager__c, Project_Manager_Name__c FROM Project__c]) {
			if (prj.Project_Manager__c != null) {
				setProjectManager.add(prj.Project_Manager_Name__c);
			}
		}

		return new List<String>(setProjectManager);
	}

	//======================================================================================//
	/**
	get all Project Status to use on Project Console
	*/
	@remoteaction
	global static List<String> getAllProjectStatus() {
		Set<String> setPrjStatus = new Set<String>();

		List<Schema.PicklistEntry> regionPickListValues = Project__c.Status__c.getDescribe().getPickListValues();
		for (Schema.PicklistEntry picklistValue : regionPickListValues) {
		 	String status = picklistValue.getLabel();
		 	if (!status.equals('Completed') && !status.equals('Cancelled')) {
		 		setPrjStatus.add(status);
		 	}
		}

		return new List<String>(setPrjStatus);
	}

	//======================================================================================//
	/**
	get all Project Region to use on Project Console
	*/
	@remoteaction
	global static List<String> getAllRegion() {
		List<String> lstRegions = new List<String>();

		Schema.DescribeFieldResult fieldResult = Project__c.Project_Region__c.getDescribe();
	    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
	        
	    for( Schema.PicklistEntry f : ple)
	    {
	    	lstRegions.add(f.getLabel());	
	    }       

		return lstRegions;
	}

	//======================================================================================//
	/**
	get all Projects based on filters
	*/
	@remoteaction
	global static List<ProjectModal> filterProjects(List<String> region, List<String> manager, List<String> status, List<String> category, List<String> recordType) {
		List<ProjectModal> lstProjectModals = new List<ProjectModal>();
		lstProjectModals = ProjectModal.queryProjects(region, manager, status, category);		

		List<ProjectModal> lstResults = new List<ProjectModal>();
		Set<String> setRecordTypes = new Set<String>(recordType);
		System.debug('recordType = ' + recordType);
		System.debug('setRecordTypes = ' + setRecordTypes);		
		for (ProjectModal prj : lstProjectModals) {
			if (!setRecordTypes.isEmpty()) {
				if (setRecordTypes.contains(prj.recordTypeId)) {
					lstResults.add(prj);	
				}
			} else {
				lstResults.add(prj);
			}
		}

		return lstResults;
	}

	//======================================================================================//
	/**
	remove a resource from a project
	*/
	@remoteaction
	global static void removeResource(String resourceID, String projectID) {
		ProjectModal.removeResource(resourceID, projectID);
	}

	//=====================================================================================//
	/**
	get all resource
	*/
	@remoteaction
	global static List<ResourceModal> getAllResources() {
		List<ResourceModal> lstResourceModals = new List<ResourceModal>();
		lstResourceModals = ResourceModal.getAllResources('');
		
		return lstResourceModals;
	}

	//======================================================================================//
	/**
	filter resource
	*/
	@remoteaction
	global static List<ResourceModal> filterResources(String category) {
		List<ResourceModal> lstResourceModals = new List<ResourceModal>();
		lstResourceModals = ResourceModal.getAllResources(category);
		for (ResourceModal rsModal : lstResourceModals) {
			rsModal.mapProjectModals = null;
		}
		return lstResourceModals;
	}

	//======================================================================================//
	/**
	Get project Modal detail of one project based on project id
	*/
	@remoteaction
	global static List<ProjectModal> getProjectModalDetail(String prjID) {
		List<ProjectModal> lstPrjModals = new List<ProjectModal>();
		lstPrjModals = ProjectModal.getProjectModalDetail(prjID);
		return lstPrjModals;
	}

	//======================================================================================//
	/**
	Get all Resource Modals of a Project
	*/
	@remoteaction
	global static List<ResourceModal> getAllResourcesOfProject(String prjID) {
		List<ResourceModal> lstResourceModals = new List<ResourceModal>();
		lstResourceModals = ResourceModal.getResourceModalsOfProject(prjID);
		return lstResourceModals;
	}

	//=====================================================================================//
	/**
	search project and resource based on name and return result
	*/
	@remoteaction
	global static List<SearchResult> searchProjectResource(String name) {
		List<SearchResult> lstResults = new List<SearchResult>();
		String sosl = 'FIND {' + name + '} IN Name FIELDS RETURNING Project__c(Id, Name, End__c, Start__c, UAT_Due_Date__c), sked__resource__c(Id, Name)';
		System.debug('sosl =  ' + sosl);	
		Search.SearchResults results = Search.Find(sosl);
		System.debug('1111 results = ' + results);
		if (results != null) {
			List<Search.SearchResult> lstProjects = results.get('Project__c');
			List<Search.SearchResult> lstResources = results.get('sked__resource__c');

			if (lstProjects != null && !lstProjects.isEmpty()) {
				for (Search.SearchResult result : lstProjects) {
					Project__c prj = (Project__c)result.getSObject();
					SearchResult prjResult = new SearchResult();
					prjResult.Id = prj.Id;
					prjResult.type = 'Project';
					prjResult.name = prj.Name;
					prjResult.startDate = prj.Start__c;
					prjResult.uatDueDate = prj.UAT_Due_Date__c;
					prjResult.goliveDate = prj.End__c;
					
					lstResults.add(prjResult);
				}
			}

			if (lstResources != null && !lstResources.isEmpty()) {
				for (Search.SearchResult result : lstResources) {
					sked__resource__c resource = (sked__resource__c)result.getSObject();
					SearchResult resourceResult = new SearchResult();
					resourceResult.Id = resource.Id;
					resourceResult.name = resource.name;
					resourceResult.type = 'Resource';
					
					lstResults.add(resourceResult);
				}
			}
		}

		return lstResults;
	}

	//=====================================================================================//
	/**
	Get one specificed resource
	*/
	@remoteaction
	global static List<ResourceModal> getOneResource(String resourceID) {
		List<ResourceModal> lstResources = new List<ResourceModal>();
		lstResources = ResourceModal.getOneResource(resourceID);
		return lstResources;
	}

	//=====================================================================================//
	/**
	assign a resource to a project
	*/
	@remoteaction
	global static void assignResourceToProject(String projectID, String resourceID, String categoryName, String startTime, String endTime) {
		if (String.isBlank(projectID))	{
			throw new CustomException('Cannot assign resource to project. Missing Project ID');
		} else if (String.isBlank(resourceID)) {
			throw new CustomException('Cannot assign resource to project. Missing Resource ID');
		} else if (String.isBlank(categoryName)) {
			throw new CustomException('Cannot assign resource to project. Missing Category');
		} else {
			SavePoint sp = Database.setSavepoint();

			List<Contact_Role__c> lstResourceCategoryIDs = [SELECT ID FROM Contact_Role__c WHERE Name = :categoryName];	
			if (lstResourceCategoryIDs == null) {
				throw new CustomException('Cannot assign resource to project. Category is not existing');
			} else if (checkAssignResource(projectID, resourceID, categoryName)) {
				throw new CustomException('This resource has beed assigned to this project with that category');
			} else {
				try {
					Date startDate = Date.valueOf(startTime);
					Date endDate = Date.valueOf(endTime);

					String categoryID = lstResourceCategoryIDs.get(0).Id;
					Project_Resource__c prjResource = new Project_Resource__c(
							Contact_Role__c = categoryID,
							Project__c = projectID,
							Resource__c = resourceID,
							Start_Date__c = startDate,
							End_Date__c = endDate
						);
					System.debug('prjResource = ' + prjResource);
					insert prjResource;

				} catch (Exception e) {
					Database.rollback(sp);
					throw new CustomException('Error happens: ' + e.getMessage() + ', stack trace = ' + e.getStackTraceString());
				}
			}
		}
	}

	/**
	Check if a resource has been assigned to a project already
	*/
	private static boolean checkAssignResource(String projectID, String resourceID, String categoryName) {
		List<Project_Resource__c> lstPrjResources = [SELECT Id FROM Project_Resource__c 
														WHERE Project__c = :projectID AND Resource__c = :resourceID AND Resource_Category_Name__c = :categoryName];
		if (lstPrjResources != null && !lstPrjResources.isEmpty()) {
			return true;
		}

		return false;
	}

	//=====================================================================================//
	/**
	Filter potential resources of a project based on project Id and Category
	*/
	@remoteaction
	global static List<ResourceModal> filterPotentialResources(String projectID, String category) {
		List<ResourceModal> lstResult = new List<ResourceModal>();
		System.debug('category = ' + category);
		List<ProjectModal> lstPrjs = new List<ProjectModal>();
		lstPrjs = ProjectModal.getProjectModalDetail(projectID);

		if (!lstPrjs.isEmpty()) {
			ProjectModal prjModal = lstPrjs.get(0);

			if (prjModal.potentialResources != null && !prjModal.potentialResources.isEmpty()) {
				if (String.isNotBlank(category) && category != 'Any') {
					for (ResourceModal rsModal : prjModal.potentialResources) {
						//rsModal.mapProjectModals = null;
						if (category.equals(rsModal.category)) {
							lstResult.add(rsModal);
						}
					}	
				} else {
					lstResult.addAll(prjModal.potentialResources);
				}
			}

			//Get assigned projects on each day from current date to the end of the project for potential resources
	        //get end date of this project
	        Date endDate = prjModal.goLiveDate == null ? prjModal.uatDueDate : prjModal.goLiveDate;
	        ResourceModal.getAssignedProjects(lstResult, System.Date.today(), endDate);
		}

		return lstResult;
	}

	//=====================================================================================//
	/**
	search result class
	*/
	global class SearchResult {
        public String Id {get;set;}
        public String name {get;set;}
        public String type {get;set;}
        public Date startDate {get;set;}
        public Date uatDueDate {get;set;}
        public Date goliveDate {get;set;}
    }

    global class OptionItem {
    	public String id {get; set;}
    	public String name {get; set;}
    }

}