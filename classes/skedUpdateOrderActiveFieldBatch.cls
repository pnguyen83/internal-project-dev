global class skedUpdateOrderActiveFieldBatch implements Database.Batchable<sObject> {
	
	String query;
	global skedUpdateOrderActiveFieldBatch() {}
	global Database.QueryLocator start(Database.BatchableContext BC) {
		query = 'select id,Is_Active_Order__c  from Order__c where Subscription_Entry_Start_Date__c <= TODAY and Is_Active_Order__c = false';
		return Database.getQueryLocator(query);
	}
   	global void execute(Database.BatchableContext BC, List<Order__c> scope) {
   		list<Order__c> updateOrders = new list<Order__c>();
		for(Order__c o: scope){
			o.Is_Active_Order__c = true;
			updateOrders.add(o);
		}
		update updateOrders;
	}
	global void finish(Database.BatchableContext BC) {}
	
}