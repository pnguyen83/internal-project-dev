global class ProjectUtil 
{
    public static boolean isFromTrigger {get;set;}
    
    //Use to get all task of a project from Jira
    webservice static String getAllTask(String prjKey, String prjID) {
        String result = 'success:true';
        Map<String, sked__Resource__c> mapResources = new Map<String, sked__Resource__c>();
        Map<String, Jira_Ticket__c> mapJiraTicket = new Map<String, Jira_Ticket__c>();
        Map<String, String> mapResourceTickets = new Map<String, String>();
        
        for (Jira_Ticket__c ticket : [SELECT ID, Name, Project__c, Jira_Issue_Key__c FROM Jira_Ticket__c WHERE Project__c = :prjID]) {
            mapJiraTicket.put(ticket.Jira_Issue_Key__c, ticket);
        }
        
        for (sked__Resource__c a : [SELECT Id, sked__Alias__c FROM sked__Resource__c]) {
            if (String.isNotEmpty(a.sked__Alias__c)) mapResources.put(a.sked__Alias__c, a);
        }
        
        JiraUtil.getAllJiraTickets(prjKey, prjID, mapJiraTicket, mapResources, mapResourceTickets);
        
        SavePoint sp = Database.setSavepoint();
        try {
            if (mapJiraTicket != null && !mapJiraTicket.isEmpty()) {
                upsert mapJiraTicket.values();
            }
            if (mapResources != null && !mapResources.isEmpty()) {
                upsert mapResources.values();
            }
            
            connectJiraTicketWithResource(mapResourceTickets);
            updateProjectResource(new List<String>{prjID});
        }catch (Exception e) {
            Database.rollback(sp);
            LogUtil.generateLog('Error happens: ' + e.getMessage() + ', at line: ' + e.getLineNumber() + ', with stack trace setting: ' + e.getStackTraceString() + ' in ProjectUtil class');
            result = 'Error happened: ' + e.getMessage();
        }
        
        return result;
    }
    
    //======================================================================================================//
    //use to map jira ticket with resource, only apply for resource that is not existing in the system yet
    public static void connectJiraTicketWithResource(Map<String, String> mapResourceTickets) {
        try {
            Map<String, String> mapJiraTicketResource = new Map<String, String>();
            List<Jira_Ticket__c> lstJiraTicket = new List<Jira_Ticket__c>();
            //get Resource
            for (sked__Resource__c a : [SELECT Id, sked__Alias__c FROM sked__Resource__c 
                                        WHERE sked__Alias__c IN : mapResourceTickets.keySet()]) {
                if (mapResourceTickets.containsKey(a.sked__Alias__c)) {
                    mapJiraTicketResource.put(mapResourceTickets.get(a.sked__Alias__c),a.Id);
                }
            }
            
            //get jira ticket record and update Assign_To___c field
            for (Jira_Ticket__c ticket : [SELECT Id, Jira_Issue_Key__c, Resource__c 
                                          FROM Jira_Ticket__c WHERE Jira_Issue_Key__c IN : mapResourceTickets.values()]) {
                if (mapJiraTicketResource.containsKey(ticket.Jira_Issue_Key__c)) {
                    ticket.Resource__c = mapJiraTicketResource.get(ticket.Jira_Issue_Key__c);
                    lstJiraTicket.add(ticket);
                }
            }
            
            if (!lstJiraTicket.isEmpty()) {
                update lstJiraTicket;
            }
        }
        catch(Exception e) {
            LogUtil.generateLog('Error happened: ' + e.getMessage() + ', at line: ' + e.getLineNumber() + ', with trace stack: ' + e.getStackTraceString() + ' in ProjectUtil class');
        }
    }
    
    //============================================================================================//
    public static void updateProjectResource(List<String> lstPrjID) {
        Set<Project_Resource__c> setPrjR = new Set<Project_Resource__c>();
        Map<String, String> mapResourceProject = new Map<String, String>();
        Id developerID = [SELECT Id FROM Contact_Role__c WHERE Role_Key__c = 'DEV'].Id;
        
        //query all project resources of this project to get resource ID
        for (Project_Resource__c prjR : [SELECT Id, Resource__c, Project__c FROM Project_Resource__c WHERE Project__c IN :lstPrjID]) {
            mapResourceProject.put(prjR.Resource__c, prjR.Project__c);
        }
        
        //query all jira ticket of this project to check which resource does not have project resource record
        for (Jira_Ticket__c ticket : [SELECT ID, Resource__c, Project__c  FROM Jira_Ticket__c WHERE Project__c IN :lstPrjID]) {
            if (!mapResourceProject.containsKey(ticket.Resource__c) || mapResourceProject.isEmpty()) {
                Project_Resource__c prjR = new Project_Resource__c(Resource__c = ticket.Resource__c,
                                                                   Contact_Role__c = developerID != null ? developerID : null,
                                                                   Project__c = ticket.Project__c);
                mapResourceProject.put(ticket.Resource__c, ticket.Project__c);
                setPrjR.add(prjR);
            }
        }
        
        if (!setPrjR.isEmpty()) {
            insert new List<Project_Resource__c>(setPrjR);
        }
    }
    
    //======================================================================================================//
    //Update salesforce project data based on jira project data
    public static void updateProject(Project__c prj, JsonProject json, Map<String, String> mapPrjType)
    {
        prj.Project_Key__c = json.key;
        prj.Jira_URL__c = Jira_Credential__c.getOrgDefaults().Host__c + '/projects/' + json.key;
        if (json.projectTypeKey != null && mapPrjType.containsKey(json.projectTypeKey))	prj.Jira_Project_Type__c = mapPrjType.get(json.projectTypeKey);
        if (json.projectCategory != null)	prj.Project_Category__c = json.projectCategory.name;
        prj.Name = json.name;
    }
    
    //====================================================================================================//
    //Generate Jira Project
    webservice static String generateJiraProject(String prjID, String prjKey, String name, String description, String type, String resourceName)
    {
        String result = 'success:true';
       // SavePoint sp = Database.setSavepoint();
        try
        {	
            Jira_Credential__c jiraLogin = Jira_Credential__c.getInstance();
            HttpResponse res = JiraUtil.createProject(prjKey, name, description, type, resourceName, jiraLogin);
            JiraUtil.JsonMessage errorMessage = (JiraUtil.JsonMessage) System.JSON.deserialize(res.getBody(), JiraUtil.JsonMessage.class);
                
            if (res.getStatusCode() == 400 || res.getStatusCode() == 401 || res.getStatusCode() == 403) result = errorMessage.errors.values()[0];
            else 
            {
                Project__c prj = new Project__c(Id = prjID, Jira_URL__c = jiraLogin.Host__c + '/projects/' + prjKey.toUpperCase());
                update prj;
            }
        }
        catch(Exception e)
        {
            //Database.rollback(sp);
            LogUtil.generateLog('Error happened: ' + e.getMessage() + ', at line: ' + e.getLineNumber() + ', with trace stack: ' + e.getStackTraceString() + ' in ProjectUtil class');
            result = 'Error happened: ' + e.getMessage() + ', at line: ' + e.getLineNumber();
        }
        
        
        return result;
    }
    
    
    //====================================================================================//
    //validate if the user for a new project is existing in Jira or not
    webservice static String validateProjectManager(String username, String name)
    {
        String result = 'success:true';
        if (String.isNotBlank(username))
        {
            HttpResponse response = JiraUtil.connectToJira('/rest/api/2/user?username=' + username);
            JiraUtil.JsonMessage message = (JiraUtil.JsonMessage) System.JSON.deserialize(response.getBody(), JiraUtil.JsonMessage.class);
            if (message.errors != null && !message.errors.isEmpty())	result = HardcodeUtil.JIRA_USERNAME_NOT_EXIST_MESSAGE + username;
        }
        else
        {
            result = HardcodeUtil.JIRA_USER_NOT_EXISTING_MESSAGE + name;
        }
        return result;
    } 
    
    //=======================================================================================//
    //get list of Project that update Project Manager field
    public static void updatePMProjectResource(List<Project__c> lstNew, List<Project__c> lstOld) {
        Map<String, Project__c> mapPrj = new Map<String, Project__c>();
        for (Project__c prj : lstNew) {
            if (prj.Project_Manager__c != null) {
                mapPrj.put(prj.Id, prj);
            }
        }
        
        if (lstOld != null) {
            for (Project__c prj : lstOld) {
                if (mapPrj.containsKey(prj.Id) && mapPrj.get(prj.Id).Project_Manager__c.equals(prj.Project_Manager__c)) {
                    mapPrj.remove(prj.Id);
                }
            }
        }
        
        if (!mapPrj.isEmpty()) {
            // update or insert PM Project resource
            ProjectResourceUtil.updateProjectManagerID(mapPrj);
        }
    }
    
    //=======================================================================================//
    //get list of Projects that have Project Manager field removed
    public static void getProjectsRemovedPM(List<Project__c> lstNew, List<Project__c> lstOld) {
        Map<String, Project__c> mapPrj = new Map<String, Project__c>();
        Map<String, Project__c> mapResult = new Map<String, Project__c>();
        
        //get map of old Project 
        for (Project__c prj : lstNew) {
            if (prj.Project_Manager__c == null) {
                mapPrj.put(prj.Id, prj);
            }
        }
        
        //get map of projects that have removed Project Manager field
        for (Project__c prj : lstOld) {
            if (mapPrj.containsKey(prj.ID) && prj.Project_Manager__c != null) {
                mapResult.put(prj.Id, prj);
            }
        }
        
        //remove the corresponding PM project resource
        if (!mapResult.isEmpty()) {
            ProjectResourceUtil.deletePMProjectResource(mapResult);
        }
    }
    
    //=====================================================================================//
    //Remove the Key_Contact__c of associated project when PM Project Resource is deleted
    public static void removePMField(Set<String> setPrjID) {
        try {
            List<Project__c> lstPrj = new List<Project__c>();
            
            for (Project__c prj : [SELECT Id, Project_Manager__c FROM Project__c WHERE ID IN : setPrjID]) {
                if (prj.Project_Manager__c != null) {
                    prj.Project_Manager__c = null;
                    lstPrj.add(prj);
                }
            }
            
            if (!lstPrj.isEmpty()) {
                update lstPrj;
            }
        }
        catch(Exception e) {
            LogUtil.generateLog('Error happened: ' + e.getMessage() + ', at line: ' + e.getLineNumber() + ', with trace stack: ' + e.getStackTraceString() + ' in ProjectUtil class');
        }
    }

    //=====================================================================================//
    //Get all project's name
    public static List<String> getAllProjectNames() {
        Set<String> setPrjNames = new Set<String>();
        for (Project__c prj : [SELECT Id, Name FROM Project__c]) {
            setPrjNames.add(prj.Name);
        }

        return new List<String>(setPrjNames);
    }

    //====================================================================================//
    //Update all project resource start date and end date
    public static void updateAllPrjResources(List<Project__c> lstPrjs) {
        Set<String> setPrjID = new Set<String>();
        List<Project_Resource__c> lstPrjResources = new List<Project_Resource__c>();

        for (Project__c prj : lstPrjs) {
            setPrjID.add(prj.Id);
        }
        
        for (Project_Resource__c prjResource : [SELECT ID, Project__c FROM Project_Resource__c WHERE Project__c IN : setPrjID]) {
            lstPrjResources.add(prjResource);
        }
        
        if (!lstPrjResources.isEmpty()) {
            ProjectResourceUtil.updateStartDateAndEndDate(lstPrjResources);
            SavePoint sp = Database.setSavePoint();
            try {
                    update lstPrjResources;
                } catch (Exception e) {
                    Database.rollback(sp);
                    throw new CustomException(e.getMessage());
                }
            
        }
    }

    //======================================================================================//
    //Delete the project field of related opportunity when delete a project
    public static void deleteOpportunityProjectField(List<Project__c> lstPrj) {
        Set<String> setOppID = new Set<String>();
        List<Opportunity> lstOpps = new List<Opportunity>();

        for (Project__c prj : lstPrj) {
            if (prj.Opportunity__c != null) {
                setOppId.add(prj.Opportunity__c);
            }
        }

        if (!setOppID.isEmpty()) {
            for (String oppID : setOppId) {
                Opportunity opp = new Opportunity(Id = oppID, Project__c = null);
                lstOpps.add(opp);    
            }

            try {
                update lstOpps;
            } catch (Exception e) {
                throw new CustomException(e.getMessage());
            }
        }
    }
    
}