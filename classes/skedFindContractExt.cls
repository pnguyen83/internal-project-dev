global class skedFindContractExt  {
	
	webservice static String findContract(String oppId) {

		try {
			Opportunity opp = [SELECT Id, Start_Date__c, Subscription_Order__c, Primary_Contact__c, Payment_Terms__c, License_Payment_Method__c, End_Date__c, Purchase_Order_No__c, AccountId, RecordTypeId, RecordType.Name, 
									Currency__c, BillingContactId__c, CurrencyIsoCode
							   FROM Opportunity WHERE Id = : oppID];

			if (opp == null) {
				return 'Invalid Opportunity.';
			}

			if (opp.Start_Date__c == null) {
				return 'You have not selected a Start Date for this Opportunity, you must have a start date before we can find you the right contract.';
			}

			Boolean isActive = false;
            for (Account acc : [SELECT Id, (SELECT Id, Order_Status__c, Subscription_Contract_Start__c, Subscription_Contract_End__c FROM Orders__r WHERE Order_Status__c = 'Active' LIMIT 1) 
                               FROM Account WHERE Id =: opp.AccountId]) {
                if (acc.Orders__r.isEmpty()) isActive = true;
            }

            String conId = getActiveContract(opp, isActive);

            if (conId == null) return 'There is currently no contract created for this Add On opportunity to associate to. Please contact Finance to organise the contract creation, you will not be able to enter any license entries until we know the contract details.';

            if (String.isNotBlank(conId)) {
				Subscription_Contract__c cont = [SELECT Id, Contact__c, Payment_Terms__c, Payment_Method__c, Subscription_Contract_End__c, Purchase_Order_No__c
													FROM Subscription_Contract__c WHERE Id =: conId LIMIT 1];
                opp.Subscription_Order__c		= conId;
				opp.Primary_Contact__c          = cont.Contact__c;
				opp.Payment_Terms__c            = cont.Payment_Terms__c;
				opp.License_Payment_Method__c   = cont.Payment_Method__c;
				opp.End_Date__c                 = cont.Subscription_Contract_End__c;
				opp.Purchase_Order_No__c        = cont.Purchase_Order_No__c;
            } else return '';

			update opp;

			return '';
		} catch (Exception ex) {
			return ex.getDmlMessage(0);
		}
	}

	private static String getActiveContract(Opportunity opp, Boolean isActive) {
        Id oppAddonRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(SkeduloConstants.OPP_RECORDTYPE_ADDON).getRecordTypeId();
        Id oppRenewalRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(SkeduloConstants.OPP_RECORDTYPE_RENEWAL).getRecordTypeId();
        Subscription_Contract__c SC = new Subscription_Contract__c();

		if (opp.RecordTypeId == oppAddonRecordTypeId) {
            String availContractId = '';
            for (Subscription_Contract__c con : [SELECT Id, Name, Order_Status__c, Subscription_Contract_Start__c, Subscription_Contract_End__c 
                                                       FROM Subscription_Contract__c 
                                                       WHERE Account__c =: opp.AccountId
													   AND (Order_Status__c = 'Active' OR Order_Status__c = 'Not Started')]) {
                if (opp.Start_Date__c >= con.Subscription_Contract_Start__c && opp.Start_Date__c <= con.Subscription_Contract_End__c) {
                    availContractId = con.Id;
                }
            }
        
            if (String.isBlank(availContractId)) {
                return null;
            }

            return availContractId;

        } else {
            SC = new Subscription_Contract__c(
                Purchase_Order_No__c = opp.Purchase_Order_No__c,
                Account__c = opp.AccountId,
                Currency__c = opp.Currency__c,
                Contact__c = opp.Primary_Contact__c,
                Order_Status__c = isActive ? 'Active' : 'Not Started', 
                BillingContact__c = opp.BillingContactId__c,
                Subscription_Contract_Start__c = opp.Start_Date__c,
                Subscription_Contract_End__c = opp.End_Date__c,
                Total_Invoiced__c = 0,
                Payment_Terms__c = opp.Payment_Terms__c,
                Payment_Method__c = opp.License_Payment_Method__c,
                CurrencyIsoCode = opp.CurrencyIsoCode,
                Initial_Opportunity__c = opp.Id
            );
            insert SC;
        }
        return SC.Id;
    }

}